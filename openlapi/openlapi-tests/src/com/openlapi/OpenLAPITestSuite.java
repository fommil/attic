/*
 * Copyright ThinkTank Maths Limited 2006 - 2008
 *
 * This file is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file. If not, see <http://www.gnu.org/licenses/>.
 */
package com.openlapi;

import java.util.Enumeration;
import java.util.Vector;

import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.StringItem;
import javax.microedition.midlet.MIDlet;

import thinktank.j2me.TTUtils;
import thinktank.j2me.TTUtils.ILogger;

/**
 * A collection of tests for the LandmarkStore to be run in an emulator or on a J2ME
 * device.
 */
public class OpenLAPITestSuite extends MIDlet {

	private final Form form;
	// Vector<ALAPITest>
	private final Vector tests = new Vector();

	public OpenLAPITestSuite() {
		// add the tests here
		tests.addElement(new AddressInfoTest());
		tests.addElement(new CoordinatesTest());
		tests.addElement(new LandmarkStoreTest());
		tests.addElement(new LocationProviderTest());
		form = new Form("OpenLAPI Tests");
		TTUtils.registerLogger(new ILogger() {
			public void log(String message) {
				form.append(message);
				Display.getDisplay(OpenLAPITestSuite.this).setCurrent(form);
			}
		});
	}

	public void destroyApp(boolean unconditional) {
	}

	public void pauseApp() {
	}

	public void startApp() {
		form.append("Starting tests...\n");
		Display display = Display.getDisplay(this);
		display.setCurrent(form);

		// cycle through all the tests
		for (Enumeration e = tests.elements(); e.hasMoreElements();) {
			ALAPITest test = (ALAPITest) e.nextElement();
			String title = test.getClass().getName();
			try {
				test.runTests();
				StringItem item = new StringItem(title, "passed\n");
				form.append(item);
				display.setCurrent(form);
			} catch (Exception ex) {
				// set the alert message accordingly
				StringItem item =
						new StringItem(title + " FAILED", ex.getMessage()
								+ "\n");
				//TTUtils.log(title + " FAILED: " + ex.getMessage());
				form.append(item);
				display.setCurrent(form);
			}

		}
		form.append("Completed.\n");
		display.setCurrent(form);
		// catch any failed test Exceptions
	}

}
