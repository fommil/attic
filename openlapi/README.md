openlapi
========

*OpenLAPI* is most commonly used to access Bluetooth GPS devices from mobile devices, whilst retaining a high level of source code compatibility with JSR-179. OpenLAPI also has a range of backends for emulating location in development environments, making it a popular tool for J2ME developers.

The emulator mode supports the following back-ends for determining location:-

  * Access of hardware GPS device over bluetooth (if available)
  * Emulate a moving device from an [NMEA](http://en.wikipedia.org/wiki/NMEA_0183) log file (recorder application provided).
  * Emulate a moving device from a predefined trail in a Google Earth file [KML](http://en.wikipedia.org/wiki/Keyhole_Markup_Language).
  * Emulate a moving device by picking random entries from a persistent store

And the software may be shipped as part of a [MIDlet](http://en.wikipedia.org/wiki/Midlet) (Java application) to provide location awareness through the following back-ends:-

  * Access of bluetooth GPS device. Bluetooth GPS devices are very affordable.
  * Proprietary APIs. A select number of handsets were released with GPS devices in the period before the adoption of JSR-179, and therefore have their own interface. OpenLAPI can provide a wrapper layer to hide the hardware in these cases.
  * Log file lookup, appropriate for physically static devices

It is possible to negotiate commercial arrangements with local network providers to allow location detection via CellID and other network specific techniques (e.g. angle of incidence and triangulation). A mobile phone knows which basestation it is communicating with. By knowing where the basestation is located, location awareness can be accurate to several hundred meters!

Similar projects
================

The [FLOPI](http://sourceforge.net/projects/flopi/) (Free LOcation aPI) project is written in J2SE and is aimed at adding location awareness to desktop Java applications through many of the same back-ends as OpenLAPI. However, be warned that the author has admitted serious numerical flaws in the `Coordinates.distanceTo()` and `Coordinates.azimuthTo()` methods. OpenLAPI uses the [Vincenty](http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf Vincenty) (1.2MB) algorithm which is guaranteed to be accurate to the μm scale.

Legal
=====

For various technical and legal reasons, OpenLAPI is not referred to as an implementation of JSR-179. In order to legally refer to a piece of software as a JSR, it must pass the SUN TCK. However, as there is currently no publicly available TCK for JSR-179, it is impossible to test any implementations for 100% compliance. Included in OpenLAPI is a suite of tests for self-compliance and we invite the Open Source community to contribute to the tests.



Donations
=========

Please consider supporting the maintenance of this open source project with a donation:

[![Donate via Paypal](https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=B2HW5ATB8C3QW&lc=GB&item_name=openlapi&currency_code=GBP&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)


Licence
=======

Copyright (C) 2006 Samuel Halliday

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see http://www.gnu.org/licenses/


Contributing
============

Contributors are encouraged to fork this repository and issue pull
requests. Contributors implicitly agree to assign an unrestricted licence
to Sam Halliday, but retain the copyright of their code (this means
we both have the freedom to update the licence for those contributions).
