/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu.widgets;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

import com.yiibu.AWidget;

public class ImageWidget extends AWidget {

	protected void onClick() {
		System.out.println("ImageWidget received click! :-D");
	}

	protected void onFocus() {
		System.out.println("ImageWidget received focus! :-D");
	}

	protected void onFocusOut() {
		System.out.println("ImageWidget lost focus :-(");
	}

	protected void paint(Graphics graphics, int width, int height) {
		String fileName = (String) table.get("src");
		InputStream stream = ImageWidget.class.getResourceAsStream("/"
				+ fileName);
		Image image;
		try {
			image = Image.createImage(stream);
		} catch (IOException e) {
			return;
		}
		graphics.drawImage(image, 0, 0, Graphics.TOP | Graphics.LEFT);
	}

}
