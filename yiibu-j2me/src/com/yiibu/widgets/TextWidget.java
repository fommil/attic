/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu.widgets;

import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;

import com.yiibu.AWidget;
import com.yiibu.Application;

public class TextWidget extends AWidget {

	protected void onClick() {
		System.out.println("TextWidget received click! :-D");	}

	protected void onFocus() {
		System.out.println("TextWidget received focus! :-D");
	}

	protected void onFocusOut() {
		System.out.println("TextWidget lost focus :-(");
	}

	protected void paint(Graphics graphics, int width, int height) {
		int background = 0xFFFFFF;
		try {
			background = Integer.valueOf((String) table.get("background"))
					.intValue();
		} catch (NumberFormatException e) {
		}
		graphics.setColor(background);
		graphics.fillRoundRect(0, 0, width, height, 10, 10);

		String text = (String) table.get("text");
		if (text == null)
			return;
		int colour = 0x000000;
		colour = Application.parseColour((String) table.get("color"));
		try {
			Font font = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN,
					Font.SIZE_SMALL);
			graphics.setFont(font);
		} catch (IllegalArgumentException e) {
			System.out.println("Font not available: " + e.getMessage());
		}
		graphics.setColor(colour);
		graphics.drawString(text, width / 2, height / 2, Graphics.HCENTER
				| Graphics.BASELINE);
	}
}
