/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu.exceptions;

/**
 * Indicates a work in progress.
 */
public class NotImplementedException extends RuntimeException {

}
