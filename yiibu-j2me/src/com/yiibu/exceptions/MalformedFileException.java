/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu.exceptions;

/**
 * Indicates that the XML file was malformed.
 */
public class MalformedFileException extends Exception {

	/**
	 * @param string
	 */
	public MalformedFileException(String string) {
		super(string);
	}

}
