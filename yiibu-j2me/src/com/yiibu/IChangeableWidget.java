/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu;

/**
 * A widget that may have its state changed by other widgets.
 */
public interface IChangeableWidget {

	/**
	 * Another widget has requested that the state of this widget be changed. Code coding
	 * practise would suggest that if this widget changes its own state, it do so using
	 * this call (e.g. a list of strings that the user may select).
	 *
	 * @param newValue
	 */
	void onChange(String newValue);

}
