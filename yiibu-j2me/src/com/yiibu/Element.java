/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu;

import java.util.Enumeration;
import java.util.Hashtable;

import com.yiibu.exceptions.MalformedFileException;

/**
 * Common superclass of all XML elements, giving access to the attributes and unique ID.
 */
public class Element {

	private final String name;

	/** <String, String> */
	final Hashtable table;

	/**
	 * @param table
	 * @throws MalformedFileException
	 */
	public Element(Hashtable table) throws MalformedFileException {
		this.table = table;
		name = (String) table.get("id");
		if (name == null)
			throw new MalformedFileException(
					"Element did not have an 'id' attribute.");

		// don't want this showing up in the attributes list anymore
		table.remove("id");
	}

	/**
	 * @return the name of the Page,
	 */
	public String getName() {
		return name;
	}

	public String toString() {
		Enumeration en = table.keys();
		StringBuffer buffer = new StringBuffer();
		buffer.append(name + ":\n");
		for (; en.hasMoreElements();) {
			String key = (String) en.nextElement();
			String value = (String) table.get(key);
			buffer.append("    Key: " + key + ", Value: " + value + "\n");
		}
		return buffer.toString();
	}

}
