/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu;

import java.util.Hashtable;

import javax.microedition.lcdui.Graphics;

import com.yiibu.exceptions.NotImplementedException;

/**
 * The parent of all J2ME widget objects, which are dynamically loaded at runtime.
 */
public abstract class AWidget {

	/**
	 * Set to false by default, but true when the user has requested that the widget
	 * release control of the application.
	 */
	protected volatile boolean exit = false;

	/** <String, String> properties as read from XML and dynamically updateable */
	protected Hashtable table;

	/**
	 * Bit of a hack, but this is how the widget implementations get to see their state.
	 *
	 * @param widget
	 */
	void setProperties(Widget widget) {
		this.table = widget.table;
	}

	/**
	 * When the user has clicked on this widget, this method is called. It need not do
	 * anything, but should return quickly as the UI will block on this method (unless the
	 * widget intends to keep the focus).
	 * <p>
	 * Implementations must check the status of the flag {@link #exit} at regular
	 * intervals, as this indicates that the user would like to exit this widget at the
	 * earliest possible opportunity. (Don't forget to reset the flag to false just before
	 * returning cleanly).
	 *
	 * @see #exit
	 */
	protected abstract void onClick();

	/**
	 * When the user has given focus to this widget, this method is called. It need not do
	 * anything, but should return quickly as the UI will block on this method (unless the
	 * widget intends to keep the focus).
	 * <p>
	 * Implementations must check the status of the flag {@link #exit} at regular
	 * intervals, as this indicates that the user would like to exit this widget at the
	 * earliest possible opportunity. (Don't forget to reset the flag to false just before
	 * returning cleanly).
	 *
	 * @see #exit
	 */
	protected abstract void onFocus();

	/**
	 * When the user removes focus from this widget, this method is called. It need not do
	 * anything, but should return quickly as the UI will block on this method. This
	 * method should not be greedy.
	 */
	protected abstract void onFocusOut();

	/**
	 * Called when the widget is asked to draw its contents.
	 *
	 * @param graphics
	 *            bounded so that (0, 0) is the upper left of the widget's screen real
	 *            estate
	 * @param width
	 *            the maximum width that may be drawn upon (anything beyond this is
	 *            ignored)
	 * @param height
	 *            the maximum height that may be drawn upon (anything beyond this is
	 *            ignored)
	 */
	protected abstract void paint(Graphics graphics, int width, int height);

	/**
	 * Called by the widget to request that it be redrawn, e.g. periodically called by
	 * animations.
	 */
	protected final void repaint() {
		// TODO: implement repaint
		throw new NotImplementedException();
	}

	/**
	 * Called when the user has chosen the exit/back button when this widget has control.
	 */
	final void onExit() {
		exit = true;
	}
}
