/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu;

import java.util.Hashtable;

import com.yiibu.exceptions.MalformedFileException;


public class Page extends Element {

	/** <String, Widget> */
	final Hashtable widgets;
	final String navigate;

	/**
	 * @param table
	 * @param navigate
	 *            the place to navigate to by the default button. This is a hack.
	 * @throws MalformedFileException
	 */
	public Page(Hashtable table, Hashtable widgets, String navigate)
			throws MalformedFileException {
		super(table);
		this.widgets = widgets;
		this.navigate = navigate;
	}

}
