/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu;

import java.util.Hashtable;

import com.yiibu.exceptions.MalformedFileException;

/**
 * Not to be confused with {@link AWidget}, this is the container class that contains the
 * attributes associated to a widget. These objects are held in memory, unlike the
 * implementations.
 */
public class Widget extends Element {

	/**
	 * @param table
	 * @throws MalformedFileException
	 */
	public Widget(Hashtable table) throws MalformedFileException {
		super(table);
		if (!table.containsKey("class"))
			throw new MalformedFileException("Widget didn't set a class");
	}

	/**
	 * @return a new object which is the implementation of this widget.
	 * @throws ClassNotFoundException
	 */
	public AWidget getImpl() throws ClassNotFoundException {
		String className = (String) table.get("class");
		Class clazz = Class.forName(className);
		AWidget impl;
		try {
			impl = (AWidget) clazz.newInstance();
		} catch (InstantiationException e) {
			throw new ClassNotFoundException(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new ClassNotFoundException(e.getMessage());
		}
		return impl;
	}
}
