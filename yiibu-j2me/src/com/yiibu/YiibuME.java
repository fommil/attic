/**
 * Aug 23, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu;

import java.io.InputStream;

import javax.microedition.lcdui.Display;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import com.yiibu.exceptions.NotImplementedException;

/**
 * Toy model implementation of an XML-based framework for designing J2ME applications
 * without writing any Java.
 */
public class YiibuME extends MIDlet {

	private volatile Application application = null;

	protected void destroyApp(boolean unconditional)
			throws MIDletStateChangeException {
		// TODO: implement destroyApp
		throw new NotImplementedException();
	}

	protected void pauseApp() {
		// TODO: implement pauseApp
		throw new NotImplementedException();
	}

	protected void startApp() throws MIDletStateChangeException {
		if (application == null) {
			// first call of startApp, load the Application
			InputStream stream = YiibuME.class
					.getResourceAsStream("/application.xml");
			if (stream == null)
				throw new NullPointerException();

			application = new Application(this, stream);
		}
		Display display = Display.getDisplay(this);
		display.setCurrent(application);

		// hand over control to the Application
		application.run();
	}

}
