/**
 * Aug 24, 2007
 *
 * @author Samuel Halliday
 */
package com.yiibu;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDlet;

import org.kxml2.io.KXmlParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.yiibu.exceptions.MalformedFileException;

/**
 * This is the master of the application. It is entirely created at runtime from the
 * definition XML. This class controls:
 * <ul>
 * <li>Parsing the XML</li>
 * <li>Auto insertion of expected {@link Command}s (e.g. exit, back)</li>
 * <li>Rendering of {@link Page}s</li>
 * <li>Controlling access to {@link Widget} state for {@link AWidget}s</li>
 * </ul>
 */
public class Application extends Canvas implements CommandListener {

	private static final String PAGE = "page";

	private static final String WIDGET = "widget";

	/**
	 * Parse an integer RGB colour value e.g. "123" TODO: support "0x123", "#123"
	 *
	 * @param value
	 * @return
	 */
	public static int parseColour(String value) {
		if (value == null)
			return 0;

		return Integer.valueOf(value).intValue();
	}

	/**
	 * Parse a size {@link String} such as "100px" or "10%" into pixels.
	 *
	 * @param value
	 * @param full
	 *            the number of pixels considered to be 100%
	 * @return
	 */
	public static int parseSize(String value, int full) {
		if (value == null)
			throw new NullPointerException();

		if (value.endsWith("px"))
			return Integer.valueOf(value.substring(0, value.length() - 2))
					.intValue();
		else if (value.endsWith("%")) {
			int percentage = Integer.valueOf(
					value.substring(0, value.length() - 1)).intValue();
			return (percentage * full) / 100;
		} else
			throw new NumberFormatException();
	}

	private final Command backCommand = new Command("Back", Command.BACK, 1);

	/** the Page the user is on, or null if it has not been set yet */
	private volatile Page currentPage = null;

	private volatile Widget currentWidget = null;

	private int cursorX;

	private int cursorY;

	private final Command exitCommand = new Command("Exit", Command.EXIT, 2);

	private volatile Widget lastWidget = null;

	/** Page names to the pages themselves <String, Page> */
	private final Hashtable pages = new Hashtable();

	/** <String, String> */
	private final Hashtable properties = new Hashtable();

	// the Commands currently attached (so we can delete them)
	final Vector commands = new Vector();

	private MIDlet midlet;

	/**
	 * @param midlet
	 * @param stream
	 */
	public Application(MIDlet midlet, InputStream stream) {
		this.midlet = midlet;
		try {
			parseApplication(stream);
		} catch (XmlPullParserException e) {
			throw new RuntimeException(e.getMessage());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		} catch (MalformedFileException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public void commandAction(Command c, Displayable d) {
		if (c == exitCommand) {
			System.out.println("Exiting...");
			midlet.notifyDestroyed();
			return;
		}
		// HACK
		String label = c.getLabel();
		if (pages.containsKey(label)) {
			currentPage = (Page) pages.get(label);
			repaint();
		}
	}

	public void paint(Graphics g) {
		// this is where all the rendering goes. We are currently doing the inefficient
		// thing of drawing the background, then drawing the widgets every time.

		// remove old commands
		// ?? this could be abstracted
		Enumeration en = commands.elements();
		while (en.hasMoreElements()) {
			Command command = (Command) en.nextElement();
			removeCommand(command);
		}
		commands.removeAllElements();

		Command navigateCommand;
		if ("exit".equals(currentPage.navigate.toLowerCase())) {
			navigateCommand = exitCommand;
		} else {
			navigateCommand = new Command(currentPage.navigate, Command.ITEM, 2);
		}
		addCommand(navigateCommand);
		commands.addElement(navigateCommand);

		final int x = g.getClipX();
		final int y = g.getClipY();
		final int width = g.getClipWidth();
		final int height = g.getClipHeight();

		// application background
		// TODO: only draw this if the page hasn't set one
		int colour = 0;
		String colourString = (String) currentPage.table.get("background");
		if (colourString == null) {
			// fallback to application setting
			colourString = (String) properties.get("background");
		}
		if (colourString != null) {
			// System.out.println(colourString);
			colour = parseColour(colourString);
		}
		g.setColor(colour);
		g.fillRect(0, 0, getWidth(), getHeight());

		// now draw the widgets
		en = currentPage.widgets.elements();
		while (en.hasMoreElements()) {
			Widget widget = (Widget) en.nextElement();
			AWidget aWidget;
			try {
				aWidget = widget.getImpl();
			} catch (ClassNotFoundException e) {
				System.out.println("Failed to load widget " + widget);
				continue;
			}
			aWidget.setProperties(widget);
			int left = 0;
			int top = 0;
			String leftString = (String) widget.table.get("left");
			if (leftString != null) {
				left = parseSize(leftString, getWidth());
			}
			String topString = (String) widget.table.get("top");
			if (topString != null) {
				top = parseSize(topString, getHeight());
			}
			// ignoring padding
			int widgetWidth = 50;
			int widgetHeight = 50;
			String widthString = (String) widget.table.get("width");
			if (widthString != null) {
				widgetWidth = parseSize(widthString, getWidth());
				// System.out.println("Width: " + widgetWidth);
			}
			String heightString = (String) widget.table.get("height");
			if (heightString != null) {
				widgetHeight = parseSize(heightString, getHeight());
			}
			// translate the graphics object so that the AWidget gets it as it expects
			g.translate(left, top);
			// set the clipping region so that the AWidget cannot draw outside
			g.setClip(0, 0, widgetWidth, widgetHeight);
			aWidget.paint(g, widgetWidth, widgetHeight);
			// undo the translation
			// ?? assuming we were at 0 already
			g.translate(x - g.getTranslateX(), y - g.getTranslateY());
			// undo the clip
			g.setClip(0, 0, width, height);
		}

		// HACK draw the cursor
		g.fillRoundRect(cursorX - 5, cursorY - 5, 10, 10, 5, 5);
	}

	/**
	 * (re-)initiate the application. Called when the application must become "alive" from
	 * a sleeping state.
	 */
	public void run() {
		setFullScreenMode(true);

		// get the active Page
		// TODO re-initialisation code would go here

		// setup the relevant Commands
		// TODO: check hierarchy of page and decide what should exit
		setCommandListener(this);

		// FIXME cursor shown for debugging, need window movement algorithm
		cursorX = getWidth() / 2;
		cursorY = getHeight() / 2;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		Enumeration en = pages.elements();
		while (en.hasMoreElements()) {
			Page page = (Page) en.nextElement();
			buffer.append(page.toString());
			Enumeration widEn = page.widgets.elements();
			while (widEn.hasMoreElements()) {
				Widget widget = (Widget) widEn.nextElement();
				buffer.append(widget.toString());
			}
		}
		return buffer.toString();
	}

	/**
	 * @param stream
	 * @throws XmlPullParserException
	 * @throws IOException
	 * @throws MalformedFileException
	 */
	private void parseApplication(InputStream stream)
			throws XmlPullParserException, IOException, MalformedFileException {
		XmlPullParser parser = new KXmlParser();
		parser.setInput(stream, "UTF-8");
		// read the application
		if ((parser.next() != XmlPullParser.START_TAG)
				|| !"application".equals(parser.getName()))
			throw new MalformedFileException("Did not find an application!");

		int atts = parser.getAttributeCount();
		for (int i = 0; i < atts; i++) {
			String key = parser.getAttributeName(i);
			String value = parser.getAttributeValue(i);
			properties.put(key, value);
		}
		if (!properties.containsKey("entry"))
			throw new MalformedFileException("No entry point for application.");

		// loop that increments elements in the XML stream
		int event = parser.next();
		for (; event != XmlPullParser.END_DOCUMENT; event = parser.next()) {
			if (event != XmlPullParser.START_TAG) {
				// we're only interested in finding "screen" tags
				continue;
			}
			String name = parser.getName();
			if (PAGE.equals(name)) {
				// if it's a PAGE parse it
				Page page = parsePage(parser);
				if (page != null) {
					pages.put(page.getName(), page);
				}
			}
			// else ignore it
		}
		stream.close();

		// set the entry point
		currentPage = (Page) pages.get(properties.get("entry"));
	}

	/**
	 * @param parser
	 * @return
	 * @throws MalformedFileException
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	private Page parsePage(XmlPullParser parser) throws MalformedFileException,
			XmlPullParserException, IOException {
		if (!PAGE.equals(parser.getName()))
			throw new IllegalStateException();

		int atts = parser.getAttributeCount();
		Hashtable table = new Hashtable();
		for (int i = 0; i < atts; i++) {
			String attribute = parser.getAttributeName(i);
			String value = parser.getAttributeValue(i);
			table.put(attribute, value);
		}
		Hashtable widgets = new Hashtable();
		int event = parser.next();
		String navigate = null;
		for (; event != XmlPullParser.END_DOCUMENT; event = parser.next()) {
			if ((event == XmlPullParser.END_TAG)
					&& PAGE.equals(parser.getName()))
				// end of this page
				break;
			if (event != XmlPullParser.START_TAG)
				// we're only interested in "widget" tags
				continue;
			String name = parser.getName();
			if ("widget".equals(name)) {
				// if it's a widget parse it
				Widget widget = parseWidget(parser);
				if (widget != null) {
					widgets.put(widget.getName(), widget);
				}
			} else if ("navigate".equals(name)) {
				// HACK
				int navAttrs = parser.getAttributeCount();
				for (int i = 0; i < navAttrs; i++) {
					String attribute = parser.getAttributeName(i);
					if ("to".equals(attribute)) {
						navigate = parser.getAttributeValue(i);
						break;
					}
				}
			}
			// else ignore it
		}
		Page page = new Page(table, widgets, navigate);
		return page;
	}

	/**
	 * @param parser
	 * @return
	 * @throws MalformedFileException
	 */
	private Widget parseWidget(XmlPullParser parser)
			throws MalformedFileException {
		if (!WIDGET.equals(parser.getName()))
			throw new IllegalStateException();

		int atts = parser.getAttributeCount();
		Hashtable table = new Hashtable();
		for (int i = 0; i < atts; i++) {
			String attribute = parser.getAttributeName(i);
			String value = parser.getAttributeValue(i);
			table.put(attribute, value);
		}

		Widget widget = new Widget(table);
		return widget;
	}

	protected void keyPressed(int keyCode) {
		// this mechanism only handles one key pressed at a time
		// consider using GameCanvas (MIDP2.0) for more flexibility

		// NOTE: this currently moves a cursor. it should really just move the selection
		// between widgets, but this was easier to code up

		int action = getGameAction(keyCode);
		if (action == UP) {
			// System.out.println("UP");
			cursorY -= 10;
			if (cursorY < 0)
				cursorY = 0;
		}
		if (action == LEFT) {
			// System.out.println("LEFT");
			cursorX -= 10;
			if (cursorX < 0)
				cursorX = 0;
		}
		if (action == DOWN) {
			// System.out.println("DOWN");
			cursorY += 10;
			if (cursorY > getHeight())
				cursorY = getHeight();
		}
		if (action == RIGHT) {
			// System.out.println("RIGHT");
			cursorX += 10;
			if (cursorX > getWidth())
				cursorX = getWidth();
		}

		// detect the chosen widget
		// ?? very inefficient way to do this
		// so much code repetition it makes me want to weap like a little girl
		Enumeration en = currentPage.widgets.elements();
		boolean nothingSelected = true;
		while (en.hasMoreElements()) {
			Widget widget = (Widget) en.nextElement();
			int left = 0;
			int top = 0;
			String leftString = (String) widget.table.get("left");
			if (leftString != null) {
				left = parseSize(leftString, getWidth());
			}
			String topString = (String) widget.table.get("top");
			if (topString != null) {
				top = parseSize(topString, getHeight());
			}
			// ignoring padding
			int widgetWidth = 50;
			int widgetHeight = 50;
			String widthString = (String) widget.table.get("width");
			if (widthString != null) {
				widgetWidth = parseSize(widthString, getWidth());
				// System.out.println("Width: " + widgetWidth);
			}
			String heightString = (String) widget.table.get("height");
			if (heightString != null) {
				widgetHeight = parseSize(heightString, getHeight());
			}
			if (((cursorX >= left) && (cursorX <= left + widgetWidth))
					&& ((cursorY >= top) && (cursorY <= top + widgetHeight))) {
				// System.out.println("mouseOver on " + widget.getName());
				lastWidget = currentWidget;
				currentWidget = widget;
				nothingSelected = false;
				if (currentWidget != lastWidget) {
					AWidget currentAWidget;
					try {
						currentAWidget = currentWidget.getImpl();
						currentAWidget.setProperties(currentWidget);
						currentAWidget.onFocus();
					} catch (ClassNotFoundException e) {
						System.out.println("Failed to load widget "
								+ currentWidget);
					}
				}
				break;
			}
		}

		if (nothingSelected) {
			lastWidget = currentWidget;
			currentWidget = null;
		}

		if ((lastWidget != null) && (lastWidget != currentWidget)) {
			AWidget lastAWidget;
			try {
				lastAWidget = lastWidget.getImpl();
				lastAWidget.setProperties(lastWidget);
				lastAWidget.onFocusOut();
			} catch (ClassNotFoundException e) {
				System.out.println("Failed to load widget " + lastWidget);
			}
		}

		if ((action == FIRE) && (currentWidget != null)) {
			// TODO: proper YiibuScript support
			String yiibuScript = (String) currentWidget.table.get("onClick");
			if (yiibuScript != null) {
				// ugly, ugly, ugly!
				int pageBreak = yiibuScript.indexOf('.');
				String pageName = yiibuScript.substring(0, pageBreak);
				int widBreak = yiibuScript.indexOf('.', pageBreak + 1);
				String widName = yiibuScript.substring(pageBreak + 1, widBreak);
				int attBreak = yiibuScript.indexOf('=', widBreak + 1);
				String attName = yiibuScript.substring(widBreak + 1, attBreak);
				int attValueBreak1 = yiibuScript.indexOf('"');
				int attValueBreak2 = yiibuScript.indexOf('"',
						attValueBreak1 + 1);
				String attValue = yiibuScript.substring(attValueBreak1 + 1,
						attValueBreak2);
				System.out.println(pageName + " " + widName + " " + attName
						+ " " + attValue);
				Page page = (Page) pages.get(pageName);
				Widget widget = (Widget) page.widgets.get(widName);
				widget.table.put(attName, attValue);

				AWidget currentAWidget;
				try {
					currentAWidget = currentWidget.getImpl();
					currentAWidget.setProperties(currentWidget);
					currentAWidget.onClick();
				} catch (ClassNotFoundException e) {
					System.out
							.println("Failed to load widget " + currentWidget);
				}
			}
		}

		repaint();
	}

}
