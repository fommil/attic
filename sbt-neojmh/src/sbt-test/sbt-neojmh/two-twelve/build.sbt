scalaVersion in ThisBuild := "2.12.6"

enablePlugins(NeoJmhPlugin)

envVars in Jmh += ("RANDOM_DATA_GENERATOR_SEED" -> "0")
libraryDependencies += "com.danielasfregola" %% "random-data-generator" % "2.4" % "test,jmh"

val sdv = "0.13.1"
addCompilerPlugin("com.fommil" %% "deriving-plugin" % sdv)
libraryDependencies ++= Seq(
  "com.fommil" %% "deriving-macro" % sdv,
  "com.fommil" %% "scalaz-deriving" % sdv
)
