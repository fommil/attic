addCommandAlias("fmt", "all scalafmtSbt scalafmt")

organization := "com.fommil"
startYear := Some(2017)

name := "sbt-neojmh"
sbtPlugin := true

crossSbtVersions := Seq(sbtVersion.value, "0.13.17")

sonatypeGithost := (Gitlab, "fommil", name.value)
licenses := Seq(LGPL3)
sonatypeDevelopers := List("Sam Halliday")

scalafmtConfig in ThisBuild := Some(file("project/scalafmt.conf"))

scriptedBufferLog := false
scriptedLaunchOpts := Seq(
  "-Dplugin.version=" + version.value
)
