ivyLoggingLevel := UpdateLogging.Quiet
scalacOptions in Compile ++= Seq("-feature", "-deprecation")

addSbtPlugin("com.fommil"   % "sbt-sensible" % "2.4.6")
addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "1.6.0-RC4")
