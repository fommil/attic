A minimal and idiomatic alternative to [sbt-jmh](https://github.com/ktoso/sbt-jmh)

To install add as a plugin to `project/plugins.sbt`

```scala
addSbtPlugin("com.fommil" % "sbt-neojmh" % "<version>")
```

where `<version>` is the latest on [maven central](http://search.maven.org/#search|ga|1|g:com.fommil a:sbt-neojmh).

See the tests under `src/sbt-test/sbt-neojmh` for working examples.
