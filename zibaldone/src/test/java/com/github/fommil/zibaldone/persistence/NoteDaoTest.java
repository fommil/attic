/*
 * Created 13-Aug-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.persistence;

import com.github.fommil.zibaldone.Bunch;
import com.github.fommil.zibaldone.Note;
import com.github.fommil.zibaldone.Tag;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 *
 * @author Samuel Halliday
 */
public class NoteDaoTest {

    @Autowired BunchDao bunchDao;

    @Autowired NoteDao noteDao;

    @Test
    public void testDelete() {
        Note a = new Note();
        a.setTitle("a");
        a.setTags(Tag.asTags("smug", "silly"));

        Note b = new Note();
        b.setTitle("b");
        b.setTags(Tag.asTags("silly", "sausage"));

        Note c = new Note();
        c.setTitle("c");
        c.setTags(Tag.asTags("sausage", "chips"));

        Set<Note> notes = Sets.newHashSet(a, b, c);

        noteDao.save(notes);

        Bunch bunch = new Bunch();
        bunch.setTitle("Bunch");
        bunch.setNotes(notes);

        bunchDao.save(bunch);
        
        noteDao.delete(notes);
        Bunch reloadedBunch = bunchDao.findOne(bunch.getId());
        Assert.assertEquals(0, reloadedBunch.getNotes().size());
    }
}
