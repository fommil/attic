/*
 * Created 07-Aug-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone;

import com.github.fommil.zibaldone.importer.OrgModeImporter;
import com.github.fommil.zibaldone.persistence.NoteDao;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Samuel Halliday
 */
public class ReconcilerTest {

    @Autowired Reconciler reconciler;

    @Autowired NoteDao noteDao;

    @Test
    public void testReconcile() throws Exception {
        File file1 = new File("../data/QT2-notes-1.org");
        File file2 = new File("../data/QT2-notes-2.org");

        OrgModeImporter importer = new OrgModeImporter();
        importer.getSettings().setFile(file1);
        List<Note> notes1 = importer.getNotes();

        importer.getSettings().setFile(file2);
        List<Note> notes2 = importer.getNotes();

        UUID uuid = UUID.nameUUIDFromBytes("ReconcilerTest".getBytes());
        reconciler.reconcile(uuid, notes1, Reconciler.SIMPLE_RECONCILE);

        long count = noteDao.count();

        reconciler.reconcile(uuid, notes2, Reconciler.SIMPLE_RECONCILE);

        Assert.assertEquals(count, noteDao.count());
    }
}
