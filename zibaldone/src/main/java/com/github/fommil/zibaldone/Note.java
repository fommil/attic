/*
 * Copyright Samuel Halliday 2011
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.ToString;
import lombok.extern.java.Log;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

/**
 * The atomic element for users: holds a title, rich text and tags.
 *
 * @author Samuel Halliday
 */
@NamedQueries({
        @NamedQuery(
                name = "Note.countForImporter",
                query = "SELECT COUNT(n) FROM Note n WHERE n.source = :source"
        ),
        @NamedQuery(
                name = "Note.allTags",
                query = "SELECT DISTINCT t.text FROM Note n JOIN n.tags t ORDER BY t.text"
        ),
        @NamedQuery(
                name = "Note.forImporter",
                query = "SELECT n FROM Note n WHERE n.source = :source"
        )
})
@Entity
@Data
@Log
@ToString(exclude = "contents")
public class Note implements Serializable {

    private static final int CONTENTS_MAX = 8192;

    @Column
    private UUID source;

    @Id
    private UUID id = UUID.randomUUID();

    @Column
    private String title;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Tag> tags = Sets.newTreeSet();

    @Lob
    @Column(length = CONTENTS_MAX)
    @Basic(fetch = FetchType.EAGER)
    private String contents;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;

    // this might seem superfluous, but it is actually free information
    // that is already in the DB and it makes Note deletion much easier.
    @ManyToMany(mappedBy = "notes", fetch = FetchType.LAZY)
    private Set<Bunch> bunches = Sets.newHashSet();

    // http://stackoverflow.com/a/14911910/1041691
    @PreRemove
    private void removeNoteFromBunches() {
        for (Bunch bunch : bunches) {
            bunch.getNotes().remove(this);
        }
    }

    public void setContents(String contents) {
        if (contents.length() > CONTENTS_MAX) {
            Note.log.warning("Cutting contents of " + toString());
            this.contents = contents.substring(0, CONTENTS_MAX);
        } else {
            this.contents = contents;
        }
    }

    /**
     * @param other
     * @return true if all the properties are the same as this
     */
    public boolean propertiesEquals(Note other) {
        Preconditions.checkNotNull(other);
        if (this == other) {
            return true;
        }
        return Objects.equal(source, other.source)
                && Objects.equal(title, other.title)
                && Objects.equal(tags, other.tags)
                && Objects.equal(contents, other.contents)
                && Objects.equal(dateTime, other.dateTime);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Note) || id == null) {
            return false;
        }
        Note other = (Note) obj;
        return id.equals(other.id);
    }

    @Override
    public int hashCode() {
        Preconditions.checkNotNull(id, "id must be set before @Entity.hashCode can be called");
        return id.hashCode();
    }
}
