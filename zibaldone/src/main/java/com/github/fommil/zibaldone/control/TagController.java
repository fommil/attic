/*
 * Created 19-Jul-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.control;

import com.github.fommil.zibaldone.Tag;
import com.github.fommil.zibaldone.control.Listeners.TagListener;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Controller for {@link Tag} changes.
 * 
 * @author Samuel Halliday
 */
@Component
public class TagController {

    @Autowired
    private Settings settings;

    @Autowired
    private transient List<TagListener> tagListeners;

    public enum TagChoice {

        IGNORE, SHOW, HIDE

    }

    /**
     * @param choice
     * @param tag
     */
    public void selectTag(Tag tag, TagChoice choice) {
        Preconditions.checkNotNull(tag);
        Preconditions.checkNotNull(choice);

        Map<Tag, TagChoice> selected = settings.getSelectedTags();
        boolean change;
        if (choice == TagChoice.IGNORE) {
            change = (selected.remove(tag) != null);
        } else {
            change = (selected.put(tag, choice) != choice);
        }
        if (change) {
            fireTagSelection(tag, choice);
        }
    }


    private void fireTagSelection(Tag tag, TagChoice choice) {
        for (TagListener listener : tagListeners)
            listener.tagSelection(tag, choice);
    }

}
