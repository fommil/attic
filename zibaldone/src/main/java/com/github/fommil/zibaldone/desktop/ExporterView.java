/*
 * Created 14-Aug-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.desktop;

import com.github.fommil.beans.JBeanEditor;
import com.github.fommil.zibaldone.Bunch;
import com.github.fommil.zibaldone.Exporter;
import com.github.fommil.zibaldone.control.BunchController;
import com.google.common.base.Preconditions;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.Box.Filler;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;

/**
 * View for an {@link Exporter}.
 * 
 * @author Samuel Halliday
 */
@Log
@Component
public class ExporterView extends JPanel {

    @Autowired
    private BunchController bunchController;

    private Exporter exporter;

    public ExporterView() {
        initComponents();
    }

    public void setExporter(Exporter exporter) {
        this.exporter = Preconditions.checkNotNull(exporter);
        beanEditor.setBean(exporter.getSettings());
    }

    private void initComponents() {
        JToolBar jToolBar1 = new JToolBar();
        Filler filler1 = new Filler(new Dimension(0, 0), new Dimension(0, 0), new Dimension(32767, 0));
        JButton jButton1 = new JButton();

        setLayout(new BorderLayout());

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.add(filler1);

        jButton1.setText("Export");
        jButton1.setFocusable(false);
        jButton1.setHorizontalTextPosition(SwingConstants.CENTER);
        jButton1.setVerticalTextPosition(SwingConstants.BOTTOM);
        jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        add(jToolBar1, BorderLayout.PAGE_END);
        add(beanEditor, BorderLayout.CENTER);
    }

    private void jButton1ActionPerformed(ActionEvent evt) {
        Collection<Bunch> bunches = bunchController.getBunches();
        try {
            exporter.export(bunches);
        } catch (IOException ex) {
            log.log(Level.WARNING, "Exporter FAIL", ex);
        }
    }

    private JBeanEditor beanEditor = new JBeanEditor();
}
