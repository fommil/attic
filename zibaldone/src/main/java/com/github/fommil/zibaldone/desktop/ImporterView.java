/*
 * Created 31-May-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.desktop;

import com.github.fommil.beans.BeanHelper;
import com.github.fommil.beans.JBeanEditor;
import com.github.fommil.swing.SwingConvenience;
import com.github.fommil.zibaldone.Importer;
import com.github.fommil.zibaldone.control.ImporterController;
import com.github.fommil.zibaldone.control.Settings;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.jdesktop.swingx.JXButton;
import org.jdesktop.swingx.JXTaskPane;
import org.springframework.beans.factory.InitializingBean;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;

/**
 * The specialist GUI widget for interacting with {@link Importer}s.
 *
 * @author Samuel Halliday
 */
@Log
public final class ImporterView extends JXTaskPane implements InitializingBean {

    @Getter @Setter
    private ImporterController importerController;

    @Getter @Setter
    private UUID uuid;

    @Getter @Setter
    private Settings settings;

    public ImporterView() {
        initComponents();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setTitle(importerController.getImporter(uuid).getName());
        Importer.Settings importerSettings = importerController.getImporter(uuid).getSettings();
        BeanHelper beanHelper = new BeanHelper(importerSettings);
        beanHelper.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                // this means a mutable Map value just changed in Settings
                settings.firePropertyChange("importers", null, settings.getImporters());
            }
        });

        jBeanEditor.setBeanHelper(beanHelper);
    }

    @Override
    public Dimension getMaximumSize() {
        Dimension preferred = getPreferredSize();
        preferred.width = Integer.MAX_VALUE;
        return preferred;
    }

    private void initComponents() {
        javax.swing.JPanel jPanel1 = new javax.swing.JPanel();
        javax.swing.JToolBar jToolBar = new javax.swing.JToolBar();
        javax.swing.Box.Filler filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 25), new java.awt.Dimension(0, 1000));
        org.jdesktop.swingx.JXButton jXRemoveButton = new org.jdesktop.swingx.JXButton();
        javax.swing.JPanel jPanel2 = new javax.swing.JPanel();
        javax.swing.JSeparator jSeparator1 = new javax.swing.JSeparator();

        jPanel1.setLayout(new java.awt.BorderLayout());

        jToolBar.setFloatable(false);
        jToolBar.setOrientation(1);

        reloadButton.setText("Load");
        reloadButton.setFocusable(false);
        reloadButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        reloadButton.setMaximumSize(new java.awt.Dimension(54, 18));
        reloadButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        reloadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadButtonActionPerformed(evt);
            }
        });
        jToolBar.add(reloadButton);
        jToolBar.add(filler1);

        jXRemoveButton.setText("Remove");
        jXRemoveButton.setFocusable(false);
        jXRemoveButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jXRemoveButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jXRemoveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jXRemoveButtonActionPerformed(evt);
            }
        });
        jToolBar.add(jXRemoveButton);

        jPanel1.add(jToolBar, java.awt.BorderLayout.EAST);

        jPanel2.setLayout(new java.awt.BorderLayout());
        jPanel2.add(jBeanEditor, java.awt.BorderLayout.CENTER);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jPanel2.add(jSeparator1, java.awt.BorderLayout.EAST);

        jPanel1.add(jPanel2, java.awt.BorderLayout.CENTER);

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }

    private void jXRemoveButtonActionPerformed(java.awt.event.ActionEvent evt) {
        importerController.doRemove(uuid);
        JPanel parent = (JPanel) getParent();
        parent.remove(this);
        parent.revalidate();
    }

    private void reloadButtonActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            importerController.doImport(uuid);
        } catch (IOException ex) {
            log.log(Level.WARNING, "failed import", ex);
            SwingConvenience.warning(this, "There was a problem with the data source.");
        }
    }

    private com.github.fommil.beans.JBeanEditor jBeanEditor = new JBeanEditor();
    private org.jdesktop.swingx.JXButton reloadButton = new JXButton();
}
