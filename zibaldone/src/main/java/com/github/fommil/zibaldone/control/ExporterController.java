/*
 * Created 14-Aug-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.control;

import com.github.fommil.zibaldone.Exporter;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.ServiceLoader;

/**
 * A Controller for implementations of the {@link Exporter} API.
 *
 * @author Samuel Halliday
 */
@Component
public class ExporterController {

    /**
     * @return
     */
    public Collection<Exporter> getExporterImplementations() {
        ServiceLoader<Exporter> exporterService = ServiceLoader.load(Exporter.class);
        Collection<Exporter> exporters = Lists.newArrayList();
        for (Exporter exporter : exporterService) {
            exporters.add(exporter);
        }
        return exporters;
    }
}
