/*
 * Created 27-Jul-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.desktop;

import com.github.fommil.zibaldone.Bunch;
import com.github.fommil.zibaldone.control.BunchController;
import com.github.fommil.zibaldone.control.Listeners;
import com.github.fommil.zibaldone.control.TagController.TagChoice;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.UUID;

/**
 * @author Samuel Halliday
 */
@Log
@Component
public class BunchMenu extends JMenu implements Listeners.BunchListener {

    @Autowired
    private BunchController bunchController;

    // JCheckBoxMenuItem closes JMenu http://stackoverflow.com/questions/3759379
    private final Map<UUID, JCheckBoxMenuItem> entries = Maps.newTreeMap();

    private final JMenuItem none = new JMenuItem("empty");

    {
        none.setEnabled(false);
        add(none);
    }

    @Override
    public void bunchAdded(Bunch bunch) {
        Preconditions.checkNotNull(bunch);

        remove(none);
        final UUID id = bunch.getId();
        final JCheckBoxMenuItem item = new JCheckBoxMenuItem(bunch.getTitle(), false);
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // this is called after the 'selected' flag is changed,
                // which is J2SE breaking the MVC by holding M in the V
                TagChoice choice = TagChoice.SHOW;
                if (!item.isSelected()) {
                    choice = TagChoice.HIDE;
                }
                Bunch update = bunchController.getBunch(id);
                bunchController.selectBunch(update, choice);
            }
        });
        entries.put(id, item);
        rebuild();
    }

    @Override
    public void bunchRemoved(Bunch bunch) {
        Preconditions.checkNotNull(bunch);
        entries.remove(bunch.getId());
        rebuild();
    }

    @Override
    public void bunchUpdated(Bunch bunch) {
        Preconditions.checkNotNull(bunch);
        entries.get(bunch.getId()).setText(bunch.getTitle());
        rebuild();
    }

    @Override
    public void bunchSelectionChanged(Bunch bunch, TagChoice choice) {
        boolean selected = false;
        if (choice == TagChoice.SHOW) {
            selected = true;
        }
        entries.get(bunch.getId()).setSelected(selected);
    }

    private void rebuild() {
        removeAll();
        for (JCheckBoxMenuItem item : entries.values()) {
            add(item);
        }

        if (getMenuComponentCount() == 0) {
            add(none);
        }
    }
}
