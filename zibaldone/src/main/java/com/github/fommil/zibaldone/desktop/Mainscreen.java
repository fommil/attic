/*
 * Created 24-May-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.desktop;

import com.github.fommil.beans.editors.DatePropertyEditor;
import com.github.fommil.beans.editors.FilePropertyEditor;
import com.github.fommil.swing.SwingConvenience;
import com.github.fommil.zibaldone.Exporter;
import com.github.fommil.zibaldone.Importer;
import com.github.fommil.zibaldone.control.*;
import lombok.extern.java.Log;
import org.jdesktop.swingx.JXSearchField;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.jdesktop.swingx.combobox.MapComboBoxModel;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.beans.PropertyEditorManager;
import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * @author Samuel Halliday
 */
@Log
@Component
public final class Mainscreen extends JFrame implements InitializingBean {
    
    public static void main(String args[]) throws Exception {
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        
        PropertyEditorManager.registerEditor(File.class, FilePropertyEditor.class);
        PropertyEditorManager.registerEditor(Date.class, DatePropertyEditor.class);

        final ApplicationContext context =
                new ClassPathXmlApplicationContext("META-INF/application-context.xml");

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Mainscreen main = context.getBean(Mainscreen.class);
                SwingConvenience.enableOSXFullscreen(main);
                main.setVisible(true);
                SwingConvenience.setUncaughtExceptionHandlerPopup(main);
            }
        });
    }
    
    @Autowired private GraphController graphController;
    @Autowired private BunchController bunchController;
    @Autowired private ImporterController importerController;
    @Autowired private ExporterController exporterController;
    @Autowired private Settings settings;
    @Autowired private BunchMenu bunchMenu;
    @Autowired private ExporterMenu exportMenu;
    @Autowired private ExporterView exporterView;
    @Autowired private JungGraphView jungGraphView;
    @Autowired private RelatorMenu relatorMenu;
    @Autowired private TagsView tagSelectView;
    
    public Mainscreen() {
        rootPane.putClientProperty("apple.awt.brushMetalLook", Boolean.TRUE);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initComponents();

        exportMenu.setExporters(exporterController.getExporterImplementations());
        exportMenu.setCallback(new ExporterMenu.Callback() {
            @Override
            public void selectedExporter(Exporter exporter) {
                exporterActionPerformed(exporter);
            }
        });

        // Graph should probably be a managed bean
        jungGraphView.setGraph(graphController.getGraph());

        importerController.loadDb();
        bunchController.loadDb();
    }
    
    private void addImporter(UUID uuid, boolean used) throws Exception {
        // ImporterViews require some runtime info during construction, so we manually create them...
        ImporterView importerView = new ImporterView();
        importerView.setImporterController(importerController);
        importerView.setSettings(settings);
        importerView.setUuid(uuid);
        importerView.setCollapsed(used);
        importerView.afterPropertiesSet();
        importersPanel.add(importerView);
        importersPanel.revalidate();
    }

    private void initComponents() throws Exception {
        javax.swing.JPanel jImportersPanel = new javax.swing.JPanel();
        javax.swing.JToolBar jToolBar1 = new javax.swing.JToolBar();
        org.jdesktop.swingx.JXButton jAddImporterButton = new org.jdesktop.swingx.JXButton();
        javax.swing.Box.Filler filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(20, 0), new java.awt.Dimension(500, 32767));
        javax.swing.JScrollPane jScrollPane1 = new javax.swing.JScrollPane();
        javax.swing.JToolBar jToolBar = new javax.swing.JToolBar();
        javax.swing.JButton tagsButton = new javax.swing.JButton();
        javax.swing.Box.Filler filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(50, 0), new java.awt.Dimension(1000, 0));
        javax.swing.JMenuBar menu = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem importMenu = new javax.swing.JMenuItem();

        tagDialog.setTitle("Tags");
        tagDialog.setAlwaysOnTop(true);
        tagDialog.setResizable(false);

        tagSelectView.setSelectable(true);
        tagSelectView.setWidth(400);
        tagDialog.getContentPane().add(tagSelectView, java.awt.BorderLayout.CENTER);

        importDialog.setTitle("Importers");
        importDialog.setAlwaysOnTop(true);
        importDialog.setModal(true);
        importDialog.setResizable(false);

        jImportersPanel.setLayout(new java.awt.BorderLayout());

        jToolBar1.setFloatable(false);

        jAddImporterButton.setText("+");
        jAddImporterButton.setFocusable(false);
        jAddImporterButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jAddImporterButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jAddImporterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jAddImporterButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(jAddImporterButton);
        jToolBar1.add(importerSelector);
        jToolBar1.add(filler2);

        jImportersPanel.add(jToolBar1, java.awt.BorderLayout.SOUTH);

        jScrollPane1.setBorder(null);
        jScrollPane1.setPreferredSize(new java.awt.Dimension(320, 480));
        jScrollPane1.setViewportView(null);
        jScrollPane1.setViewportView(importersPanel);

        jImportersPanel.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        importDialog.getContentPane().add(jImportersPanel, java.awt.BorderLayout.CENTER);

        exportDialog.setTitle("Export");
        exportDialog.setAlwaysOnTop(true);
        exportDialog.setModal(true);
        exportDialog.setResizable(false);
        exportDialog.getContentPane().add(exporterView, java.awt.BorderLayout.CENTER);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Zibaldone");
        setMinimumSize(new java.awt.Dimension(900, 600));
        setSize(new java.awt.Dimension(800, 600));

        jToolBar.setFloatable(false);
        jToolBar.setRollover(true);
        jToolBar.setPreferredSize(new java.awt.Dimension(480, 42));

        search.setMaximumSize(new java.awt.Dimension(1000, 2147483647));
        search.setMinimumSize(new java.awt.Dimension(100, 28));
        search.setPrompt("Search titles, tags and contents");
        search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchActionPerformed(evt);
            }
        });
        search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchKeyTyped(evt);
            }
        });
        jToolBar.add(search);

        tagsButton.setText("Tags");
        tagsButton.setFocusable(false);
        tagsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tagsButtonActionPerformed(evt);
            }
        });
        jToolBar.add(tagsButton);
        jToolBar.add(filler1);

        getContentPane().add(jToolBar, java.awt.BorderLayout.PAGE_START);
        getContentPane().add(jungGraphView, java.awt.BorderLayout.CENTER);

        fileMenu.setText("File");

        importMenu.setText("Import");
        importMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importMenuActionPerformed(evt);
            }
        });
        fileMenu.add(importMenu);

        exportMenu.setText("Export");
        fileMenu.add(exportMenu);

        menu.add(fileMenu);

        bunchMenu.setText("Bunches");
        menu.add(bunchMenu);

        relatorMenu.setText("Relators");
        menu.add(relatorMenu);

        setJMenuBar(menu);

        Map<String, Class<Importer>> importers = importerController.getImporterImplementations();
        ComboBoxModel importerChoices = new MapComboBoxModel<String, Class<Importer>>(importers);
        importerSelector.setModel(importerChoices);
        for (UUID uuid : settings.getImporters().keySet()) {
            addImporter(uuid, true);
        }
        search.setText(settings.getSearch());

        pack();
    }

    @SuppressWarnings("unchecked")
    private void jAddImporterButtonActionPerformed(java.awt.event.ActionEvent evt) {
        String name = (String) importerSelector.getSelectedItem();
        Entry<UUID, Importer> pair = importerController.newImporter(name);
        settings.getImporters().put(pair.getKey(), pair.getValue());
        try {addImporter(pair.getKey(), false);}
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private void tagsButtonActionPerformed(java.awt.event.ActionEvent evt) {
        tagDialog.pack();
        SwingConvenience.relocateDialogAtMouse(tagDialog);
        tagDialog.setVisible(true);
    }
    
    private void searchActionPerformed(java.awt.event.ActionEvent evt) {
        graphController.searchChanged(search.getText());
    }
    
    private void searchKeyTyped(java.awt.event.KeyEvent evt) {
        char c = evt.getKeyChar();
        if (c == KeyEvent.VK_ENTER || c == KeyEvent.VK_TAB) {
            jungGraphView.requestFocus();
        }
    }
    
    private void importMenuActionPerformed(java.awt.event.ActionEvent evt) {
        importDialog.pack();
        SwingConvenience.relocateDialogAtMouse(importDialog);
        importDialog.setVisible(true);
    }
    
    private void exporterActionPerformed(Exporter exporter) {
        exporterView.setExporter(exporter);
        exportDialog.pack();
        SwingConvenience.relocateDialogAtMouse(exportDialog);
        exportDialog.setVisible(true);
    }

    private JDialog exportDialog = new JDialog();
    private JDialog importDialog = new JDialog();
    private JDialog tagDialog = new JDialog();
    private JComboBox importerSelector = new JComboBox();
    private JXTaskPaneContainer importersPanel = new JXTaskPaneContainer();
    private JXSearchField search = new JXSearchField();
}
