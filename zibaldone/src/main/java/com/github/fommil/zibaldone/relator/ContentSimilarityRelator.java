/*
 * Created 02-Sep-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.relator;

import com.github.fommil.zibaldone.Note;
import com.github.fommil.zibaldone.Relator;

/**
 * {@link Relator} which uses NLP to define a metric of similarity between
 * {@link Note}s.
 *
 * @author Samuel Halliday
 */
public class ContentSimilarityRelator extends AbstractClusteringRelator {

    @Override
    public String getName() {
        return "Similarity";
    }

    @Override
    public double relate(Note a, Note b) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
