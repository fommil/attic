/*
 * Created 29-May-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.control;

import com.github.fommil.zibaldone.Importer;
import com.github.fommil.zibaldone.Note;
import com.github.fommil.zibaldone.Reconciler;
import com.github.fommil.zibaldone.Tag;
import com.github.fommil.zibaldone.control.Listeners.NoteListener;
import com.github.fommil.zibaldone.control.Listeners.TagListener;
import com.github.fommil.zibaldone.control.TagController.TagChoice;
import com.github.fommil.zibaldone.persistence.NoteDao;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Specialist MVC Controller for working with {@link Importer}s.
 * 
 * @author Samuel Halliday
 */
@Log
@Component
public class ImporterController {

    @Autowired
    private NoteDao dao;

    @Autowired
    private Reconciler reconciler;

    @Autowired
    private Settings settings;

    @Autowired
    private transient List<TagListener> tagListeners;

    @Autowired
    private transient List<NoteListener> noteListeners;


    /**
     * @param klass
     * @return indexed by the proposed importer id
     */
    public Map.Entry<UUID, Importer> newImporter(Class<Importer> klass) {
        Preconditions.checkNotNull(klass);

        final Importer importer;
        try {
            importer = klass.newInstance();
        } catch (Exception ex) {
            throw new IllegalArgumentException("Failed to load Importer: " + klass.getName(), ex);
        }
        UUID uuid = UUID.randomUUID();
        return new AbstractMap.SimpleImmutableEntry<UUID, Importer>(uuid, importer);
    }

    /**
     * @return the {@link Importer} implementations, indexed by their name.
     */
    public Map<String, Class<Importer>> getImporterImplementations() {
        ServiceLoader<Importer> importerService = ServiceLoader.load(Importer.class);
        HashMap<String, Class<Importer>> importerImpls = Maps.newHashMap();
        for (Importer importer : importerService) {
            String name = importer.getName();
            @SuppressWarnings("unchecked")
            Class<Importer> klass = (Class<Importer>) importer.getClass();
            importerImpls.put(name, klass);
        }
        return importerImpls;
    }

    /**
     * Convenience to create a new Importer instance by its user "name".
     *
     * @param name
     * @return
     */
    public Map.Entry<UUID, Importer> newImporter(String name) {
        return newImporter(getImporterImplementations().get(name));
    }

    public void loadDb() {
        Set<Tag> tags = Tag.asTags(dao.allTags());
        Set<Note> notes = Sets.newHashSet(dao.findAll());

        fireNotesChanged(notes);
        fireTagsAdded(tags);
        for (Entry<Tag, TagChoice> entry : settings.getSelectedTags().entrySet()) {
            fireTagSelection(entry.getKey(), entry.getValue());
        }
    }

    public Importer getImporter(UUID sourceId) {
        return settings.getImporters().get(sourceId);
    }

    public void doImport(UUID sourceId) throws IOException {

        Set<Tag> tagsBefore = Tag.asTags(dao.allTags());

        List<Note> importedNotes = getImporter(sourceId).getNotes();
        reconciler.reconcile(sourceId, importedNotes, Reconciler.SIMPLE_RECONCILE);

        Set<Tag> tagsAfter = Tag.asTags(dao.allTags());
        diffTags(tagsBefore, tagsAfter);

        Set<Note> notes = Sets.newHashSet(dao.findAll());
        fireNotesChanged(notes);
    }

    public void doRemove(UUID sourceId) {
        Preconditions.checkNotNull(sourceId);
        settings.getImporters().remove(sourceId);
        List<Note> toRemove = dao.forImporter(sourceId);
        dao.delete(toRemove);
        Set<Note> notes = Sets.newHashSet(dao.findAll());
        fireNotesChanged(notes);
    }

    private void diffTags(Set<Tag> before, Set<Tag> after) {
        Set<Tag> removed = Sets.newHashSet();
        for (Tag note : before) {
            if (!after.contains(note)) {
                removed.add(note);
            }
        }
        if (!removed.isEmpty()) {
            fireTagsRemoved(removed);
        }

        Set<Tag> added = Sets.newHashSet();
        for (Tag note : after) {
            if (!before.contains(note)) {
                added.add(note);
            }
        }
        if (!added.isEmpty()) {
            fireTagsAdded(added);
        }
    }

    private void fireTagsAdded(Set<Tag> tags) {
        for (TagListener listener : tagListeners)
            listener.tagsAdded(tags);
    }

    private void fireTagsRemoved(Set<Tag> tags) {
        for (TagListener listener : tagListeners)
            listener.tagsRemoved(tags);
    }

    private void fireTagSelection(Tag tag, TagChoice choice) {
        for (TagListener listener : tagListeners)
            listener.tagSelection(tag, choice);
    }

    private void fireNotesChanged(Set<Note> notes) {
        for (NoteListener listener : noteListeners)
            listener.notesChanged(notes);
    }
}
