/*
 * Copyright Samuel Halliday 2011
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.persistence;

import com.github.fommil.zibaldone.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface NoteDao extends JpaRepository<Note, UUID> {
    List<String> allTags();
    Long countForImporter(@Param("source") UUID source);
    List<Note> forImporter(@Param("source") UUID source);
}