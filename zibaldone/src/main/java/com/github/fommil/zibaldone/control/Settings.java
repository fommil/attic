/*
 * Created 17-Jul-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.control;

import com.github.fommil.collections.ObservableCollection;
import com.github.fommil.collections.ObservableMap;
import com.github.fommil.collections.ObservableSet;
import com.github.fommil.zibaldone.Importer;
import com.github.fommil.zibaldone.Relator;
import com.github.fommil.zibaldone.Tag;
import com.github.fommil.zibaldone.control.TagController.TagChoice;
import com.github.fommil.zibaldone.relator.TagRelator;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.MapperWrapper;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Keeps all the persistent user settings in one place.
 * <p>
 * Note that although this is considered to be the <i>de facto</i> repository
 * of all current settings, it is not the official source for change events.
 * Change support is added here so that the settings can be persisted
 * (for most changes).
 * Objects wishing to be informed of changes should register with the appropriate
 * controller.
 *
 * @author Samuel Halliday
 * @see Listeners
 */
@Getter
@EqualsAndHashCode
@ToString
@Log
@Component
public class Settings implements InitializingBean {

    private static final String PROP_CONNECTIONS = "connections";
    private static final String PROP_SEARCH = "search";
    private static final String PROP_RELATOR = "selectedRelator";

    private final File file = new File("zibaldone.xml");

    @Deprecated // with preference for sparsity value
    private int connections = 500;

    private String search = "";

    private final ObservableMap<Tag, TagChoice> selectedTags = ObservableMap.newObservableTreeMap();

    private final ObservableSet<UUID> selectedBunches = ObservableSet.newObservableHashSet();

    private final ObservableMap<UUID, Importer> importers = ObservableMap.newObservableHashMap();

    private Relator selectedRelator = new TagRelator();

    private final transient java.beans.PropertyChangeSupport propertySupport = new java.beans.PropertyChangeSupport(this);

    {
        ObservableMap.propertyChangeAdapter(propertySupport, selectedTags, "selectedTags");
        ObservableCollection.propertyChangeAdapter(propertySupport, selectedBunches, "selectedBunches");
        ObservableMap.propertyChangeAdapter(propertySupport, importers, "importers");

        // we could potentially add a timer to check if any mutable Map values change without us knowing
        // but that would involve dictating that they implement hashCode/equals. Best handle that as a
        // special case.
    }

    // JAXB is more powerful and quicker, but XStream is a lot easier to use
    // http://www.oracle.com/technetwork/articles/javase/index-140168.html
    // http://jaxb.java.net/tutorial/
    private static final ThreadLocal<XStream> xstreams = new ThreadLocal<XStream>() {
        @Override
        protected XStream initialValue() {
            // see http://pvoss.wordpress.com/2009/01/08/xstream/
            // http://jira.codehaus.org/browse/XSTR-30
            XStream xstream = new XStream() {
                @Override
                protected MapperWrapper wrapMapper(MapperWrapper next) {
                    return new MapperWrapper(next) {
                        @Override
                        public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                            if (definedIn == Object.class) {
                                return false;
                            }
                            return super.shouldSerializeMember(definedIn, fieldName);
                        }
                    };
                }
            };
            xstream.alias("settings", Settings.class);
            xstream.alias("uuid", UUID.class);
            xstream.alias("tag", Tag.class);
            xstream.alias("tagchoice", TagChoice.class);
            xstream.alias("importer", Importer.class);
            xstream.alias("relator", Relator.class);

            // BUG: https://github.com/peichhorn/lombok-pg/issues/118
            xstream.omitField(ObservableCollection.class, "$registeredCollectionListener");
            xstream.omitField(ObservableMap.class, "$registeredMapListener");
            xstream.omitField(Settings.class, "$propertyChangeSupport");
            xstream.omitField(Settings.class, "$propertyChangeSupportLock");
//            try {
//                Field field = Settings.class.getDeclaredField("$propertyChangeSupportLock");
//                log.info("modifiers = " + Modifier.toString(field.getModifiers()));
//
//            } catch (Exception e) {
//                log.log(Level.WARNING, "Reflection FAIL", e);
//            }

            return xstream;
        }
    };

    /**
     * @param file
     * @return 
     */
    public static Settings load(File file) {
        Preconditions.checkNotNull(file);
        Preconditions.checkArgument(file.exists());
        XStream xstream = xstreams.get();
        return (Settings) xstream.fromXML(file);
    }

    /**
     * @param file
     * @throws IOException 
     */
    public void save(File file) throws IOException {
        Preconditions.checkNotNull(file);
        XStream xstream = xstreams.get();
        String xml = xstream.toXML(this);
        Files.write(xml, file, Charsets.UTF_8);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (file.exists()) {
            Settings loaded = load(file);
            // it's this or implement readResolve
            connections = loaded.connections;
            importers.putAll(loaded.importers);
            search = loaded.search;
            selectedBunches.addAll(loaded.selectedBunches);
            selectedTags.putAll(loaded.selectedTags);
            selectedRelator = loaded.selectedRelator;
        }

        addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                try {
                    log.fine("Saving " + file.getName() + ": " + evt.getPropertyName());
                    save(file);
                } catch (IOException e) {
                    log.log(Level.WARNING, "Problem saving " + file.getName(), e);
                }
            }
        });
    }

    public void addPropertyChangeListener(final java.beans.PropertyChangeListener listener) {
        this.propertySupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(final java.beans.PropertyChangeListener listener) {
        this.propertySupport.removePropertyChangeListener(listener);
    }

    public void setConnections(final int connections) {
        final int old = this.connections;
        this.connections = connections;
        this.propertySupport.firePropertyChange(PROP_CONNECTIONS, old, this.connections);
    }

    public void setSearch(final String search) {
        final String old = this.search;
        this.search = search;
        this.propertySupport.firePropertyChange(PROP_SEARCH, old, this.search);
    }

    public void setSelectedRelator(final Relator relator) {
        final Relator old = this.selectedRelator;
        this.selectedRelator = relator;
        this.propertySupport.firePropertyChange(PROP_RELATOR, old, this.selectedRelator);
    }

    public void firePropertyChange(String property, Object old, Object update) {
        this.propertySupport.firePropertyChange(property, old, update);
    }

}