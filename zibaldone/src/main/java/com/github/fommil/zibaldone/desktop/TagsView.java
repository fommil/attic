/*
 * Created 04-Jul-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.desktop;

import com.github.fommil.swing.WrapLayout;
import com.github.fommil.zibaldone.Tag;
import com.github.fommil.zibaldone.control.Listeners.TagListener;
import com.github.fommil.zibaldone.control.TagController;
import com.github.fommil.zibaldone.control.TagController.TagChoice;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.Set;
import java.util.SortedMap;

/**
 * Shows tags and provides programmatic options to enable tag selection,
 * deletion and creation. The user sets the width of this and the height
 * is automatically calculated based on the tags and the inherited font.
 * 
 * @author Samuel Halliday
 */
@Log
@Component
public class TagsView extends JPanel implements TagListener {

    private final class TagView extends JLabel {

        @Getter
        private final Tag tag;

        @Getter
        private TagChoice choice;

        public TagView(Tag tag, TagChoice choice) {
            super(tag.getText());
            setLayout(new WrapLayout());
            this.tag = tag;
            setOpaque(true);
            setChoice(Preconditions.checkNotNull(choice));
            setFont(TagsView.this.getFont());
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    click();
                }
            });
        }

        private void click() {
            if (!selectable) {
                return;
            }
            tagController.selectTag(tag, nextChoice());
        }

        public void setChoice(TagChoice choice) {
            this.choice = Preconditions.checkNotNull(choice);
            setBackground(getColorForChoice(choice));
            repaint();
        }

        private TagChoice nextChoice() {
            switch (choice) {
                case IGNORE:
                    return TagChoice.SHOW;
                case SHOW:
                    return TagChoice.HIDE;
                default:
                    return TagChoice.IGNORE;
            }
        }

        private Color getColorForChoice(TagChoice choice) {
            switch (choice) {
                case IGNORE:
                    return null;
                case SHOW:
                    return Color.GREEN;
                default:
                    return Color.RED;
            }
        }
    }

    @Autowired
    private TagController tagController;

    @Getter @Setter
    private boolean selectable;

    public TagsView() {
        super(new WrapLayout());
    }

    private final SortedMap<Tag, TagView> views = Maps.newTreeMap();

    @Override
    public void tagsAdded(Set<Tag> tags) {
        for (Tag tag : tags) {
            if (!views.containsKey(tag)) {
                TagView view = new TagView(tag, TagChoice.IGNORE);
                views.put(tag, view);
            }
        }
        redraw();
    }

    @Override
    public void tagsRemoved(Set<Tag> tags) {
        Set<Tag> toGo = Sets.newHashSet();
        for (Tag tag : views.keySet()) {
            if (!tags.contains(tag)) {
                toGo.add(tag);
            }
        }
        for (Tag tag: toGo)
            views.remove(tag);
        redraw();
    }

    @Override
    public void tagSelection(Tag tag, TagChoice choice) {
        Preconditions.checkNotNull(tag);
        Preconditions.checkNotNull(choice);

        views.get(tag).setChoice(choice);
    }

    /**
     * @return
     */
    public Set<Tag> getTags() {
        return Collections.unmodifiableSet(views.keySet());
    }

    private void redraw() {
        removeAll();
        for (TagView view : views.values()) {
            add(view);
        }
        revalidate();
        repaint();
    }

    /**
     * @param width
     */
    public void setWidth(int width) {
        setSize(width, getSize().height);
    }

    @Override
    public void setFont(Font font) {
        super.setFont(font);
        if (views != null) {
            for (TagView view : views.values()) {
                view.setFont(font);
            }
        }
    }
}
