/*
 * Created 19-Jul-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.control;

import com.github.fommil.zibaldone.Bunch;
import com.github.fommil.zibaldone.Note;
import com.github.fommil.zibaldone.control.Listeners.BunchListener;
import com.github.fommil.zibaldone.control.TagController.TagChoice;
import com.github.fommil.zibaldone.persistence.BunchDao;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Controller for {@link Bunch} instances.
 * 
 * @author Samuel Halliday
 */
@Log
@Component
public class BunchController {

    public static final String NEW_BUNCH_TITLE = "New Bunch";

    @Autowired
    private Settings settings;

    @Autowired
    private BunchDao dao;

    @Autowired
    private transient List<BunchListener> bunchListeners;

    public Collection<Bunch> getBunches() {
        return dao.findAll();
    }

    public Set<Bunch> getBunches(Collection<UUID> ids) {
        // https://jira.springsource.org/browse/DATAJPA-332
        if (ids.isEmpty())
            return Collections.emptySet();
        return Sets.newHashSet(dao.findAll(ids));
    }

    public Bunch getBunch(UUID bunchId) {
        return dao.findOne(bunchId);
    }

    /**
     * @param notes 
     */
    public void newBunch(Set<Note> notes) {
        Preconditions.checkNotNull(notes);
        Preconditions.checkArgument(!notes.isEmpty());

        Bunch bunch = new Bunch();
        bunch.setTitle(NEW_BUNCH_TITLE);
        bunch.setNotes(notes);
        dao.save(bunch);

        fireBunchAdded(bunch);
        selectBunch(bunch, TagChoice.SHOW);
    }

    public void removeBunch(Bunch bunch) {
        Preconditions.checkNotNull(bunch);

        dao.delete(bunch);

        fireBunchRemoved(bunch);
    }

    /**
     * @param bunch 
     */
    public void updateBunch(Bunch bunch) {
        Preconditions.checkNotNull(bunch);

        Bunch update = dao.save(bunch);

        fireBunchUpdated(update);
    }

    /**
     * @param bunch
     * @param choice 
     */
    public void selectBunch(Bunch bunch, TagChoice choice) {
        boolean change;
        switch (choice) {
            case HIDE:
                change = settings.getSelectedBunches().remove(bunch.getId());
                break;
            case SHOW:
                change = settings.getSelectedBunches().add(bunch.getId());
                break;
            default:
                throw new UnsupportedOperationException("only HIDE and SHOW supported");
        }
        if (change) {
            fireBunchSelectionChanged(bunch, choice);
        }
    }

    public void loadDb() {
        List<Bunch> all = dao.findAll();
        for (Bunch bunch : all) {
            fireBunchAdded(bunch);
            if (settings.getSelectedBunches().contains(bunch.getId())) {
                fireBunchSelectionChanged(bunch, TagChoice.SHOW);
            }
        }
    }

    private void fireBunchAdded(Bunch bunch) {
        for (BunchListener listener : bunchListeners)
            listener.bunchAdded(bunch);
    }

    private void fireBunchRemoved(Bunch bunch) {
        for (BunchListener listener : bunchListeners)
            listener.bunchRemoved(bunch);
    }

    private void fireBunchUpdated(Bunch bunch) {
        for (BunchListener listener : bunchListeners)
            listener.bunchUpdated(bunch);
    }

    private void fireBunchSelectionChanged(Bunch bunch, TagChoice choice) {
        for (BunchListener listener : bunchListeners)
            listener.bunchSelectionChanged(bunch, choice);
    }
}
