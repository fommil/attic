/*
 * Created 01-Sep-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.desktop;

import com.github.fommil.zibaldone.Relator;
import com.github.fommil.zibaldone.control.GraphController;
import com.github.fommil.zibaldone.control.Settings;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Samuel Halliday
 */
@Component
public class RelatorMenu extends JMenu implements InitializingBean {

    private final JMenuItem none = new JMenuItem("empty");

    @Autowired
    private GraphController graphController;

    @Autowired
    private Settings settings;

    {
        none.setEnabled(false);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        String selected = settings.getSelectedRelator().getName();
        Map<String, Relator> relators = graphController.getRelators();
        ButtonGroup group = new ButtonGroup();
        for (Entry<String, Relator> entry : relators.entrySet()) {
            String name = entry.getKey();
            final Relator relator = entry.getValue();
            final JRadioButtonMenuItem item = new JRadioButtonMenuItem(name, selected.equals(name));
            group.add(item);

            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (item.isSelected()) {
                        graphController.selectRelator(relator);
                    }
                }
            });
            add(item);
        }

        if (getMenuComponentCount() == 0) {
            add(none);
        }
    }
}
