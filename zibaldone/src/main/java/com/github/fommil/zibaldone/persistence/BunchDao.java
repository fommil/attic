/*
 * Created 10-Jul-2012
 * 
 * Copyright Samuel Halliday 2012
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package com.github.fommil.zibaldone.persistence;

import com.github.fommil.zibaldone.Bunch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BunchDao extends JpaRepository<Bunch, UUID> {
}