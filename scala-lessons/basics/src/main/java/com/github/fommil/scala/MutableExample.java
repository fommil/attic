package com.github.fommil.scala;

import java.util.ArrayList;
import java.util.List;

public class MutableExample {

  public static void main(String[] arg) {
    List<String> list = new ArrayList<String>();
    list.add("Gobo");
    System.out.println(list);
    // -> "Gobo"

    list.add("Wembly");
    System.out.println(list);
    // -> "Gobo, Wembly"
  }
}
