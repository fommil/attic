package com.github.fommil.scala;

import com.google.common.base.Stopwatch;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author Sam Halliday
 */
public class LoopSpeed {

    public static void main(String[] args) {
//        for (int i = 0; i < 100; i++) {
//            int[] array = new int[1000000];
//            for (int j = 0; j < array.length; j++)
//                array[j] = j;
//            Stopwatch watch = Stopwatch.createStarted();
//                for (int j = 0; j < array.length; j++) {
//                    synchronized (array) {
//                    int k = array[j];
//                }
//            }
//            System.out.println(watch);
//        }
//        for (int i = 0; i < 100; i++) {
//            int[] array = new int[1000000];
//            for (int j = 0; j < array.length; j++)
//              array[j] = j;
//            int[] order = new int[1000000];
//            Random random = new Random();
//            for (int j = 0; j < order.length; j++)
//              order[j] = random.nextInt(order.length);
//            Stopwatch watch = Stopwatch.createStarted();
//            for (int j = 0; j < array.length; j++) {
//                synchronized(array) {
//              int k = array[order[j]];
//                }
//            }
//            System.out.println(watch); // 500μs
//        }



        UUID[] array = new UUID[1000000];
        for (int j = 0; j < array.length; j++)
            array[j] = UUID.randomUUID();
        for (int i = 0; i < 10; i++) {
            Stopwatch watch = Stopwatch.createStarted();
            Set<UUID> set = Collections.synchronizedSet(new HashSet<UUID>());
            for (int j = 0; j < array.length; j++)
              set.add(array[j]);
            System.out.println(watch); // 200ms
        }

    }
}
