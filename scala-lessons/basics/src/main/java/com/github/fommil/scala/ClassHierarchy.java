package com.github.fommil.scala;

import java.util.Set;

interface Enemy {
    boolean surrender(String warning);
    void die();
}

interface Klingon { void attack(Enemy enemy); }

abstract class TngKlingon implements Klingon {
    private static final String WARNING = "bIjeghbe'chugh vaj bIHegh";
    public static boolean isGoodDayToDie() { return true; }
    @Override
    public void attack(Enemy enemy) {
        if (!isGoodDayToDie())
            return;
        if (enemy.surrender(WARNING))
            return;
        enemy.die();
        celebrate();
    }
    abstract void celebrate();
}

class Worf extends TngKlingon {
    @Override
    void celebrate() { brood(); }
    void brood() { System.out.println("I am NOT sulking!"); }
}

interface Reveller<T> {
    void celebrate(Set<? extends T> others);
}

class BigParty {
    void bang(Set<TngKlingon> klingons, Reveller<Klingon> reveller) {
        reveller.celebrate(klingons);
    }
}