package com.github.fommil.scala;

import java.util.List;

public class LoopExample {
  int howManyRadishes(List<Fraggle> fraggles) {
    int count = 0;
    for (Fraggle fraggle : fraggles) {
      String food = fraggle.favouriteFood();
      if (food.equals("Radish")) {
        count++;
      }
    }
    return count;
  }
}
