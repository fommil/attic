package com.infiniteskills.scala

import com.github.fommil.scala.Pirate

trait Monkey

package pop {
  package caribbean {
    object Jack extends Pirate (
      "Jack", "Barbossa"
    ) with Monkey
  }
  package sahara {
    object Rafiki extends Monkey
  }
}

package real {
  class Bonobo extends Monkey
  class Chimpanzee extends Monkey
}

object `package` {
  type Monkeys = List[Monkey]

  import pop._
  import caribbean.Jack
  import sahara.Rafiki

  def barrel(): Monkeys = List(Jack, Rafiki)
}

package object linux {
  object Bonobo
}


object MonkeyOption1 {
  import real.{Bonobo => BonoboMonkey, Chimpanzee => Chimp}
  import linux._
}

object MonkeyOption2 {
  import real.{Bonobo => _, _}
  import linux._
}

import java.lang._
import scala._
import Predef._
