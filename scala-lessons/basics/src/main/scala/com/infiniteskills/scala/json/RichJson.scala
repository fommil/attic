package com.infiniteskills.scala.json

import spray.json._
import scala.collection.immutable.ListMap

object RichJsonApp extends App {

  import implicits._

  val tree = JsObject {
      ("name", JsString("Samurai Jack")) ::
      ("robot", JsFalse) ::
      ("appears", JsNumber("48")) ::
      ("powers", JsArray(JsTrue, JsFalse, JsNull)) ::
      Nil
  }

  false

  48
  null

  val dslTree =
      "name" :> JsString("Samurai Jack") <>
      "robot" :> JsFalse <>
      "appears" :> JsNumber("48") <>
      "powers" :> JsTrue ~: JsFalse ~: JsNull

  println(dslTree.prettyPrint)

  def stuff(in: (String, Boolean)*) = ListMap(in:_*)


  val blah = ("a", true) :: Nil
  ListMap(blah:_*)

  val symbol = 'Vendetta
  symbol == 'Vendetta
}

package object implicits {

  implicit class JsObjectBuilder(key: String) {
    def :>(that: JsValue) = JsObject((key, that))
  }

  implicit class JsObjectMappend(obj: JsObject) {
    def <>(that: JsObject) = JsObject(obj.fields ++ that.fields)
  }

  implicit class JsArrayBuilderA(obj: JsArray) {
    def ~:(that: JsValue) = JsArray(that :: obj.elements)
  }

  implicit class JsArrayBuilderB(obj: JsValue) {
    def ~:(that: JsValue) = JsArray(that, obj)
  }
}