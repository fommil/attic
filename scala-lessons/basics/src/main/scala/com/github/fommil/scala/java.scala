package com.github.fommil.scala

import scala.util.matching.Regex
import java.util
import java.util.Date
import scala.beans.BeanProperty

object AccessJava extends App {

  class Goonie extends Enemy {
    def surrender(warning: String) = true
    def die() = ??? // Goonies never say die
  }

  val worf = new Worf
  val mikey = new Goonie
  worf attack mikey


  // checking
  println(TngKlingon.isGoodDayToDie)


  "to be sure " * 3 // "to be sure to be sure to be sure "
  val detectMovie: Regex = "(?i)Star Trek [0-9IV]+" r
  "spock".capitalize // Spock

  import collection.JavaConverters._

  class KlingonWarrior(friends: Iterable[Klingon])
    extends TngKlingon with Reveller[Klingon] {

    def celebrate(others: java.util.Set[_ <: Klingon]) =
      celebrate(others.asScala)

    def celebrate() =
      celebrate(friends)

    def celebrate(others: Iterable[Klingon]) = ???
  }

  class Person(@BeanProperty var name: String,
               @BeanProperty var dob: Date)

}