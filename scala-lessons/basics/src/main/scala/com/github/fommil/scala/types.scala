package com.github.fommil.scala

trait Types[K, V, T] {
  //val a: Array[T]
  //val b: Map[K, V]
  //type People = Array[Person]
  //val c: Array[People]

  //    val a  = Array("hello", "world")
  //    val b: Array[Any] = a
  //    b(1) = 42

}

trait Doctor

trait ScottishDoctor extends Doctor

trait Dalek[-T] {
  def exterminate(subject: T) { }
}

trait DalekApp {

  val doctor: Doctor
  def conquer(dalek: Dalek[Doctor]) {
    dalek.exterminate(doctor)
  }

  val dalek: Dalek[Doctor]
  conquer(dalek)

  val omni: Dalek[Any]
  conquer(omni)

  val specialist: Dalek[ScottishDoctor]
//  conquer(specialist)

}

trait MyList[+T] {
  def head: T = ???

  def tail: MyList[T] = ???

  def prepend[U >: T](e: U): MyList[U] = new MyLink(e, this)
}

object MyEmptyList extends MyList[Nothing]

class MyLink[+T](override val head: T,
                 override val tail: MyList[T]) extends MyList[T]

class SinisterRobot(id: String) extends Robot(id) {
  def destroy(human: Human) {
    println("muahahaha!")
  }
}

object LowerBounds {
  val friend = new FriendlyRobot("Marvin")
  val sinister = new SinisterRobot("Megatron")
  val list = MyEmptyList.prepend(friend).prepend(sinister)
}

trait RobotChecker {
  def check[R <: Robot with Named](r: R): R
}


class Witch {
  def floatOnWater() {}
}

class Duck {
  def floatOnWater() {}
}

class WitchHunter {
  def burn(witch: {def floatOnWater(): Unit}) {}
}

object WitchHunterApp extends App {
  val hunter = new WitchHunter
  hunter.burn(new Duck)
  hunter.burn(new Witch)
}


class Fixer {
  def repair(robots: List[Robot]) = robots match {
    case a: List[FriendlyRobot] =>
    case b: List[SinisterRobot] => println("muahahaha!")
    case _ =>
  }
}

object FixerApp extends App {
  val fixer = new Fixer

  fixer.repair(List(new FriendlyRobot("Marvin")))
  // nothing

  fixer.repair(List(new SinisterRobot("Megatron")))
  // nothing
}
