package com.github.fommil.scala

object ImmutableExample extends App {

  {
    val empty = List[String]()
    val listA = "Gobo" :: empty
    println(empty) // List()
    println(listA) // List(Gobo)

    val listB = "Wembly" :: listA
    println(empty) // List()
    println(listA) // List(Gobo)
    println(listB) // List(Wembly, Gobo)
  }
  {
    var list = List[String]()
    list = "Gobo" :: list
    println(list) // List(Gobo)

    list = "Wembly" :: list
    println(list) // List(Wembly, Gobo)
  }

}
