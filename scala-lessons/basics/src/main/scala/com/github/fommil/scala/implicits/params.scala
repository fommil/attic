package com.github.fommil.scala.implicits

import com.infiniteskills.scala.Monkey

import languageFeature.postfixOps
import com.infiniteskills.scala.real.{Chimpanzee, Bonobo}
import scala.reflect.ClassTag

trait Factory[T] {
  def create: T
}

class SpaceTravel {
  def testFlight[T](count: Int)
                   (implicit factory: Factory[T]) =
    launch {
      (1 to count) map { _ => factory create }
    }

  //  def testFlight[T: Factory](count: Int) =
  //    launch {
  //      (1 to count) map { _ => implicitly[Factory[T]] create }
  //    }

  private def launch[T](astros: Seq[T]) = ???
}


object BuildYourOwnMonkeyApp extends App {

  println(
    List(3, 1, 2).sorted == List(1, 2, 3)
  )

  def square[T](x: T)(implicit n: Numeric[T]) =
    n.times(x, x)

  def mkArray[T: ClassTag](elems: T*) = Array[T](elems: _*)

//  def inspect[T](list: List[T])
//    // T is erased at runtime

  def inspect[T](list: List[T])(implicit unerase: ClassTag[T]) =
    unerase.runtimeClass.getName

  println(inspect(List("E", "A", "D", "G")))
  println(inspect(List(327, 1138)))


  val factory = new Factory[Monkey] {
    def create = new Monkey {}
  }
  val jupiter = new SpaceTravel
  jupiter.testFlight(1)(factory)


  implicit val monkeys = factory
  jupiter.testFlight(1)

  implicitly[Factory[Monkey]] == factory

  {
    implicit val monkeys = factory
    implicitly[Factory[Monkey]] == factory
  }
}

object ImplicitMonkeys extends App {
  implicit val bonobos = new Factory[Bonobo] {
    def create = new Bonobo
  }
  implicit val chimps = new Factory[Chimpanzee] {
    def create = new Chimpanzee
  }

  implicitly[Factory[Bonobo]] == bonobos
  implicitly[Factory[Chimpanzee]] == chimps

  //  implicitly[Factory[Monkey]]
}