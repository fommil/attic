package com.github.fommil.scala.implicits

import com.infiniteskills.scala.Monkey
import com.github.fommil.scala.{RobotChecker, FriendlyRobot, Pirate, Robot}
import com.infiniteskills.scala.real.Chimpanzee

object RoboApp extends App {

  def killAllHumans(robot: Robot) { }

  implicit def robo(name: String) = new FriendlyRobot(name)

  killAllHumans("Bender Bending Rodriquez")


  1 to 10 foreach println
  1 until 11 foreach println


  trait Monolith
  object TMA0 extends Monolith

  implicit class MonkeyPimp(monkey: Monkey) {
    def touch(m: Monolith) {
      println("daaa Daaa DAA DUN DUNNNNNNN!")
    }
  }

  new Chimpanzee touch TMA0

  def square[T](x: T)(implicit n: Numeric[T]) = {
    import n._
    x * x
  }



  2 * 10.0


//  def killAllHumansAndReturn[A](convertable: A)
//                               (implicit converter: A => Robot) = {
//    killAllHumans(convertable)
//    convertable
//  }

  def killAllHumansAndReturn[T <% Robot](convertable: T) = {
    killAllHumans(convertable)
    convertable
  }

}

