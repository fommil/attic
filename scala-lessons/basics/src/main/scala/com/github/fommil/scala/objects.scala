package com.github.fommil.scala

trait Named {
  def fullName(): String = firstName + " " + lastName
  val firstName: String
  val lastName: String
}


//trait NameHelper {
//  this: Named =>
//
//  override def fullName(): String = firstName + " " + lastName
//
//  protected def capitalize(s: String) = s(0).toUpper + s.drop(1)
//
//  println("Initializing NameHelper")
//}


trait Great {
  this: Named =>

  override def fullName() = firstName + " the Great"
}

trait Parrot {
  this: {def fullName(): String} =>

  def parrot() = fullName() + "'s parrot"
}

//class Pirate(first: String,
//             last: String)
//  extends Named
//  with Great with Parrot {
//
//  val firstName = first
//  val lastName = last
//
//}

class Pirate(val firstName: String,
             val lastName: String)
  extends Named
  with Great with Parrot {
  require(!firstName.isEmpty())
}

object Pirate {
  implicit object Ordering extends Ordering[Pirate] {
    def compare(x: Pirate, y: Pirate) = x.firstName compare y.firstName
  }
}


object PirateApp extends App {
  val leChuck = new Pirate("LeChuck", "Threepwood")
  println(leChuck.parrot())
  // LeChuck the Great's parrot
}

trait Strange {
  val spooky = "ghost"
}

trait Frightening {
  val spooky: String
  //lazy
  val scare = spooky.toUpperCase
}


object FrightApp extends App {

  val a = new Strange with Frightening
  println(a.scare) // GHOST

  val b = new Frightening with Strange
  // NullPointerException
  println(b.scare)
}




trait Eerie extends Named
  with Great with Parrot
  with Strange with Frightening

trait Human

abstract class Robot(id: String) {
  def destroy(human: Human): Unit
}

class FriendlyRobot(id: String) extends Robot(id) {
  def destroy(human: Human) { }
}

final class VeryFriendlyRobot(id: String) extends FriendlyRobot(id) {
  override def destroy(human: Human) {
    super.destroy(human)
    println("I am " + id + ", human cyborg relations.")
  }
}

object Tuples {
  val robot = new FriendlyRobot("Johnny Five")
  val pirate = new Pirate("LeChuck", "Threepwood")

  val roboPirate: (Robot, Pirate) = (robot, pirate)

  roboPirate._1 // robot
  roboPirate._2 // pirate

  val (r, p) = roboPirate
  r == roboPirate._1 == robot
  p == roboPirate._2 == pirate

}