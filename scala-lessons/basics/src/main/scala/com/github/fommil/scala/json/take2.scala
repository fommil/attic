package com.github.fommil.scala

import spray.json._

package object json {

  implicit class RichJsValue(self: JsValue) {
    def ::(that: String): JsField = (that, self)
  }

  def j(fields: JsField*) = JsObject(fields: _*)
  def j(elements: JsValue*) = JsArray(elements: _*)
}

import json._

object jsonDslApp extends App {
  val tree = j(
    "name"    :: JsString("Samurai Jack"),
    "robot"   :: JsFalse,
    "appears" :: JsNumber("48"),
    "powers"  :: j(JsTrue, JsFalse, JsNull)
  )

  println(tree.prettyPrint)
}
