package com.github.fommil.scala.implicits

import com.github.fommil.scala.{Pirate, Ninja}
import com.infiniteskills.scala.Monkey
import com.infiniteskills.scala.real.Bonobo

package object aliens {

  implicit def asSpacePirate(alien: Alien) = new Pirate("Han", "Solo")

  class Alien

  object Alien {
    implicit def asNinja(alien: Alien) = new Ninja(Int.MaxValue)
    implicit def fromMonkey(monkey: Monkey) = new Alien
  }

}

import aliens.Alien

object ImplicitRulesApp extends App {

  val scorpion = new Ninja(100)
  val predator = new Alien

  scorpion fight predator

  println((scorpion fight predator) != predator)


  val han = new Alien
  println(han fullName)

  println(1 < scorpion)
  println(scorpion fight 1)


  println(
    List(
      new Ninja(1),
      new Ninja(2)
    ).sorted
  )

  println(
    List(
      new Pirate("Guybrush", "Threepwood"),
      new Pirate("LeChuck", "Threepwood")
    ).sorted
  )


  def arrange[T](a: T, b: T)(implicit ordering: Ordering[T]) =
    if (ordering.compare(a, b) <= 0) (a, b)
    else (b, a)

  arrange(
    new Ninja(1),
    new Ninja(2)
  )

  val aliens = List[Alien](new Bonobo, new Alien)
}

object Chaining1App extends App {
  class A(val i : Int)
  class B(val i : Int)
  class C(val i : Int) {
    def cube = i * i * i
  }

  implicit def toA(n: Int): A = new A(n)
  implicit def aToB(a: A): B = new B(a.i)
  implicit def bToC(b: B): C = new C(b.i)

  new C(13).cube
  new B(13).cube
//  new A(13).cube
}

object Chaining2App extends App {
  class A(val i : Int)
  class B(val i : Int)
  class C(val i : Int) {
    def cube = i * i * i
  }

  //  implicit def aToB[A1 <% A](a: A1): B = new B(a.i)
  //  implicit def bToC[B1 <% B](b: B1): C = new C(b.i)
  implicit def toA(n: Int): A = new A(n)
  implicit def aToB[A1](a: A1)(implicit v:A1=>A): B = new B(a.i)
  implicit def bToC[B1](b: B1)(implicit v:B1=>B): C = new C(b.i)

  new C(13).cube
  new B(13).cube
  new A(13).cube
  13 cube
}