package com.github.fommil.scala

import com.infiniteskills.scala.Monkey
import com.infiniteskills.scala.pop.caribbean.Jack
import com.github.fommil.scala.implicits.aliens.Alien
import scala.collection.mutable
import scala.util.Random
import scala.Stream
import scala.collection.immutable.HashSet
import scala.Stream

//trait Any {
//  def equals(that: Any): Boolean
//  def hashCode: Int
//
//  def toString: String
//
//  final def ==(that: Any) = equals(that)
//  final def !=(that: Any) = !equals(that)
//  final def ## = hashCode()
//
//  final def getClass: Class[_]
//  final def asInstanceOf[T0]: T0
//  final def isInstanceOf[T0]: Boolean
//}
//
//trait AnyRef extends Any {
//  final def eq(that: AnyRef): Boolean
//  final def ne(that: AnyRef) = !(this eq that)
//
//  final def synchronized[T](block: ⇒ T): T
//
//  final def notify(): Unit
//  final def notifyAll(): Unit
//  final def wait(): Unit
//  final def wait(arg0: Long, arg1: Int): Unit
//  final def wait(arg0: Long): Unit
//}

package object wrapped {

  implicit class WrappedNinja(val wrapped: Ninja) extends AnyVal {
    def withFamiliar(monkey: Monkey) = wrapped train 1
  }

}

object WrappedNinjaApp extends App {

  import wrapped._

  new Ninja(10) withFamiliar Jack

  new Ninja(10) train 1

  def upgrade(ninja: Ninja, extra: Option[Monkey]) =
    if (extra.isEmpty) ninja
    else ninja withFamiliar extra.get

  def double(opt: Option[Int]) = opt map {_ * 2} getOrElse 0

  double(Some(10)) // 20
  double(None) // 0

  //  sealed abstract class Option[+A] {
  //    def isEmpty: Boolean
  //    def isDefined: Boolean = !isEmpty
  //
  //    def get: A
  //    def getOrElse[B >: A](default: => B): B =
  //      if (isEmpty) default else this.get
  //
  //    def map[B](f: A => B): Option[B] =
  //      if (isEmpty) None else Some(f(this.get))
  //    def flatMap[B](f: A => Option[B]): Option[B] =
  //      if (isEmpty) None else f(this.get)
  //  }
  //  object Option {
  //    def apply[A](x: A): Option[A] = if (x == null) None else Some(x)
  //  }
  //
  //  object None extends Option[Nothing] {
  //    def isEmpty = true
  //    def get = throw new NoSuchElementException("None.get")
  //  }
  //  final case class Some[+A](x: A) extends Option[A] {
  //    def isEmpty = false
  //    def get = x
  //  }

  // made available by scala package object
  Traversable(1, 2, 3)
  Iterable("x", "y", "z")
  Map("x" -> 24, "y" -> 25, "z" -> 26)
  Set("red", "green", "blue")
  IndexedSeq(1.0, 2.0)

  import collection._

  SortedSet("hello", "world")
  LinearSeq("a", "b", "c")

  import immutable._

  HashMap("x" -> 24, "y" -> 25, "z" -> 26)

  mutable.Buffer("x", "y", "z")


  List(1, 2, 3) map (_ + 1)

  immutable.SortedSet(1, 2, 3) map (_ + 1)

  //  trait CanBuildFrom[-From, -Elem, +To]
  //
  //  trait GenTraversableLike[+A, +Repr] {
  //    def ++[B >: A, That](that: GenTraversableOnce[B])
  //                        (implicit bf: CanBuildFrom[Repr, B, That]): That
  //  }
  //
  //  trait Traversable[+A] extends GenTraversableLike[A, Traversable[A]]
  //
  //  trait List[+A] {
  //    type Collection[B] = GenTraversableOnce[B]
  //
  //    def ++[B >: A](that: Collection[B]): List[B]
  //  }
  //
  //  trait Combiner[T, Coll] extends mutable.Builder[T, Coll] {
  //    def combine(other: Combiner[T, Coll]): Combiner[T, Coll]
  //  }
  //
  //
  //  sealed abstract class Either[+A, +B] {
  //    def isLeft: Boolean
  //    def isRight: Boolean
  //    def left: A = ???
  //    def right: B = ???
  //  }
  //  final case class Left[+A, +B](a: A) extends Either[A, B] {
  //    def isLeft = true
  //    def isRight = false
  //    override def left = a
  //  }
  //  final case class Right[+A, +B](b: B) extends Either[A, B] {
  //    def isLeft = false
  //    def isRight = true
  //    override def right = b
  //  }


  def alienify(either: Either[Monkey, Alien]): Alien =
    if (either.isLeft) either.left.get
    else either.right.get

}

object TraversableOps extends App {

  // COMBINATIONS AND MAPPING

  val ints = Seq(1, 2) ++ Seq(3, 4) // List(1,2,3,4)

  1 to 4 map {new Ninja(_)} // Vector[Ninja]

  1 to 2 map { i => Seq(i, i * 2)} // Vector(List(1,2), List(2,4))

  1 to 2 map { i => Seq(i, i * 2)} flatten // Vector(1,2,2,4)

  1 to 2 flatMap { i => Seq(i, i * 2)} // Vector(1,2,2,4)

  Map((1, "a"), (2, "b"))
  (1, "a") == 1 -> "a"
  Map(1 -> "a", 2 -> "b").map { e => s"${e._1} -> ${e._2}"}
  // Iterable[String]

  //  trait PartialFunction[-From, +To] extends (From => To) {
  //    def isDefinedAt(test: From): Boolean
  //
  //    def lift = (test: From) =>
  //      if (isDefinedAt(test)) Some(this(test))
  //      else None
  //  }

  val oddNinjas = new PartialFunction[Int, Ninja] {
    def apply(x: Int) = new Ninja(x)

    def isDefinedAt(x: Int) = x > 0 && x % 2 == 1
  }

  (1 to 3) collect oddNinjas
  // Ninja(1), Ninja(3)


  // CONVERSIONS
  Seq(1, 2, 3).toArray.toList.toIterable.toSeq.toIndexedSeq.toStream.toSet

  Map(1 -> "a", 2 -> "b").toList // List[(Int, String)]
  List((1, "a"), (2, "b")).toMap

  // COPYING TO BUFFER (maybe best with Buffer)

  import util.Random.nextInt

  val array = Array.ofDim[Int](1024)
  val buffer = mutable.Buffer[Int]()
  val noise = Stream.continually(nextInt(100))

  noise.copyToArray(array)
  array.copyToBuffer(buffer)

  //noise.copyToBuffer(buffer)

  // SIZES
  Nil.isEmpty // true
  (1 to 100).nonEmpty // true
  (0 until 100).size // 100
  Stream.from(0).hasDefiniteSize

  // false


  // heads and tails
  def sum[T](xs: Traversable[T])
            (implicit n: Numeric[T]) = {
    import n._
    def sum1(xs: Traversable[T], acc: T): T = {
      if (xs.isEmpty) acc
      else sum1(xs.tail, acc + xs.head)
    }
    sum1(xs, zero)
  }


  def leader(ninjas: Traversable[Ninja]) = {
    val master = ninjas.find(_.skill > 100)
    if (master.isDefined) master
    else ninjas.headOption
  }

  val ninjas = Seq(new Ninja(1), new Ninja(99))
  leader(ninjas) // Some(Ninja(1))
  leader(Nil) // None

  ninjas.last // Ninja(99)
  Nil.lastOption
  // None

  val warriors = HashSet("monkeys", "robots", "pirates", "ninjas")
  warriors.head // pirates !!
  warriors.last // ninjas
  warriors.init // pirates, robots, monkeys

  // slicing and dicing

  warriors.slice(1, 3) // robots, monkeys

  (1 to 100) take 10 // 1 to 10
  (1 to 100) drop 10 // 11 to 100
  (1 to 100) takeWhile (_ < 10) // 1 to 9
  (1 to 100) dropWhile (_ < 10) // 10 to 100

  (1 to 100) takeWhile (_ % 7 == 0) // empty
  (1 to 100) filter (_ % 7 == 0) // Vector(7, 14, 21, ... 98)
  (1 to 100) filterNot (_ % 2 == 0) // Vector(1, 3, 5, ... 99)

  (1 to 100) withFilter (_ % 2 == 0) map (_ / 2) // Vector(1, 2, 3 ... 50)

  (1 to 100) splitAt 50 // (1 to 50), (51 to 100)
  (1 to 100) span (_ < 10) // (1 to 9), (10 to 100)

  // grouping

  // groupBy(rule: T => G): Map[G, T]
  (1 to 100) groupBy (_ % 2)
  // Map[Int, IndexedSeq[Int]]
  // Map(1 -> Vector(1,3...), 2 -> Vector(2,4...))

  (1 to 100) partition (_ % 2 == 1)
  // (IndexedSeq[Int], IndexedSeq[Int])
  // (Vector(1,3...), Vector(2,4...))

  // more involved

  // Map[String, Traversable[Int]]
  val recurring = Map(
    "Alexander Rozhenko" -> (4 to 7),
    "Lore" -> Set(1, 4, 6, 7),
    "Thomas Riker" -> Seq(6),
    "K'Ehleyr" -> Seq(2, 4),
    "Alyssa Ogawa" -> (3 to 7),
    "Guinan" -> (2 to 6),
    "Keiko O'Brien" -> (4 to 6),
    "Kurn" -> (3 to 7),
    "Lwaxana Troi" -> ((1 to 5) ++ Seq(7)),
    "Professor Moriarty" -> Seq(2, 6),
    "Q" -> ((1 to 4) ++ (6 to 7)),
    "Reginald Barclay" -> (3 to 7),
    "Ro Laren" -> (5 to 7),
    "Sela" -> (4 to 5),
    "The Traveler" -> Seq(1, 4, 7),
    "Sarek" -> Seq(3, 5),
    "Miles O'Brien" -> ((1 to 5) ++ Seq(7)),
    "Gowron" -> (4 to 6),
    "Robin Lefler" -> Seq(5),
    "Vash" -> (3 to 4),
    "Spock" -> Seq(5)
  )

  // Map[String, Traversable[Int]] => Map[Int, Traversable[String]]
  def reversi[A, B](ns: (A, Traversable[B])): Traversable[(B, A)] = ns._2 map {(_, ns._1)}

  val seasons = recurring. // Map[String, Traversable[Int]]
    toSeq. // Seq[(String, Traversable[Int])]
    flatMap {reversi}. // Seq[(Int, String)]
    groupBy {_._1}. // Map[Int, Seq[(String, Int)]]
    map { e => (e._1, e._2 map {_._2})} // Map[Int, Seq[String]]

  // Map(5 -> Seq(Alexander Rozhenko, Alyssa Ogawa, Guinan ...


  // element tests

  (1 to 100) exists (_ == 101) // false
  (1 to 100) count (_ % 2 == 0) // 50
  (1 to 100) foreach {println}
  // side effect

  // BAD!
  var tmp: List[String] = Nil
  seasons foreach { e =>
    tmp = tmp ++ e._2
  }
  // GOOD
  seasons flatMap {_._2}


  // folds

  // foldLeft (start: E) ((R, E) => R): R
  def sumLeft[T](xs: Traversable[T])(implicit n: Numeric[T]) = {
    import n._
    xs.foldLeft(zero)((result, e) => result + e)
    xs.foldLeft(zero)(_ + _)
    (zero /: xs)(_ + _)
  }

  // foldRight (start: E) ((E, R) => R): R
  def sumRight[T](xs: Traversable[T])(implicit n: Numeric[T]) = {
    import n._
    xs.foldRight(zero)((e, result) => result + e)
    xs.foldRight(zero)(_ + _)
    (xs :\ zero)(_ + _)
  }

  //  (1 to 100).foldLeft("") {(r, e) => s"$r $e"}
  // " 1 2 3 4 ..."

  // reduce ((E, E) => E): E
  (1 to 100) reduceLeft (_ + _)
  (1 to 100) reduceRight (_ + _)

  Seq[Int]() reduceLeft (_ + _)
  // java.lang.UnsupportedOperationException: empty.reduceLeft

  Seq[Int]() reduceLeftOption (_ + _)

  // None


  def reduceLeft[T](xs: Traversable[T])(f: (T, T) => T) =
    xs.tail.foldLeft(xs.head)(f)

  // fold (start: E) ((E, E) => E): E
  def sumParallel[T](xs: Traversable[T])(implicit n: Numeric[T]) = {
    import n._
    xs.par.fold(zero)((result, e) => result + e)
    xs.par.fold(zero)(_ + _)
  }

  (1 to 100).par reduce (_ + _)


  (1 to 100) sum // Range.sum optimised for Ints

  (1 to 10) product

  noise take (100) min

  noise take (100) max


  // String interaction
  (1 to 10) mkString // "12345678910"

  (1 to 3) mkString (",") // "1,2,3"

  (1 to 3) mkString("(", ", ", ")")
  // "(1, 2, 3)"

  val builder = new StringBuilder()
  (1 to 3) addString(builder, ",")
  builder.toString // "1,2,3"

  // views

  (1 to 1000) view(500, 550) foreach println


  // Iterable

  // Iterator
  val random = Stream.continually(nextInt(100)).iterator.buffered
  var break = false
  while (random.hasNext && !break) {
    println(random.next())
    break = true
  }

  // Iterators have most of the Traversable ops
  val numbers = (1 to 100).iterator
  numbers.sum // 5050
  numbers.hasNext // false

  (1 to 100) grouped 10 // iterators for (1 to 10), (11 to 20) ...
  (1 to 100).grouped(10).map {_.sum}.toList
  // List(55, 155, 255, 355, 455, 555, 655, 755, 855, 955)


  (1 to 10).sliding(3).toList
  // (1,2,3), (2,3,4), (3,4,5), ...

  (1 to 10) takeRight 3 // (8,9,10)
  (1 to 10) dropRight 5 // (1,2,3,4,5)

  (1 to 3) zip Seq("a", "b", "c")
  // (1,a), (2,b), (3,c)

  val (characters, characterSeason) = recurring unzip
  // List(Miles O'Brien, Vash, Lore, Sela...
  // List(Vector(1, 2, 3, 4, 5, 7), Range(3, 4...

  def reversiAlt[A, B](ns: (A, Iterable[B])) = ns._2.zip(Stream.continually(ns._1))


  Seq("a", "b", "c") zipAll(1 to 2, "", 1)
  // (a,1), (b,2), (c,1)


  Seq("a", "b", "c").zipWithIndex
  // (a,0), (b,1), (c,2)

  mutable.Buffer("a", "b") sameElements Vector("a", "b")
  // true


  // below traversable all the traits implement PartialFunction
  // Seq allows zero-indexing

  val abc = Seq("a", "b", "c")
  abc(0) // "a"
  abc.isDefinedAt(3) // false

  abc.length // 3
  abc.indices // 0,1,2

  noise.lengthCompare(abc.length) // 1

  abc.indexOf("b") // 1
  Seq("a", "a", "b").lastIndexOf("a") // 1

  abc.indexOfSlice(Seq("b", "c")) // 1
  Seq("a", "b", "a", "b").lastIndexOfSlice(Seq("b", "c")) // 2

  noise indexWhere (_ < 10) // ~10

  (1 to 100) lastIndexWhere (_ % 7 == 0) // 97

  (1 to 100) segmentLength(_ % 5 != 0, 0) // 4
  (1 to 100) prefixLength (_ % 5 != 0) // 4


  (1 to 5) :+ 7
  7 +: (1 to 5)

  (1 to 2) padTo(10, 1) // 1,2,1,1,1,1...

  Seq("a", "b", "b") updated(2, "c") // a,b,c



  Seq("a", "d").patch(1, Seq("b", "c"), 0) // a,b,c,d



  noise.take(5).sorted // 30, 39, 49, 85, 86
  noise.take(5).sorted(Ordering.Int.reverse) // 86, 85, 49, 39, 30

  noise.take(5).sortWith(_ > _) // 86, 85, 49, 39, 30

  (9 to 11).sortBy(_.toString) // 10, 11, 9


  (1 to 5).reverse // 5,4,3,2,1
  (1 to 5).reverseIterator.foreach {println}
  (1 to 3).reverseMap(_.toString) // "3", "2", "1"


  (1 to 10).startsWith(Seq(1)) // true
  (1 to 10).endsWith(Seq(1)) // false
  (1 to 10).contains(5) // true
  (1 to 10).containsSlice(Seq(3, 4))

  // true

  def letter(i: Int): Char = {
    val letters = "abcdefghijklmnopqrstuvwxyz"
    require(i >= 0 && i < 26)
    letters(i)
  }

  val letters = (0 until 26) map letter

  (0 until 26).corresponds(letters)((i, a) => letter(i) == a)


  letters intersect Seq('a', 'b', '?') // a, b

  Seq('a', 'b', '?') diff Seq('a', 'b') // ?

  Seq('a', 'b') union Seq('a', 'c') // a, b, a, c

  Seq('a', 'b', 'b') distinct // a, b


  // Buffers

  mutable.Buffer("a", "b") += "c"
  mutable.Buffer("a", "b") +=("c", "d")
  mutable.Buffer("a", "b") ++= Seq("c", "d")
  mutable.Buffer("a", "b") insert(1, "boo!")
  mutable.Buffer("a", "b") insertAll(1, Seq("foo", "bar"))
  mutable.Buffer("a", "b") -= "a"
  mutable.Buffer("a", "b") remove (1)
  mutable.Buffer("a", "b", "c") remove(0, 2)
  mutable.Buffer("a", "b") trimStart (1)
  mutable.Buffer("a", "b") trimEnd (1)
  mutable.Buffer("a", "b") clear()
  val other = mutable.Buffer("a", "b").clone

  mutable.Buffer("a", "b", "b").update(2, "c")
  mutable.Buffer("a", "b", "b")(2) = "c"


  // Set

  // default apply function is to check an element for existence
  Set('a', 'b')('z') // false

  Set('a', 'b') contains 'z'
  Set('a') subsetOf Set('a', 'b')


  Set(1, 2) + 1 // 1, 2
  Set(1, 2) - 1 // 2
  Set(1, 2) -- Set(1, 2) // empty

  Set('a', 'b') intersect Set('a', 'b', '?') // a, b
  Set('a', 'b') union Set('a', 'c') // a, b, c
  Set('a', 'b', '?') diff Set('a', 'b') // ?

  Set('a', 'b') & Set('a', 'b', '?') // a, b
  Set('a', 'b') | Set('a', 'c') // a, b, c
  Set('a', 'b', '?') &~ Set('a', 'b')
  // ?


  val mutableNames = mutable.Set(recurring.keySet.toSeq: _*)
  mutableNames += "Captain Kirk"
  mutableNames -= "Captain Kirk"
  mutableNames --= Seq("Spock")
  mutableNames("Spock") = true

  mutableNames retain (_ contains "O'Brien")
  mutableNames.clear()

  // Map

  // default operation is to return the value for the key
  recurring("Spock") // Seq(5)

  recurring.get("Spock") // Some(Seq(5))
  recurring.get("V'Ger") // None

  recurring.getOrElse("V'Ger", Nil)
  recurring.withDefaultValue(Nil)("V'Ger")


  recurring.contains("Spock") // true

  recurring + ("V'Ger" -> Seq(-1))
  recurring - "Thomas Riker"

  recurring.keys // Iterable[String]
  recurring.keysIterator // Iterator[String]
  recurring.keySet // Set[String]

  recurring.values // Iterable[Traversable[Int]]
  recurring.valuesIterator // Iterator[Traversable[Int]]
  recurring.values.flatten.toSet // Set[Int]

  recurring filterKeys (_ contains "O'Brien")

  recurring mapValues (_.toList) // Map[String, List[Int]]

  {
    val seasons = recurring. // Map[String, Traversable[Int]]
      toSeq. // Seq[(String, Traversable[Int])]
      flatMap {reversi}. // Seq[(Int, String)]
      groupBy {_._1}. // Map[Int, Seq[(String, Int)]]
      mapValues {_ map {_._2}} // Map[Int, Seq[String]]
  }


  val mutableRecurring = mutable.Map(recurring.toSeq: _*)

  mutableRecurring += ("Captain Kirk" -> Seq(-1))
  mutableRecurring -= "Captain Kirk"
  mutableRecurring --= Seq("Spock")
  mutableRecurring("Spock") = Seq(5)

  mutableRecurring retain ((k, _) => k contains "O'Brien")
  mutableRecurring clear()
}


object Structures extends App {

  //  trait List[+T] {
  //    def head: T = ???
  //    def tail: List[T] = ???
  //    def ::[U >: T](e: U) = new ::(e, this)
  //  }
  //  class ::[+T](override val head: T,
  //               override val tail: List[T]) extends List[T]
  //  object Nil extends List[Nothing]

//  (1 to 100) foreach { i =>
//    val list = (1 to 1000000).toList
//    val watch = Stopwatch.createStarted()
//    list foreach { j =>
//      // do nothing
//    }
//    //    var e = list
//    //    while (e.nonEmpty)
//    //      e = e.tail
//
//    println(watch)
//  }


  val fraggles = Vector("Gobo", "Mokey", "Red", "Wembley", "Boober")

  import Random._
//
//  (1 to 100) foreach { i =>
//    val vec = (0 until 1000000).toVector
//    val shuffled = shuffle(vec)
//    val watch = Stopwatch.createStarted()
//    shuffled foreach vec
//    println(watch) // 60ms
//  }
//
//
//  class Queue[+T](in: List[T], out: List[T]) {
//    def enqueue[E >: T](e: E) = new Queue(e :: in, out)
//
//    def dequeue =
//      if (out.nonEmpty) (out.head, new Queue(in, out.tail))
//      else {
//        val rev = in.reverse
//        (rev.head, new Queue(Nil, rev.tail))
//      }
//
//
//    def head =
//      if (out.nonEmpty) out.head
//      else in.last
//
//    def tail =
//      if (out.nonEmpty) new Queue(in, out.tail)
//      else new Queue(Nil, in.reverse.tail)
//  }

//  val vec = (1 to 1000000).map{i => UUID.randomUUID()}.toVector
//
//  (1 to 10) foreach { i =>
//    val watch = Stopwatch.createStarted()
//    (Set[UUID]() /: vec)(_ + _)
//    println(watch)
//  }

  def isPrime(i : Int) = {
    if (i <= 1) false
    else if (i == 2) true
    else !(2 until i).exists(x => (i % x) == 0)
  }

  val primes = Stream.from(0).filter{isPrime}

  println((primes take 20).toList)

  val stream = 1 #:: primes

//  class Cons[+A](hd: A, tl: => Stream[A]) {
//    def isEmpty = false
//    def head = hd
//    lazy val tail = tl
//  }
//
//  implicit class ConsWrapper[A](tl: => Stream[A]) {
//    def #::(hd: A) = new Cons(hd, tl)
//  }

  import scala.math.BigInt
  val fibs: Stream[BigInt] =
    BigInt(0) #:: BigInt(1) #::
    fibs.zip(fibs.tail).map {n => n._1 + n._2}
  println(fibs take 20 toList)

  var i = 0
  Stream.fill(10){i+=1 ; i}.toList

  val finite = Stream.fill(100){nextGaussian()}
  val infinite = Stream.continually{nextGaussian()}
}