package com.github.fommil.scala.implicits

import spray.json._

import com.github.fommil.scala.json._

object SamuraiMarshallingLong extends App with DefaultJsonProtocol {

  class SamuraiStats(val name: String,
                     val robot: Boolean,
                     val appears: Int,
                     val powers: List[Boolean]) {
    override def toString = s"$name $robot $appears $powers"
  }

    implicit object SamuraiFormat extends JsonFormat[SamuraiStats] {
      def write(o: SamuraiStats): JsValue = j(
        "name" :: o.name.toJson,
        "robot" :: o.robot.toJson,
        "appears" :: o.appears.toJson,
        "powers" :: j(o.powers.map { _.toJson }: _*)
      )

      def read(json: JsValue): SamuraiStats = {
        val fields = json.asJsObject().fields
        new SamuraiStats(
          fields("name").convertTo[String],
          fields("robot").convertTo[Boolean],
          fields("appears").convertTo[Int],
          fields("powers").convertTo[List[Boolean]]
        )
      }
    }


  val stats = new SamuraiStats(
    "Samurai Jack",
    false,
    48,
    true :: false :: Nil
  )
  val json = stats.toJson
  println(json.prettyPrint)

  val recovered = json.convertTo[SamuraiStats]
  println(recovered)

}

object SamuraiMarshallingProduct extends App with DefaultJsonProtocol {

  class SamuraiStats(val name: String,
                     val robot: Boolean,
                     val appears: Int,
                     val powers: List[Boolean])
    extends Product4[String, Boolean, Int, List[Boolean]] {
    def _1 = name

    def _2 = robot

    def _3 = appears

    def _4 = powers

    def canEqual(that: Any) = that.isInstanceOf[SamuraiStats]
  }

  object SamuraiStats extends ((String, Boolean, Int, List[Boolean]) => SamuraiStats) {
    def apply(name: String,
              robot: Boolean,
              appears: Int,
              powers: List[Boolean]) = new SamuraiStats(name, robot, appears, powers)
  }

  implicit val SamuraiStatsFormat = jsonFormat(SamuraiStats,
    "name", "robot", "appears", "powers")

  val stats = SamuraiStats(
    "Samurai Jack",
    false,
    48,
    true :: false :: Nil
  )

  val json = stats.toJson
  println(json.prettyPrint)
  println(json.convertTo[SamuraiStats])

}

object SamuraiMarshallingCase extends App with DefaultJsonProtocol {

  case class SamuraiStats(name: String,
                          robot: Boolean,
                          appears: Int,
                          powers: List[Boolean])

  implicit val SamuraiStatsFormat = jsonFormat4(SamuraiStats)

  val stats = SamuraiStats(
    "Samurai Jack",
    false,
    48,
    true :: false :: Nil
  )

  val json = stats.toJson
  println(json.prettyPrint)
  println(json.convertTo[SamuraiStats] == stats) // true
}

object AltMarshalling extends App with DefaultJsonProtocol with NullOptions {

  case class SamuraiStats(name: String,
                          robot: Boolean,
                          appears: Int,
                          powers: List[Option[Boolean]])

  implicit val SamuraiStatsFormat = jsonFormat4(SamuraiStats)

  val stats = SamuraiStats(
    "Samurai Jack",
    false,
    48,
    Some(true) :: Some(false) :: None :: Nil
  )

  val json = stats.toJson
  println(json.prettyPrint)
  println(json.convertTo[SamuraiStats] == stats) // true
}

object ImprovedDsl extends App with DefaultJsonProtocol with NullOptions {

  import spray.json.pimpAny

  implicit class RichString(self: String) {
    def :>[T: JsonWriter](that: T): JsField = (self, that.toJson)
  }

  def j(fields: JsField*) = JsObject(fields: _*)

  def j[T: JsonWriter](elements: T*) = JsArray(elements.map { _.toJson }: _*)

  val tree = j(
    "name" :> "Samurai Jack",
    "robot" :> false,
    "appears" :> 48,
    "powers" :> j(Some(true), Some(false), None)
  )
  println(tree.prettyPrint)
}

