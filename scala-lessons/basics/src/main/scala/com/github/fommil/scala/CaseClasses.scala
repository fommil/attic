package com.github.fommil.scala

import java.util.Date

case class Address(street: String,
                   town: String,
                   zip: String,
                   country: String)

case class Person(name: String,
                  dob: Date,
                  address: Address)

object PuppeteerApp extends App {

  val fraggle = Fraggle("Gobo")

  fraggle muppeteer match {
    case Person("Jerry Nelson", _, _) => true
    case Person(_, _, Address(_, _, _, "England")) => true
    case _ => false
  }

}