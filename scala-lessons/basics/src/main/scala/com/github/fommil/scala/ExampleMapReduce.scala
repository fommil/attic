package com.github.fommil.scala

case class Fraggle(name: String) {
  def favouriteFood(): String = ???
  def muppeteer(): Person = ???
}


class ExampleMapReduce {

  def radishes(fraggles: List[Fraggle]) =
    fraggles.par.
    map(_.favouriteFood).
    count(_ == "Radish")

}
