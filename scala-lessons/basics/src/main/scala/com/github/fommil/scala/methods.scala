package com.github.fommil.scala


class Ninja(val skill: Int) extends Ordered[Ninja] {

  def compare(that: Ninja) = skill compare that.skill

  def fight(that: Ninja) = if (this > that) this else that

  def train(times: Int = 1) = new Ninja(skill + times)

  def sneak() {println("...")}

  def ^^:(that: Ninja) = that

}

// used in implicits chapter
object Ninja {
  implicit def fromInt(i: Int): Ninja = new Ninja(i)
}

//  def teamUp(those: List[Ninja]) = ???
//
//  def teamUp(those: List[Pirate]) = ???


class SpecialNinja(skill: Int) extends Ninja(skill) {
  def ⚔(that: Ninja) = fight(that)

  def +=(times: Int) = train(times)

  // ! ~ + -
  def unary_~ = sneak()
}


class Weapon(val bonus: Int)

object Sword extends Weapon(10)
object Nunchucks extends Weapon(4)
object Sai extends Weapon(3)
object Bō extends Weapon(5)

class WeaponizedNinja(skill: Int) extends Ninja(skill) {
  def apply(weapon: Weapon) = new WeaponizedNinja(skill + weapon.bonus)
}

object WeaponizedNinja {
  def apply(base: Int, weapons: Weapon*) =
    weapons.foldLeft(new WeaponizedNinja(base)){(n, w) => n(w)}
}




object NinjaApp extends App {

  val grandMaster = new SpecialNinja(100)
  val master = new SpecialNinja(10)
  var student = new SpecialNinja(1)
  val weakling = new Human {}

  // infix
  student.train(1) == (student train 1)

  // postfix
  student.sneak() == (student sneak()) == (student sneak)

  // right associative
  (student ^^: master ^^: grandMaster) == (student ^^: (master ^^: grandMaster)) == student


  // extensions

  val winner = master ⚔ student

  student += 1

  ~student

  val ninja = new WeaponizedNinja(10)

  {
  val leonardo = ninja(Sword)(Sword) // ninja.apply(Sword).apply(Sword)
  val donatello = ninja(Bō)
  val raphael = ninja(Sai)(Sai)
  val michaelanglo = ninja(Nunchucks)(Nunchucks)
  }
  {
    val leonardo = WeaponizedNinja(10, Sword, Sword)
    val donatello = WeaponizedNinja(5, Bō)
    val raphael = WeaponizedNinja(8, Sai, Sai)
    val michaelanglo = WeaponizedNinja(1, Nunchucks, Nunchucks)
  }

  val base = 10
  val weapons = Sword :: Nil

  val factory = weapons.foldLeft(new WeaponizedNinja(base))_
  factory((n: WeaponizedNinja, w: Weapon) => n(w))

}

trait NinjaUtils {
  this: Ninja =>

  protected def watch() = ???
  protected def run() = ???
  protected def jump() = ???
  protected def punch() = ???
}

class NinjaChecker {
  def check(knowThyEnemy: Boolean = false,
            locationOfEnemy: Boolean = false,
            rations: Boolean = false,
            cleanUnderwear: Boolean = false) {  }
}


object NinjaCheckerApp extends App {
  val checker = new NinjaChecker
  checker.check(false, false, true, false)

  checker.check(rations = true)

}

//class Fixer {
//
//  def repair[FriendlyRobot](rs: List[FriendlyRobot]) { }
//
//  def repair[SinisterRobot](rs: List[SinisterRobot]) {
//    println("Muahahaha!")
//  }
//}
//
//object FixerApp extends App {
//  val fixer = new Fixer
//
//  fixer.repair(List(new FriendlyRobot("Marvin")))
//  // nothing
//
//  fixer.repair(List(new SinisterRobot("Megatron")))
//  // evil laughter
//}
