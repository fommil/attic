package com.github.fommil.scala

package attic {

trait MyTrait {}

abstract class MyAbstract {}

}

import attic.{MyTrait => Awesome}
import scala.util._

class MyClass(myParam: Int) extends attic.MyAbstract with Awesome {
  {
    // single line comment
    require(myParam.>(0));
  }

  def myDef[MyType <: Any](in: MyType) =
  /*
   multi-line
   comment
   */
    () => math.sqrt(myParam);

}

object MyObject {

  var myVar = "a string"
  val `val` = "a string called:\tval"
  val myChar = 'c'
  val myBoolean = true
  val myInt = 1
  val myLong = 10L
  val myFloat = 10.0f
  val myDouble = 10.0e+100
  val a, b, c = new Object
  assert(a != b && b != c)

  val multi = """this is a
                |multi-line
                |string""".stripMargin

  val interp = s"$myInt = ${myLong - 9}"

}


package starfleet {
class Captain {
  protected val log = "Stardate 46944.2"

  // private == protected[Captain]
  private val classified = "en route to the Enterprise"
  private[this] val diary = "I ❤ Dr. Crusher"

  // can't see other.diary
  protected[starfleet]
  def look(other: Captain) = other.classified
}

  class Ensign {
    new Captain().look(new Captain)
  }
}

class LocutusOfBorg extends starfleet.Captain {
  def expose = log

  look(new starfleet.Captain)
}



trait Sensor

trait OuterSpace {
  val a = "Jack"
  val b = "Lydia"

  class InnerSpace {
    val a = "Tuck"

    def listen(sensor: Sensor) = (setting: Int) =>
      s"listening to ${OuterSpace.this.a}" +
      s" with $sensor on setting $setting"

  }
}


object OuterApp extends App {
  val boolean = true
  val expression = true

  val pirates = List[Pirate]()

  for {
    pirate <- pirates
    if pirate.lastName == "Sparrow"
  } yield pirate.firstName


  def isDrunk(pirate: Pirate) = true

  pirates match {
    case Nil => "thar be nobody here"
    case head :: tail if isDrunk(head) => "arr!"
    case sober => "t' boss be sober"
  }


  try {
    expression
  } catch {
    case a: IllegalArgumentException =>
      expression
    case u: UnsupportedOperationException =>
      expression
  } finally {
    expression
  }

  Try(expression) match {
    case Success(result) => expression
    case Failure(exception) => expression
  }

  throw new IllegalStateException("bad things")


  val evil = true

  val robot =
  if (evil)
    new SinisterRobot("Bender")
  else
    new FriendlyRobot("Bender")


  val robots = robot :: Nil

  robots map {robot => ???}

  robots foreach {robot => ???}

  //  if (boolean) {
//    expression
//  } else if (boolean) {
//    expression
//  } else {
//    expression
//  }
//
//  while (boolean) {
//    expression
//  }
//
//  do {
//    expression
//  } while (boolean)

}