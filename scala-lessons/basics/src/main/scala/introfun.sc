import java.util.Date


def lower(a: String): String = a.toLowerCase()

def runner(s: String, f: (String) => String): String = f(s)

runner("HELLO", lower)

runner("HELLO", s => s + " Scala Rocks")



val a, b, c = new Object

{
  assert(a != b && b != c)
}



def isRadish(food: String): Boolean = food == "Radish"

val menu = List("Bonkleberries", "Radish", "Doozer Sticks")

menu.count(isRadish) // => List(Radish)

val rad = (food: String) => food == "Radish"
menu.count(rad)

val notRad = (food: String) => food != "Radish"
menu.count(notRad)


menu.count(food => food == "Radish")

menu.count(food => food != "Radish")

menu.count(_ == "Radish")

menu.count(_ != "Radish")


