// mkdir -p src/{main,test}/{java,scala}/com/github/fommil/scala

organization := "com.github.fommil"

name := "scala"

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.3"

resolvers += "spray" at "http://repo.spray.io/"

libraryDependencies ++= Seq(
  "com.github.fommil"      %  "java-logging" % "1.1",
  "com.google.guava"       %  "guava"        % "15.0",
  "com.typesafe.akka"      %% "akka-contrib" % "2.2.1" intransitive(),
  "com.typesafe.akka"      %% "akka-actor"   % "2.2.1",
  "io.spray"               %% "spray-json"   % "1.2.5",
  "org.specs2"             %% "specs2"       % "2.2.3" % "test"
)
