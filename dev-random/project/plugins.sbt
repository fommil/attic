scalacOptions ++= Seq("-unchecked", "-deprecation")
ivyLoggingLevel := UpdateLogging.Quiet

addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.1.0-M4")

addSbtPlugin("com.fommil"   % "sbt-sensible" % "2.4.5")
addSbtPlugin("com.geirsson" % "sbt-scalafmt" % "1.6.0-RC3")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.7")
