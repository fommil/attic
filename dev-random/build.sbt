inThisBuild(
  Seq(
    startYear := Some(2018),
    scalaVersion := "2.12.6",
    sonatypeGithost := (Gitlab, "fommil", "dev-random"),
    sonatypeDevelopers := List("Sam Halliday"),
    licenses := Seq(GPL3),
    scalafmtConfig := Some(file("project/scalafmt.conf"))
  )
)

addCommandAlias("fmt", "all scalafmtSbt scalafmt test:scalafmt")

val derivingVersion = "1.0.0-RC1"
libraryDependencies ++= Seq(
  "org.apache.commons"         % "commons-math3"              % "3.6.1",
  "io.estatico"                %% "newtype"                   % "0.4.2",
  "org.scalaz"                 %% "scalaz-ioeffect"           % "2.10.1",
  "com.fommil"                 %% "deriving-macro"            % derivingVersion % "provided",
  "com.fommil"                 %% "scalaz-deriving"           % derivingVersion,
  "org.scalatest"              %% "scalatest"                 % "3.0.5" % "test",
  "com.github.alexarchambault" %% "scalacheck-shapeless_1.13" % "1.1.8" % "test"
)

scalacOptions ++= Seq(
  "-language:_",
  "-unchecked",
  "-explaintypes",
  "-Ywarn-value-discard",
  "-Ywarn-numeric-widen",
  "-Ypartial-unification",
  "-Xlog-free-terms",
  "-Xlog-free-types",
  "-Xlog-reflective-calls",
  "-Yrangepos",
  "-Ywarn-unused:explicits,patvars,imports,privates,locals,implicits",
  "-opt:l:method,inline",
  "-opt-inline-from:scalaz.**",
  "-opt-inline-from:fommil.**"
)

addCompilerPlugin("com.fommil"     %% "deriving-plugin"    % derivingVersion)
addCompilerPlugin("org.spire-math" %% "kind-projector"     % "0.9.7")
addCompilerPlugin("com.olegpy"     %% "better-monadic-for" % "0.2.4")
addCompilerPlugin(
  ("org.scalamacros" % "paradise" % "2.1.1").cross(CrossVersion.full)
)

scalacOptions in (Compile, console) -= "-Yno-imports"
scalacOptions in (Compile, console) -= "-Yno-predef"
initialCommands in (Compile, console) := Seq(
  "scalaz._, Scalaz._"
).mkString("import ", ",", "")

test in assembly := {}
mainClass in assembly := Some("fommil.URandom")
assemblyJarName in assembly := "fommil.jar"
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "semanticdb.semanticidx") => MergeStrategy.discard
  case PathList("META-INF", "semanticdb", _*)         => MergeStrategy.discard
  case other                                          => MergeStrategy.defaultMergeStrategy(other)
}
