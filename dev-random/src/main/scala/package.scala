// Copyright: 2018 - 2018 Sam Halliday
// License: http://www.gnu.org/licenses/gpl-3.0.en.html

package object fommil {
  // DSL to match the spec...
  implicit final class MyIntSyntax(private val self: Int) extends AnyVal {
    // bitwise addition ⊕ (exclusive OR)
    @inline def ⊕(that: Int): Int = self ^ that

    // 32-bit addition mod 2^32 ⊞
    @inline def ⊞(that: Int): Int = self + that

    // constant-distance rotation operations (<<<)
    @inline def <<<(that: Int): Int = (self << that) | (self >>> -that)

    // become the high bits of a long when combined with that
    @inline def combine(that: Int): Long =
      (self.toLong << 32) | that.toLong & 0XFFFFFFFFL
  }

  implicit final class MyLongSyntax(private val self: Long) extends AnyVal {
    @inline def split: (Int, Int) = ((self >> 32).toInt, self.toInt)
  }

}
