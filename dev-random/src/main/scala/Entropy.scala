// Copyright: 2018 - 2018 Sam Halliday
// License: http://www.gnu.org/licenses/gpl-3.0.en.html

package fommil

import java.io._
import java.util.concurrent.atomic.AtomicLong
import scalaz._, ioeffect._

/** Algebra providing True Random Numbers */
trait Entropy[F[_]] {
  def nextInt: F[Int]
}

object Entropy {
  final class FileEntropy private[fommil] (name: String) extends Entropy[Task] {
    private val pos: AtomicLong = new AtomicLong
    def nextInt: Task[Int] = Task {
      val i  = pos.getAndAdd(4)
      val in = new java.io.RandomAccessFile(name, "r")
      try {
        in.seek(i)
        in.readInt()
      } finally in.close()
    }
  }

  final class DeviceEntropy(name: String) extends Entropy[Task] {
    def nextInt: Task[Int] = Task {
      val in = new DataInputStream(new FileInputStream(name))
      try in.readInt()
      finally in.close()
    }
  }

  val `/dev/random`: Entropy[Task]   = new DeviceEntropy("/dev/random")
  val neug: Entropy[Task]            = new DeviceEntropy("/dev/ttyACM0")
  val fake_neug: Task[Entropy[Task]] = Task(new FileEntropy("neug.dat"))

}
