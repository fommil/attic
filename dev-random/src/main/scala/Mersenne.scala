// Copyright: 2018 - 2018 Sam Halliday
// License: http://www.gnu.org/licenses/gpl-3.0.en.html

import org.apache.commons.math3.random.MersenneTwister

object Mersenne {
  def main(args: Array[String]): Unit = {
    // chosen by fair dice roll.
    // guaranteed to be random.
    // https://www.xkcd.com/221/
    val seed: Long = -1117237950L

    val twister = new MersenneTwister(seed)
    val out     = new java.io.FileOutputStream("mersenne.dat")

    var i   = 0
    val max = 1024L * 1024L * 128L
    while (i < max) {
      val int = twister.nextInt
      out.write(int)

      i += 1
    }

  }
}
