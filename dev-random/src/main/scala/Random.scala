// Copyright: 2018 - 2018 Sam Halliday
// License: http://www.gnu.org/licenses/gpl-3.0.en.html

object Random {
  def main(args: Array[String]): Unit = {
    // chosen by fair dice roll.
    // guaranteed to be random.
    // https://www.xkcd.com/221/
    val seed: Long = -1117237950L

    val twister = new java.util.Random(seed)
    val out     = new java.io.FileOutputStream("java.dat")

    var i   = 0
    val max = 1024L * 1024L * 128L
    while (i < max) {
      val int = twister.nextInt
      out.write(int)

      i += 1
    }

  }
}
