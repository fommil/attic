// Copyright: 2018 - 2018 Sam Halliday
// License: http://www.gnu.org/licenses/gpl-3.0.en.html

package fommil

// format: off
import scalaz._, Scalaz._
import io.estatico.newtype.macros.newtype

// Cons Cons Cons  Cons
// Key  Key  Key   Key
// Key  Key  Key   Key
// Pos  Pos  Nonce Nonce
@deriving(Equal, Show, Semigroup)
final case class ChaCha20 private (
   _0: Int,  _1: Int,  _2: Int,  _3: Int,
   _4: Int,  _5: Int,  _6: Int,  _7: Int,
   _8: Int,  _9: Int, _10: Int, _11: Int,
  _12: Int, _13: Int, _14: Int, _15: Int
) {
  def pos: Long = _12.combine(_13)
  def nonce: Long = _14.combine(_15)
}

object ChaCha20 {
  def apply(
    key1: Int, key2: Int, key3: Int, key4: Int,
    key5: Int, key6: Int, key7: Int, key8: Int,
    pos1: Int, pos2: Int, nonce1: Int, nonce2: Int
  ): ChaCha20 = ChaCha20(
    // "expa", "nd 3", "2-by", and "te k"
    0X61707865,  0X3320646e,  0X79622d32,  0X6b206574,
    key1, key2, key3, key4,
    key5, key6, key7, key8,
    pos1, pos2, nonce1, nonce2
  )

  def apply(
    key1: Int, key2: Int, key3: Int, key4: Int,
    key5: Int, key6: Int, key7: Int, key8: Int
  ): ChaCha20 = ChaCha20(
    key1, key2, key3, key4,
    key5, key6, key7, key8,
       0,    0,    0,    0
  )

  @newtype
  @deriving(Equal, Show)
  final case class Block(val value: ChaCha20) {
    def values: IList[Int] = {
      val v = value // because scala
      import v._
      _0 :: _1 :: _2 :: _3 :: _4 :: _5 :: _6 :: _7 ::
      _8 :: _9 :: _10 :: _11 :: _12 :: _13 :: _14 :: _15 :: IList.empty
    }
  }

  def step(in: ChaCha20): (ChaCha20, ChaCha20.Block) =
    (inc(in), ChaCha20.round(in))

  private[fommil] def inc(in: ChaCha20): ChaCha20 = {
    val counter = (in.pos + 1)
    val (a, b) = counter.split
    if (counter == 0) {
      // The output will repeat when the counter resets. It is standard to use
      // the 64 bits of nonce as the high bits in a 128 bit counter, to extend
      // the length of the stream to 2^128 bits. However, we could sacrifice
      // stream length for entropy by injecting, say, 32 bits of entropy at
      // every rollover.
      //
      // When the nonce rolls over, the output stream repeats. We could return a
      // failure when that happens, but supporting streams longer than 2^128
      // feels excessive for this short exercise.
      val (c, d) = (in.nonce + 1).split
      in.copy(_12 = a, _13 = b, _14 = c, _15 = d)
    }
    else
      in.copy(_12 = a, _13 = b)
  }

  // 10 iterations of the double round
  private[fommil] def round(in: ChaCha20): Block =
    Block((0 |-> 9).foldLeft(in)((acc, _) => even(odd(acc))) |+| in)

  private[fommil] def qr(_1: Int, _2: Int, _3: Int, _4: Int): (Int, Int, Int, Int) = {
    var a = _1
    var b = _2
    var c = _3
    var d = _4

    a ⊞= b; d ⊕= a; d <<<= 16;
    c ⊞= d; b ⊕= c; b <<<= 12;
    a ⊞= b; d ⊕= a; d <<<= 8;
    c ⊞= d; b ⊕= c; b <<<= 7;

    (a, b, c, d)
  }

  // QR(0, 4, 8, 12)   // 1st column
  // QR(1, 5, 9, 13)   // 2nd column
  // QR(2, 6, 10, 14)  // 3rd column
  // QR(3, 7, 11, 15)  // 4th column
  private def odd(in: ChaCha20): ChaCha20 = {
    // I don't like the index repetition here...
    val (_0, _4, _8, _12) = qr(in._0, in._4, in._8, in._12)
    val (_1, _5, _9, _13) = qr(in._1, in._5, in._9, in._13)
    val (_2, _6, _10, _14) = qr(in._2, in._6, in._10, in._14)
    val (_3, _7, _11, _15) = qr(in._3, in._7, in._11, in._15)
    ChaCha20(
      _0, _1, _2, _3,
      _4, _5, _6, _7,
      _8, _9, _10, _11,
      _12, _13, _14, _15
    )
  }

  // QR(0, 5, 10, 15)  // diagonal 1 (main diagonal)
  // QR(1, 6, 11, 12)  // diagonal 2
  // QR(2, 7, 8, 13)   // diagonal 3
  // QR(3, 4, 9, 14)   // diagonal 4
  private def even(in: ChaCha20): ChaCha20 = {
    // I don't like the index repetition here...
    val (_0, _5, _10, _15) = qr(in._0, in._5, in._10, in._15)
    val (_1, _6, _11, _12) = qr(in._1, in._6, in._11, in._12)
    val (_2, _7, _8, _13) = qr(in._2, in._7, in._8, in._13)
    val (_3, _4, _9, _14) = qr(in._3, in._4, in._9, in._14)
    ChaCha20(
      _0, _1, _2, _3,
      _4, _5, _6, _7,
      _8, _9, _10, _11,
      _12, _13, _14, _15
    )
  }

}
