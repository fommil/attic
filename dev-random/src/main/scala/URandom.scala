// Copyright: 2018 - 2018 Sam Halliday
// License: http://www.gnu.org/licenses/gpl-3.0.en.html

package fommil

import scalaz._, Scalaz._, ioeffect._

object URandom extends SafeApp {

  def run(args: List[String]): IO[Void, ExitStatus] = {
    for {
      entropy <- Entropy.fake_neug
      //entropy <- Task.now(Entropy.neug) // needs superuser access...
      initial <- create(entropy)
      _       <- loop.run(initial)
    } yield ()
  }.attempt[Void].map {
    case \/-(_) => ExitStatus.ExitNow(0)
    case -\/(e) =>
      e.printStackTrace()
      ExitStatus.ExitNow(1)
  }

  // scalaz really needs an `iterateM(8).sequence`...
  def create(E: Entropy[Task]): Task[ChaCha20] = {
    val I = E.nextInt
    (I |@| I |@| I |@| I |@| I |@| I |@| I |@| I)(ChaCha20.apply)
  }

  // I don't like using StateT here because we are discarding the value... it
  // would be better if there was a way to loop on a kleisli arrow forever, but
  // scalaz offers no such API...
  def loop: StateT[Task, ChaCha20, Unit] =
    BindRec[StateT[Task, ChaCha20, ?]].forever(step)

  def step: StateT[Task, ChaCha20, Unit] =
    for {
      cc              <- StateT.get[Task, ChaCha20]
      (update, block) = ChaCha20.step(cc)
      _               <- StateT.put[Task, ChaCha20](update)
      _               <- StateT.liftM(print(block))
    } yield ()

  def print(block: ChaCha20.Block): Task[Unit] = Task {
    val out = new java.io.DataOutputStream(System.out)
    block.values.toList.foreach(i => out.writeInt(i))
  }

}
