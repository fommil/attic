// Copyright: 2018 - 2018 Sam Halliday
// License: http://www.gnu.org/licenses/gpl-3.0.en.html

package fommil

import org.scalatest._
import org.scalatest.Matchers._
import org.scalatest.prop.GeneratorDrivenPropertyChecks

import scalaz._, Scalaz._

class ChaCha20Test
    extends FlatSpec
    with NonImplicitAssertions
    with GeneratorDrivenPropertyChecks {

  "ChaCha20" should "reproduce the A.1 test vector" in {
    // https://www.rfc-editor.org/rfc/rfc8439.txt

    val initial = ChaCha20(
      // format: off
      0, 0, 0, 0,
      0, 0, 0, 0,
      0, 0, 0, 0
      // format: on
    )

    val expected = ChaCha20.Block(
      ChaCha20(
        // format: off
        0Xade0b876,  0X903df1a0,  0Xe56a5d40,  0X28bd8653,
        0Xb819d2bd,  0X1aed8da0,  0Xccef36a8,  0Xc70d778b,
        0X7c5941da,  0X8d485751,  0X3fe02477,  0X374ad8b8,
        0Xf4b8436a,  0X1ca11815,  0X69b687c3,  0X8665eeb2
        // format: on
      )
    )

    ChaCha20.round(initial).assert_===(expected)
  }

  it should "reproduce the 2.3.2 test vector" ignore {
    // I don't trust this vector. The data doesn't match the text. e.g.
    //
    // Nonce = (00:00:00:09:00:00:00:4a:00:00:00:00)
    // Block Count = 1
    //
    // but the nonce and pos look mixed up in the initial state.

    val initial = ChaCha20(
      // format: off
      0X03020100,  0X07060504,  0X0b0a0908,  0X0f0e0d0c,
      0X13121110,  0X17161514,  0X1b1a1918,  0X1f1e1d1c,
      0X00000001,  0X09000000,  0X4a000000,  0X00000000
      // format: on
    )

    val expected = ChaCha20.Block(
      ChaCha20(
        // format: off
        0X837778ab,  0Xe238d763,  0Xa67ae21e,  0X5950bb2f,
        0Xc4f2d0c7,  0Xfc62bb2f,  0X8fa018fc,  0X3f5ec7b7,
        0X335271c2,  0Xf29489f3,  0Xeabda8fc,  0X82e46ebd,
        0Xd19c12b4,  0Xb04e16de,  0X9e83d0cb,  0X4e3c50a2
        // format: on
      )
    )

    ChaCha20.round(initial).assert_===(expected)
  }

  it should "produce an expected output state" in {
    import org.scalacheck.ScalacheckShapeless._
    forAll { (t: ChaCha20) =>
      ChaCha20.inc(t).pos should (be(0).or(be(t.pos + 1)))
    }
  }

  it should "increment the nonce on counter overflow" in {
    val in = ChaCha20(
      // format: off
      1,  2,  3,  4,
      5,  6,  7,  8,
      -1,  -1, 0, 0
      // format: on
    )
    val out = ChaCha20(
      // format: off
      1, 2, 3, 4,
      5, 6, 7, 8,
      0, 0, 0, 1
      // format: on
    )
    ChaCha20.inc(in).assert_===(out)
  }

  it should "pass the quarter round test vector 2.1.1" in {
    val (a, b, c, d) =
      ChaCha20.qr(0x11111111, 0x01020304, 0x9b8d6f43, 0x01234567)

    a.shouldBe(0xea2a92f4)
    b.shouldBe(0xcb1cf8ce)
    c.shouldBe(0x4581472e)
    d.shouldBe(0x5881c4bb)
  }
}
