> Build a clone of `$ cat /dev/random` in the scripting language of your choice,
> generating your own entropy.

This seemingly simple task is the entrance of a very deep rabbit hole that can
lead to an existential crisis.

The expectation of the typical developer is that `/dev/random` is a readonly,
infinite, stream of bits that are 0 or 1 with equal probability, each bit
independent of the others. Each bit should be produced within a fast constant
time.

However, such a data source is an impossible goal. In the following, we discuss
the design decisions of the Linux kernel, explaining why almost every
expectation must be relaxed to be able to do anything at all. We then take even
weaker design decisions to be able to deliver something within a short period of
time for the purpose of this exercise.

## Verification

Assuming that we can produce a stream of independent bits, how can we verify
that every bit is independent? This is an unsolved problem and NIST can only
recommend a set of statistical measures [1], updated every few years, probably
under the guidance of the NSA, to measure the quality of a sample of random
data.

These measures ensure that the data isn't a simple repeating pattern such as
010101 at various hierarchies of scale (for example 1024 or 64k blocks), or
biased towards either value. But they are just heuristics and will fail to
detect deterministic correlations where bits may be related to previous values
by means of an underlying equation, or indeed to reconstruct previous values
from current ones.

Because there is no better alternative, we will resign ourselves to use the
ent [2] and dieharder [3] software which includes all the NIST recommendations,
and more. The pedantic authors of dieharder are quick to point out that the
quality of a random number generator must be continually assessed, as it could
at any moment start producing predictable numbers.

- [1] https://csrc.nist.gov/projects/random-bit-generation
- [2] http://www.fourmilab.ch/random/
- [3] http://webhome.phy.duke.edu/~rgb/General/dieharder.php

## True Randomness (TRNG)

It is impossible to generate random numbers in a closed system, such as a
function, since they would be entirely determined by the input parameters. For
true randomness, we must turn to nature.

In the United Kingdom, we have a government run national lottery, conducted
twice a week drawing 6 different numbers from 1 to 59. We can look at each game
as the generation of 78 bits. The yearly revenue of the lottery is GBP 5,500
million, with 4% going to operating costs. For 104 games a year, we can argue
that each bit costs GBP 30k, primarily due to the overhead of public scrutiny.

Other sources of truly random numbers exist that can be digitised, for example
nuclear decay and thermal noise in a resistor. However, no known system is
flawless. The most common problems being in the analogue to digital signal
conversion (which can introduce correlations / biases) or a degradation of
quality over time. The pedantic testers are correct, we really do have to
continuously measure the quality of a random number generator, even nature!

The typical desktop computer does not have access to any of these sources, so
the Linux kernel makes do with the only sources of noise it has available on
startup: the hardware clock and, recursively, a random number saved during the
previous shutdown. These numbers are both highly predictable and finite.

I am fortunate enough to have a NeuG USB True Random Number Generator (TRNG),
based on the quantization error of analogue to digital signal conversion. I can
access a stream of random numbers (as superuser) at 82 kB/s

    ~ sudo dd bs=1024 if=/dev/ttyACM0 iflag=fullblock count=1024 of=neug.dat
    1048576 bytes (1.0 MB, 1.0 MiB) copied, 12.7832 s, 82.0 kB/s

which is 24 times slower than my /dev/random

    ~ dd bs=1024 if=/dev/random iflag=fullblock count=1024 of=random.dat
    1048576 bytes (1.0 MB, 1.0 MiB) copied, 0.519614 s, 2.0 MB/s

To put this into context: both are significantly slower than the typical GB/s
tranfer speeds of an AWS ethernet connection, putting random number generation
at the bottom of the class when it comes to I/O performance.

Why does `/dev/random` not simply redirect to the NeuG? Firstly, end user
performance, secondly power usage (my NeuG gets physically hotter after
generating this much data) and third because we don't trust the numbers not to
degrade.

The dieharder NIST tests require approximately 80MB of data, providing the
following assessments:

    ~ dieharder -g 201 -d 101 -f /dev/ttyACM0
        test_name   |ntup| tsamples |psamples|  p-value |Assessment
            sts_runs|   2|    100000|     100|0.53592397|  PASSED


    ~ dieharder -g 201 -d 101 -f /dev/random
        test_name   |ntup| tsamples |psamples|  p-value |Assessment
            sts_runs|   2|    100000|     100|0.39212804|  PASSED

At least by NIST's recommended measures, both pass the test (note that an
isolated p-value is meaningless).

In the case of the Linux kernel, `/dev/random` is in fact writable and superuser
applications in userland can feed the entropy store. Indeed, a daemon process
feeds the NeuG's numbers into `/dev/random` as a source of entropy at a much
slower rate than users may be reading from `/dev/random`, using a feedback loop
where the kernel can request more entropy when it is running low.

## Pseudorandom and Quasirandom

The Linux kernel does not provide true random numbers in `/dev/random`. It
instead produces deterministic numbers based on a source of entropy that is
populated by startup clocks, previously stored numbers, user keystrokes, voltage
measurements and, in my case, a NeuG.

The broad category of random sequences of this nature are known as pseudorandom
number generators (PRNG). The goals of a PRNG typically fall under one of the
following categories:

- robustness to prediction
- fairness: uniform distribution

There is no silver bullet, hence several algorithms exist with no clear winner
in the general case.

Let us first consider "fairness".

In applications, the concept of "uniform distribution" may be different to an
equal distribution of ones and zeros at the bit level. For example, in financial
applications it may be most important that numbers fill a high dimensional space
evenly by volume, even if this compromises the predictability of the sequence.
Such numbers are called low-discrepency sequences, or quasirandom, and are often
blended with a general PRNG. In addition, such usecases care about performance
and the ability to reproduce the results of a simulation. Indeed, there is even
a market for random data (not just the generators).

Pseudorandom numbers can be generated in userland from an initial "seed" value.

For example, the Java standard library provides java.util.Random which takes as
input a 48-bit seed and produces an infinite stream of bits that are
deterministic (see included Random.scala), passing the NIST tests:

    ~ dieharder -g 201 -d 101 -f java.dat
        test_name   |ntup| tsamples |psamples|  p-value |Assessment
            sts_runs|   2|    100000|     100|0.75172762|  PASSED

However, the sequence is barely tolerable for serious applications, so it is
common for applications to use a third party Mersenne Twister (see included
Mersenne.scala, provide by Apache Commons).

    ~ dieharder -g 201 -d 101 -f mersenne.dat
            sts_runs|   2|    100000|     100|0.37382675|  PASSED

Notably, most programming languages use Mersenne Twister as their default PRNG.

## Crypographically Secure Pseudo Random Number Generators (CSPRNG)

An observer who watches a sufficient number of iterations of a PRNG can predict
all future (and past) numbers by reconstructing the internal state of the
generator: around 600 observations for Mersenne Twister.

A subcategory of PRNGs are the cryptographically secure PRNGs which cannot be
forward or back predicted (in polynomial time).

The fact that NIST gives a passing grade to Mersenne should only be evidence
that it is a low bar and not appropriate for testing CSPRNGs. Therefore, the
validation of CSPRNGs must take additional logical arguments into account and
are continually open to challenge by the community. This is serious business, as
a weakness in a CSPRNG could undermine the global economic system.

An operating system must err on the side of caution and provide a CSPRNG, not a
PRNG. This is a good compromise, as it is possible for monte carlo simulations
to implement their own sequences in userland from an initial seed value, taken
from `/dev/random`.

## Design Choices for this Exercise

What users want:

> `/dev/random` is a readonly, infinite, stream of bits that are 0 or 1 with equal
> probability, each bit independent of the others. Each bit should be produced
> within a fast constant time.

What an operating system can realistically do:

> `/dev/random` is a read/write device that accepts true random bits provided by
> the user as entropy (even if they are not aware they are being watched) and
> produces a very long, yet finite, sequence of bits that look like they are
> equally and independently distributed, but might be predictable by a malicious
> user --- we can't be sure, it depends how smart and desperate they are. At
> worst it will block if there is not enough user-provided entropy, and at best
> it is slower than talking to the network. Life is meaningless. Abandon all
> hope.

Achieving this modest goal required the heroic work of 76 contributors,
primarily Theodore Ts'o [4].

Therefore we must backtrack on the original requirement and make some decisions
about what we realistically want to achieve:

1. Implement a CSPRNG.

2. Cheat and use my NeuG as a source of good entropy (no gathering and mixing of
   sources or updating of pools).

3. Ignore performance. The operating system uses highly optimised C code that
   runs in kernel space. I can't compete with that. Therefore I choose the
   language that I am most familiar with: Scala with Scalaz. I therefore choose
   to emphasise code readability.

4. Which CSPRNG? OS X and FreeBSD use the Yarrow algorithm, whereas Linux,
   OpenBSD and NetBSD all use the ChaCha20 algorithm for their non-blocking
   variant `/dev/urandom`. Yarrow is based on SHA-1, which was beaten by
   Symbiont's Lisa Yin [5]. Its successor, Fortuna, was rejected in Linux [6] on
   the grounds that it must wait for an unreasonable amount of entropy and is
   therefore against the design goals of `/dev/random`.

5. Linux uses a cryptographic hash function for `/dev/random`. However, the man
   page for `/dev/random` states "The `/dev/random` device is a legacy interface
   which dates back to a time where the cryptographic primitives used in the
   implementation of `/dev/urandom` were not widely trusted". Linux choose
   ChaCha20 [7] for `/dev/urandom` instead of Fortuna [8] because it does not
   require an entropy estimator. I will write a ChaCha20 stream cipher by
   producing a fresh 256 bit key from my entropy source and outputting the
   cipher itself.

Running the app looks like

    ~ sbt test assembly
      ...
      [info] ChaCha20
      [info] - should produce the expected block for a given input
      [info] - should produce an expected output state
      [info] - should increment the nonce on counter overflow
      ...
      [info] Tests: succeeded 3, failed 0, canceled 0, ignored 0, pending 0
      ...
      [info] Assembly up to date: /path/to/fommil.jar
    ~ java -jar /path/to/fommil.jar > fommil.dat
      C-c
    ~ dieharder -g 201 -d 101 -f fommil.dat
        test_name   |ntup| tsamples |psamples|  p-value |Assessment
            sts_runs|   2|    100000|     100|0.95545635|  PASSED

I should stress that I have opted for Scala out of time pressures. If
demonstrating Haskell is necessary to proceed, I can redo the code, or prove
Haskell capability in some other way, for example a web-service built on my Free
Software `hmonad` project [9].

- [4] https://github.com/torvalds/linux/blob/master/drivers/char/random.c
- [5] Wang X., Yin Y.L., Yu H. (2005) Finding Collisions in the Full SHA-1
- [6] https://lkml.org/lkml/2005/4/14/61
- [7] https://cr.yp.to/chacha/chacha-20080128.pdf
- [8] https://github.com/torvalds/linux/commit/e192be9d9a30555aae2ca1dc3aad37cba484cd4a
- [9] https://github.com/cakesolutions/docker-images-public/tree/master/hmonad
