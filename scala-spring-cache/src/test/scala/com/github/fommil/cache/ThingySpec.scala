package com.github.fommil.cache

import org.springframework.test.context.ContextConfiguration
import org.springframework.beans.factory.annotation.Autowired

@ContextConfiguration
class ThingySpec extends SpringSpecs {

  sequential

  @Autowired
  val thingy: Thingy = null

  "expensive operation" should {
    "be using its cache" in {
      thingy.expensive()
      thingy.expensive()
      thingy.called === 1
    }

    "be internally using its cache" in {
      thingy.internallyCalling()
      thingy.internallyCalling()
      thingy.called === 1
    }
  }

}
