package com.github.fommil.cache

import org.specs2.mutable.{Specification, SpecificationWithJUnit}
import org.specs2.time.NoTimeConversions
import akka.contrib.jul.JavaLogging
import org.specs2.execute.Result
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import org.specs2.specification.{Step, Fragments}
import org.springframework.test.context.TestContextManager

/** Convenience for creating specs. */
trait Specs extends Specification with NoTimeConversions with JavaLogging {

  /** Timed specs (in the absence of Akka TestKit). */
  def within(duration: Duration)(code: => Result) = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val exec = Future {
      code
    }
    Await.result(exec, duration)
  }

}

/** Brings `@Autowired` (and friends) to `Specification`s. */
trait SpringSpecs extends Specs {

  override def map(fs: => Fragments) = Step(wire()) ^ super.map(fs)

  private def wire() {
    log.debug(s"wiring ${this.getClass}")
    new TestContextManager(this.getClass()).prepareTestInstance(this)
  }

}
