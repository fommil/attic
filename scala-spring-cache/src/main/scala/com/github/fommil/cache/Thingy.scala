package com.github.fommil.cache

import akka.contrib.jul.JavaLogging
import com.googlecode.ehcache.annotations.{DecoratedCacheType, Cacheable}
import DecoratedCacheType._
import org.springframework.stereotype.Service
import org.springframework.context.annotation.{ScopedProxyMode, Scope}
import ScopedProxyMode._

@Service
@Scope(proxyMode = TARGET_CLASS)
class Thingy extends JavaLogging {

  var called = 0

  @Cacheable(cacheName = "thingy", decoratedCacheType = SELF_POPULATING_CACHE)
  def expensive() = {
    called += 1
    Thread.sleep(1000)
    "bob"
  }

  def internallyCalling() = expensive()

}
