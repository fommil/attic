name := "scala-spring-cache"

version := "1.0-SNAPSHOT"

organization := "com.github.fommil"

scalaVersion := "2.10.2"

resolvers ++= Seq(
                Resolver.mavenLocal,
                Resolver.sonatypeRepo("releases"),
                Resolver.sonatypeRepo("snapshots"),
                Resolver.typesafeRepo("releases"),
                Resolver.typesafeRepo("snapshots")
              )

libraryDependencies <<= scalaVersion { scala_version => 
    Seq(
        "com.github.fommil"    %  "java-logging"             % "1.0",
        "com.googlecode.ehcache-spring-annotations" % "ehcache-spring-annotations" % "1.2.0",
        "org.springframework"  % "spring-context-support"    % "3.2.3.RELEASE" exclude("commons-logging", "commons-logging"),
        "org.aspectj"          % "aspectjrt"                 % "1.7.3",
        "org.aspectj"          % "aspectjweaver"             % "1.7.3",
        "com.typesafe.akka"    %% "akka-contrib"             % "2.1.2" intransitive(),
        "com.typesafe.akka"    %% "akka-actor"               % "2.1.2",
        "org.specs2"           %% "specs2"                   % "2.1.1" % "test",
        "org.springframework"  % "spring-test"               % "3.2.3.RELEASE" % "test"
    )
}


fork := true

javaOptions += "-Xmx2G -Djava.util.logging.config.file=logging.properties"

outputStrategy := Some(StdoutOutput)

net.virtualvoid.sbt.graph.Plugin.graphSettings