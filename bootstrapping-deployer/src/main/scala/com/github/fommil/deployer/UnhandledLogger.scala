package com.github.fommil.deployer

import akka.actor.{UnhandledMessage, Actor, ActorLogging}
import akka.event.slf4j.SLF4JLogging
import com.typesafe.config.ConfigFactory.{load => config}

// workaround inadequacies in Akka 2.0.x
class UnhandledLogger extends Actor with SLF4JLogging {

  override def preStart = {
    if (config.getBoolean("akka.actor.debug.unhandled"))
      context.system.eventStream.subscribe(self, classOf[UnhandledMessage])
  }

  def receive = {
    case UnhandledMessage(msg, sender, recipient) =>
        log.warn(sender + " to " + recipient + ": " + msg)
  }
}
