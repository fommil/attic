package com.github.fommil.deployer.drone

import akka.actor._
import akka.event.LoggingReceive
import com.typesafe.config.ConfigFactory
import com.github.fommil.deployer._
import akka.event.slf4j.SLF4JLogging
import akka.remote.RemoteActorRefProvider

object DroneApp extends App with SLF4JLogging {

  val system = ActorSystem("Drone")
  system.actorOf(Props[UnhandledLogger], "unhandled")

  val info = system.actorOf(Props[InfoActor], "info")
  val monitor = system.actorOf(Props[MonitorActor], "monitor")
  val launcher = system.actorOf(Props(new LaunchActor(monitor)), "launch")

  val address = system.asInstanceOf[ExtendedActorSystem].provider.asInstanceOf[RemoteActorRefProvider].transport.address

  log.info("starting ActorSystem as " + info.path.toStringWithAddress(address))

}

class InfoActor extends Actor with ActorLogging {

  import ConfigFactory.{load => config}

  val version = config.getInt("com.github.fommil.deployer.version")

  def receive = LoggingReceive {
    case d: RequestDetails => sender ! DroneDetails(version)
    case s: Shutdown => context.system.shutdown()
  }
}
