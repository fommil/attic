package com.github.fommil.deployer.driver

import akka.actor.{Address, Props, ActorSystem}
import com.github.fommil.deployer._
import com.github.fommil.deployer.drone.DroneApp
import akka.event.slf4j.SLF4JLogging
import com.typesafe.config.ConfigFactory

object DriverApp extends App with SLF4JLogging {

  // single JVM HACK
  DroneApp.main(args)

  val config = ConfigFactory.parseString(
    """akka.remote.netty.port = 0"""
  ).withFallback(ConfigFactory.load())

  val system = ActorSystem("Driver", config)
  system.actorOf(Props[UnhandledLogger], "unhandled")

  val bootstrapper = system.actorOf(Props[Bootstrapper], "bootstrap")

  // must use IP addresses: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=316099
  val localhost = Address("akka", "Drone", config.getString("akka.remote.netty.hostname"), 1337)
  val rory = Address("akka", "Drone", "10.4.1.128", 1337)

  bootstrapper ! Bootstrap(localhost)
  bootstrapper ! Bootstrap(rory)


  //  system.shutdown()

}
