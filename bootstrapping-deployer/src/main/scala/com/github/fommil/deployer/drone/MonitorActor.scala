package com.github.fommil.deployer.drone

import akka.actor.Actor
import akka.event.LoggingReceive
import java.util.UUID
import com.github.fommil.deployer.{Status, RequestStatus, Monitoring}

class MonitorActor extends Actor {

  var monitored = Map[UUID, Launched]()

  def receive = LoggingReceive {
    case app@Launched(exe, by, process, stdout, stderr) =>
      val uuid = UUID.randomUUID()
      monitored += (uuid -> app)
      by ! Monitoring(uuid, exe)

    case RequestStatus(uuid) =>
      val up = monitored.get(uuid) match {
        case Some(launched) if launched.alive => true
        case _ => false
      }
      sender ! Status(uuid, up)
  }
}
