package com.github.fommil.deployer

import java.io.File
import java.util.UUID

case class RequestDir()
case class CreatedDir(file: File)


case class Execute(command: String,
                   args: List[String] = Nil,
                   dir: File = new File("."),
                   env: Map[String, String] = Map.empty)
case class Monitoring(uuid: UUID, exe: Execute)

case class RequestStatus(uuid: UUID)
case class Status(uuid: UUID, up: Boolean)

case class Shutdown()
case class RequestDetails()
case class DroneDetails(version: Int)

/*NOTES
http://javadigest.wordpress.com/2012/08/26/rsa-encryption-example/

http://www.jcraft.com/jsch/examples/ScpTo.java.html
http://stackoverflow.com/questions/8490293
 */