package com.github.fommil.deployer.driver

import akka.actor._
import akka.event.LoggingReceive
import com.github.fommil.deployer._
import com.typesafe.config.ConfigFactory
import ConfigFactory.{load => config}
import akka.dispatch.Future
import scala.collection.immutable.Queue
import com.github.fommil.deployer.Shutdown
import com.github.fommil.deployer.RequestDetails
import akka.remote.RemoteClientShutdown
import com.github.fommil.deployer.DroneDetails
import java.io.File
import org.zeroturnaround.zip.ZipUtil
import org.apache.commons.io.FileUtils
import scala.collection.JavaConverters
import com.google.common.io.Files
import akka.util.{duration, Timeout}
import duration._
import FileUtils.listFiles
import akka.pattern.{ask, pipe}

case class Bootstrap(address: Address, force: Boolean = false)

trait RemoteActors {
  this: Actor =>

  def actorOn(address: Address, path: String) =
    context.system.actorFor(address + path)
}

class Bootstrapper extends Actor with ActorLogging with RemoteActors {

  val version = config.getInt("com.github.fommil.deployer.version")
  val libzip = new File(Files.createTempDir(), "deployer-" + version + "-libs.zip")
  val zip = new File(libzip.getParent, "deployer-" + version + ".zip")
  val classes = new File("./target/scala-2.9.2/classes")
  val jars = new File("./lib_managed")
  val bootstrap = new File("/var/lib/jenkins/deployer/bootstrap")
  val dest = new File(bootstrap, "deployer-" + version)

  var watching = Map[Address, Queue[ActorRef]]() withDefaultValue Queue.empty

  override def preStart = {
    context.system.eventStream.subscribe(self, classOf[RemoteClientShutdown])
  }

  def receive = LoggingReceive {

    case Bootstrap(remote, false) =>
      watching += remote -> (watching(remote) :+ sender)
      log.info("sending to " +  actorOn(remote, "/user/info"))
      actorOn(remote, "/user/info") ! RequestDetails()

    case Bootstrap(a@Address(_, _, Some(host), _), _) if watching contains a =>
      val watchers = watching(a)
      watching -= a

      // this blocks like crazy, but needs to
      if (!zip.exists() && !libzip.exists()) {
        log.info("Building the Deployer zip for distribution")
        ZipUtil.pack(classes, zip)
        ZipUtil.pack(jars, libzip)
        log.info("Built the Deployer zip")
      }

      // TODO: do this in an anonymous actor
      // TODO: dist an sbt run script instead of our local classes/libs
      Future {
        log.info("copying deployer to " + host)
        val ssh = new SshClient("jenkins", host)
        ssh.mkdirs(bootstrap)
        val out = new File(bootstrap, zip.getName)
        val libout = new File(bootstrap, libzip.getName)
        ssh.scp(zip, out)
        ssh.scp(libzip, libout)
        log.info("copied deployer to " + host)

        def unzip(from: File, to: String) {
          ssh.mkdirs(dest)
          ssh.run("/usr/bin/unzip -o " + from + " -d " + dest + "/" + to) match {
            case (_, "") =>
            case problem => log.warning(problem.toString())
          }
        }
        unzip(out, "classes")
        unzip(libout, "libs")
        log.info("unpacked deployer on " + host)

        import JavaConverters._
        val classpath = {
          "/classes" :: {
            listFiles(jars, Array("jar"), true).asScala.toList map {
              "/libs" + _.getPath.replace(jars.getPath, "")
            }
          }
        }.map { dest + _ }.mkString(":")
        val pid = ssh.nohup("java -cp " + classpath + " -Dakka.remote.netty.port=1337 -Dakka.remote.netty.hostname=" + host + " com.github.fommil.deployer.drone.DroneApp")
        log.info("remotely started " + pid)

        watchers foreach { w =>
          implicit val timeout = Timeout(10 seconds)
          (actorOn(a, "/user/info") ? RequestDetails) pipeTo w
        }

      }(context.dispatcher)

    case d@DroneDetails(v) if v == version & watching.contains(sender.path.address) =>
      val remote = sender.path.address
      log.info("Latest version " + v + " on " + remote)

      watching(remote) foreach { _ forward d }
      watching -= remote

    case DroneDetails(v) if v != version =>
      val remote = sender.path.address
      log.info("Old version " + v + " on " + remote)
      actorOn(remote, "/user/info") ! Shutdown()

    case RemoteClientShutdown(_, address) =>
      if (watching contains address)
        self ! Bootstrap(address, true)

  }
}
