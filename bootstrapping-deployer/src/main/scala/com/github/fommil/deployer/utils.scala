package com.github.fommil.deployer

import java.io.{ByteArrayOutputStream, Closeable, InputStream, OutputStream}
import akka.event.slf4j.SLF4JLogging
import org.slf4j
import akka.dispatch.ExecutionContext._
import java.util.concurrent.Executors._
import java.util.concurrent.{CountDownLatch, ThreadFactory}
import java.lang.Thread.UncaughtExceptionHandler
import akka.dispatch.{ExecutionContext, Future}
import com.google.common.io.ByteStreams
import scala.Int
import akka.event.LoggingAdapter
import akka.util.{duration, Timeout}
import duration._

object Utils extends SLF4JLogging {

  object AskTimeout {
    implicit val askTimeout = Timeout(1 hour)
  }

  class PimpedOutputStream(out: OutputStream) {
    def write(string: String) = {
      log.debug(string)
      out.write((string + "\n").getBytes)
    }
  }

  implicit def writeStringOutputStream(out: OutputStream) = new PimpedOutputStream(out)

  class PimpedInputStream(in: InputStream) {
    def readLine() = {
      val buf = new StringBuilder
      var char = '0'
      while (in.available > 0 && char != '\n') {
        char = in.read().toChar
        buf += char
      }
      buf.toString
    }
  }

  implicit def readLineInputStream(in: InputStream) = new PimpedInputStream(in)

  def withClose[A <: Closeable, B](resource: => A)(code: A => B) = {
    val r = resource
    try code(r)
    finally r.close()
  }

  def withDisconnect[A <: {def disconnect() : Unit}, B](resource: => A)(code: A => B) = {
    val r = resource
    try code(r)
    finally r.disconnect()
  }

  class PimpedAny[T](a: T) {
    def withEffect(effect: T => Unit) = {
      effect(a)
      a
    }
  }

  implicit def withEffectAny[T](a: T) = new PimpedAny(a)

  def drainInputsCompletely(inputs: InputStream*)(implicit ctx: ExecutionContext) = {
    val latch = new CountDownLatch(inputs.size)
    val outputs = inputs map { i => (i, new ByteArrayOutputStream) }
    def drain(from: InputStream, to: OutputStream) = Future {
      try ByteStreams.copy(from, to)
      finally latch.countDown()
    }
    outputs foreach { i => drain(i._1, i._2) }
    latch.await()
    outputs map { _._2 }
  }

  def drainInputsContinuously(inputs: InputStream*)(implicit ctx: ExecutionContext) = {
    val outputs = inputs map { i => (i, new ByteArrayOutputStream) }
    def drain(from: InputStream, to: OutputStream) = Future {
      ByteStreams.copy(from, to)
    }
    outputs foreach { i => drain(i._1, i._2) }
    outputs map { _._2 }
  }

  // oh pre Scala 2.10 joy
  def unboundedExecutionContext(log: Either[slf4j.Logger, LoggingAdapter]): ExecutionContext =
    fromExecutor(newCachedThreadPool(new ThreadFactory {
      def newThread(r: Runnable): Thread = {
        defaultThreadFactory().newThread(r) withEffect { t =>
          t.setDaemon(true)
          t.setUncaughtExceptionHandler(new UncaughtExceptionHandler {
            def uncaughtException(t: Thread, e: Throwable) {
              log match {
                case Left(log) => log.error("uncaught in " + t, e)
                case Right(log) => log.error(e, "uncaught in " + t)
              }
            }
          })
        }
      }
    }))

  def unboundedExecutionContext(log: slf4j.Logger): ExecutionContext = unboundedExecutionContext(Left(log))

  def unboundedExecutionContext(log: LoggingAdapter): ExecutionContext = unboundedExecutionContext(Right(log))


  object Int {
    def unapply(s: String): Option[Int] = try {
      Some(s.toInt)
    } catch {
      case _: java.lang.NumberFormatException => None
    }
  }

  def ??? = throw new UnsupportedOperationException("not implemented")

}