package com.github.fommil.deployer

import akka.event.slf4j.SLF4JLogging
import com.google.common.io.ByteStreams
import com.jcraft.jsch.{ChannelExec, JSch}
import java.io._
import java.net.ProtocolException
import Utils._
import java.lang.System._
import java.util.UUID

/**
 * Helper class for using the JSch library. All methods throw exceptions if they failed.
 *
 * @see https://blogs.oracle.com/janp/entry/how_the_scp_protocol_works
 * @see http://www.ietf.org/rfc/rfc4254.txt
 * @param privateKey
 * @param username
 * @param hostname
 */
class SshClient(username: String,
                hostname: String,
                privateKey: File = new File(getProperty("user.home") + "/.ssh/id_rsa")
                 ) extends SLF4JLogging {

  protected val jsch = new JSch()
  jsch.addIdentity(privateKey.getCanonicalPath)

  protected implicit val ctx = unboundedExecutionContext(log)

  /**
   * Programmatic version of scp, does not support recursive directory copy.
   * Relative paths should begin with `./` to avoid problems
   * (even if the relative path is the pwd).
   *
   * @param local must be a file
   * @param remote the parent directory must exist
   * @see http://www.jcraft.com/jsch/examples/ScpTo.java
   */
  def scp(local: File, remote: File) {
    require(local isFile, local + " is not a file")
    scp(new FileInputStream(local), local.length(), remote)
  }

  /**
   * Programmatic version of scp, does not support recursive directory copy or
   * creation of directories. Relative paths should begin with `./` to avoid
   * problems (even if the relative path is the pwd).
   *
   * @param source
   * @param size
   * @param remote the parent directory must exist
   * @see http://www.jcraft.com/jsch/examples/ScpTo.java
   */
  def scp(source: InputStream, size: Long, remote: File) {
    require(remote.getParentFile != null, "missing remote directory: " + remote)

    exec("scp " + " -t " + remote.getParent) { (in, out, _) =>
      checkAck(in, out)

      out.write("C0600 " + size + " " + remote.getName)
      checkAck(in, out)

      withClose(source) { ByteStreams.copy(_, out) }

      out.write(0)
      checkAck(in, out)
    }
  }

  /**
   * Creates a directory path. Relative paths should begin with `./`
   *
   * @param remote
   */
  def mkdirs(remote: File) {
    val path = remote.getPath.split("/").toList match {
      case "" :: rest => "" :: rest
      case "." :: rest => "." :: rest
      case invalid => throw new IllegalArgumentException(invalid + " is not a valid path")
    }

    exec("scp " + " -rt " + path(0) + "/") { (in, out, _) =>
      checkAck(in, out)

      def recurse(dirs: List[String]): Unit = dirs match {
        case Nil =>
        case dir :: others =>
          out.write("D0600 0 " + dir)
          recurse(others)
          out.write("C")
      }
      recurse(path)
      checkAck(in, out)
    }
  }

  /**
   * @param command should be a fast completing task. Otherwise, subclass and use `exec`.
   * @return the (stdout, stderr) from running the command
   */
  def run(command: String) = {
    exec(command) { (in, _, err) =>
      val out = drainInputsCompletely(in, err).map { _.toString }
      (out(0), out(1))
    }
  }

  /**
   * Convenience for running background tasks on UNIX systems.
   *
   * @param command
   * @return the PID of the started process and location of the process output.
   */
  def nohup(command: String) = {
    val base = "/tmp/" + UUID.randomUUID()
    val stdout = base + ".stdout"
    val stderr = base + ".stderr"

    val (pid, err) = run("nohup " + command + " > " + stdout + " 2> " + stderr + " < /dev/null & echo $!")
    pid.trim match {
      case Int(i) if err.trim.isEmpty => (i, stdout, stderr)
      case pid => throw new RuntimeException(pid + ": " + err)
    }
  }

  protected def exec[A](command: String)
                       (op: (InputStream, OutputStream, InputStream) => A) = {
    withDisconnect(jsch.getSession(username, hostname)) { session =>
      session.setConfig("StrictHostKeyChecking", "no")
      session.connect()

      withDisconnect(session.openChannel("exec")) { channel =>
        log.debug(command)
        channel.asInstanceOf[ChannelExec].setCommand(command + "\n")

        withClose(channel.getOutputStream()) { out =>
          withClose(channel.getInputStream()) { in =>
            withClose(channel.asInstanceOf[ChannelExec].getErrStream) { err =>
              channel.connect()
              op(in, out, err)
            }
          }
        }
      }
    }
  }

  protected def checkAck(in: InputStream, out: Flushable) {
    out.flush()
    val resp = in.read()
    if (resp == 0) return
    if (resp < 0 || resp > 2)
      throw new ProtocolException("bad ACK " + resp)
    throw new ProtocolException(in.readLine)
  }

}