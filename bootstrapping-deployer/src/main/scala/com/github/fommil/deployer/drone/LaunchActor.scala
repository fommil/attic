package com.github.fommil.deployer.drone

import akka.actor.{Actor, ActorLogging, ActorRef}
import com.github.fommil.deployer.Execute
import akka.event.LoggingReceive
import scala.collection.JavaConverters._
import com.github.fommil.deployer.Utils._
import java.io.ByteArrayOutputStream

// warning: incredibly mutable and not serializable
case class Launched(exe: Execute,
                    by: ActorRef,
                    process: Process,
                    stdout: ByteArrayOutputStream,
                    stderr: ByteArrayOutputStream) {

  // http://stackoverflow.com/questions/4750470
  def pid: Option[Int] =
    if (!alive) None
    else process match {
      //case p@java.lang.UNIXProcess =>
      case other =>
        log.info("unsupported implementation " + other.getClass)
        None
    }

  def alive: Boolean = try {
    process.exitValue
    false
  } catch {
    case e: IllegalThreadStateException => true
  }

}

class LaunchActor(monitor: ActorRef) extends Actor with ActorLogging {

  def receive = LoggingReceive {
    case exe@Execute(command, args, dir, env) =>
      log.info("requested to load " + exe)

      // this would be a lot cleaner if JSR-121 survived

      val builder = new ProcessBuilder((command :: args).asJava).directory(dir)
      builder.environment().putAll(env.asJava)
      val process = builder.start()
      val stdout = process.getInputStream
      val stderr = process.getErrorStream

      implicit val ctx = unboundedExecutionContext(log)
      val out = drainInputsContinuously(stdout, stderr)
      monitor ! Launched(exe, sender, process, out(0), out(1))
  }
}
