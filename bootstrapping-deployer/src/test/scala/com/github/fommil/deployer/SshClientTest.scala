package com.github.fommil.deployer

import org.specs2.mutable.Specification
import com.google.common.io.Files
import java.io.File
import System.getProperty
import akka.event.slf4j.SLF4JLogging
import org.apache.commons.io.FileUtils
import org.specs2.matcher.MatchResult

// note that this test will fail if the user does not
// have private/public ssh keys, does not have their own
// public key listed under "authorized_keys",
// or is not running an OpenSSH server.
class SshClientTest extends Specification with SLF4JLogging {

  val localhost = new SshClient(getProperty("user.name"), "localhost")

  def withTmpDir[T](f: (File) => MatchResult[T]) {
    val base = Files.createTempDir()
    base.exists must beTrue and {
      f(base)
    } and {
      // Commons because Guava is annoying
      FileUtils.deleteDirectory(base)
      base.exists must beFalse
    }
  }

  "mkdirs" should {
    "work for localhost" in {
      withTmpDir { base =>
        val subs = List("a", "b", "c")
        val target = (base /: subs) { (r, s) => new File(r, s) }
        localhost.mkdirs(target)
        target.exists must beTrue
      }
    }
  }

  "scp" should {
    "work for localhost" in {
      withTmpDir { base =>
        val logback = new File("logback.xml")
        val target = new File(base, logback.getName)

        target.exists must beFalse and {
          localhost.scp(logback, target)
          target.isFile must beTrue
        }
      }
    }
  }

  "run" should {
    "work for localhost" in {
      val pwd = new File("./")
      val files = pwd.list().length + 2 // ls will add "." and "../"

      val (stdout, _) = localhost.run("ls -a " + pwd.getCanonicalPath)

      stdout.count(_ == '\n') === files
    }
  }
}
