// to generate an IDEA project type "sbt gen-idea"

organization := "com.github.fommil"

name := "bootstrapping-deployer"

version := "1.0-SNAPSHOT"

// this was originally written for a legacy project
scalaVersion := "2.9.2"

resolvers ++= Seq(
  Resolver.mavenLocal,
  Resolver.sonatypeRepo("releases"),
  Resolver.typesafeRepo("releases")
)

// to assist fixing transitive problems, type:
//   `sbt dependency-tree`
//   `sbt test:dependency-tree`

val akka_version = "2.0.5"

retrieveManaged := true

libraryDependencies ++= Seq(
  "com.typesafe.akka"  % "akka-actor"      % akka_version,
  "com.typesafe.akka"  % "akka-remote"     % akka_version,
  "com.typesafe.akka"  % "akka-slf4j"      % akka_version,
  "com.typesafe"       % "config"          % "1.2.0",
  "com.google.guava"   % "guava"           % "16.0",
  "com.jcraft"         % "jsch"            % "0.1.50",
  "commons-io"         % "commons-io"      % "2.4",
  "org.zeroturnaround" % "zt-zip"          % "1.7",
  "com.github.fommil"  % "openssh"         % "1.0",
  "com.typesafe.akka"  % "akka-testkit"    % akka_version % "test",
  "org.specs2"        %% "specs2"          % "1.12.4.1"   % "test",
  "junit"              % "junit"           % "4.11"
).map {_ excludeAll(
  ExclusionRule(name = "log4j"),
  ExclusionRule(name = "commons-logging"),
  ExclusionRule(organization = "org.slf4j")
)} ++ Seq(
  "ch.qos.logback"     % "logback-classic" % "1.0.13",
  "org.slf4j"          % "jcl-over-slf4j"  % "1.7.5",
  "org.slf4j"          % "jul-to-slf4j"    % "1.7.5"
)

// https://github.com/jrudolph/sbt-dependency-graph
net.virtualvoid.sbt.graph.Plugin.graphSettings

mainClass in (Compile, run) := Some("com.github.fommil.deployer.DriverApp")

javaOptions += "-Dakka.remote.netty.hostname=10.4.4.152"

javaOptions += "-Dakka.remote.netty.port=1337"
