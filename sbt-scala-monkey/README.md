# sbt-scala-monkey

SBT plugin to monkey patch the scala compiler

| Windows | GNU / Linux |
|---------|-------------|
| [![Build status](https://ci.appveyor.com/api/projects/status/2uaah6lkwt3137mh?svg=true)](https://ci.appveyor.com/project/fommil/sbt-scala-monkey) | [![Build Status](http://fommil.com/api/badges/fommil/sbt-scala-monkey/status.svg)](http://fommil.com/fommil/sbt-scala-monkey) |

For license information, see [src/main/resources](src/main/resources).

For examples, see the tests in [src/sbt-test/sbt-scala-monkey](src/sbt-test/sbt-scala-monkey).
