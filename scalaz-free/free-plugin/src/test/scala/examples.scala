// Copyright: 2017 - 2018 Sam Halliday, Pawel Szulc
// License: http://www.gnu.org/licenses/lgpl-3.0.en.html

package testing

import java.lang.String
import java.time.Instant

import scala.{ Int, Unit }
import scala.collection.immutable.Map

import scalaz._
import scalaz.effect._

@algebra
trait Drone[F[_]] {
  def getBacklog: F[Int]
  def getAgents: F[Int]
}

final case class MachineNode(id: String)

@algebra
trait Machines[F[_]] {
  def getTime: F[Instant]
  def getManaged: F[NonEmptyList[MachineNode]]
  def getAlive: F[Map[MachineNode, Instant]]
  def start(node: MachineNode): F[Unit]
  def stop(node: MachineNode): F[Unit]
}

// EVERYTHING BELOW THIS LINE SHOULD BE GENERATED

// scalafix:off DisableSyntax.keywords.null
object Drone {

  def bar: String = foo

  def liftM[F[_]: Monad, G[_[_], _]: MonadTrans](
    f: Drone[F]
  ): Drone[({ type L[a] = G[F, a] })#L] = null
  def liftIO[F[_]: MonadIO](io: Drone[IO]): Drone[F] = null

  sealed abstract class Ast[A]
  final case class GetBacklog() extends Ast[Int]
  final case class GetAgents()  extends Ast[Int]

  def liftF[F[_]](
    implicit I: Ast :<: F
  ): Drone[({ type L[a] = Free[F, a] })#L] = null
  def interpreter[F[_]](f: Drone[F]): Ast ~> F = null
}

object Machines {
  def liftM[F[_]: Monad, G[_[_], _]: MonadTrans](
    f: Machines[F]
  ): Machines[({ type L[a] = G[F, a] })#L] = null
  def liftIO[F[_]: MonadIO](io: Machines[IO]): Machines[F] = null

  sealed abstract class Ast[A]
  final case class GetTime()                extends Ast[Instant]
  final case class GetManaged()             extends Ast[NonEmptyList[MachineNode]]
  final case class GetAlive()               extends Ast[Map[MachineNode, Instant]]
  final case class Start(node: MachineNode) extends Ast[Unit]
  final case class Stop(node: MachineNode)  extends Ast[Unit]

  def liftF[F[_]](
    implicit I: Ast :<: F
  ): Machines[({ type L[a] = Free[F, a] })#L] = null
  def liftA[F[_]](
    implicit I: Ast :<: F
  ): Machines[({ type L[a] = FreeAp[F, a] })#L] = null
  def interpreter[F[_]](f: Machines[F]): Ast ~> F = null
}
