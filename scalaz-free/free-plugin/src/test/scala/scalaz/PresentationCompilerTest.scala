// Copyright: 2017 - 2018 Sam Halliday, Pawel Szulc
// License: http://www.gnu.org/licenses/lgpl-3.0.en.html

package scalaz

import org.scalatest._
import Matchers._
import OptionValues._
import org.ensime.pcplod._

class PresentationCompilerTest extends FlatSpec {
  "PresentationCompiler" should "not have errors" in withMrPlod(
    "interactive.scala"
  ) { mr =>
    mr.messages.shouldBe('empty)
  }

  it should "be able to perform type-at-point" in withMrPlod(
    "interactive.scala"
  ) { mr =>
    mr.typeAtPoint('ctx).value.shouldBe("example.Demo.Ctx[Int]")
  }
}
