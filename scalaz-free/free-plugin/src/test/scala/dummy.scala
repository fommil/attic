// Copyright: 2017 - 2018 Sam Halliday, Pawel Szulc
// License: http://www.gnu.org/licenses/lgpl-3.0.en.html

import scala.Null
import scala.annotation.Annotation

package scalaz {

  class algebra extends Annotation

  package macros {
    object FreeMacros {
      def liftM: Null       = null // scalafix:ok
      def liftIO: Null      = null // scalafix:ok
      def liftF: Null       = null // scalafix:ok
      def liftA: Null       = null // scalafix:ok
      def interpreter: Null = null // scalafix:ok
    }
  }

}
