// Copyright: 2017 - 2018 Sam Halliday, Pawel Szulc
// License: http://www.gnu.org/licenses/lgpl-3.0.en.html

import scalaz._, Scalaz._

package example

trait Drone[F[_]] {
  def getBacklog: F[Int]
  def getAgents: F[Int]
}

final case class MachineNode(id: String)

trait Machines[F[_]] {
  def getTime: F[Instant]
  def getManaged: F[NonEmptyList[MachineNode]]
  def getAlive: F[Map[MachineNode, Instant]]
  def start(node: MachineNode): F[Unit]
  def stop(node: MachineNode): F[Unit]
}

object Demo {
  def todo[F[_]: Monad](M: Machines[F], D: Drone[F]): F[Int] =
    for {
      work  <- D.getBacklog
      alive <- M.getAlive
    } yield (work - alive.size)

  type Ast[a] = Coproduct[Machines.Ast, Drone.Ast, a]
  type Ctx[a] = Free[Ast, a]
  val pr@ctx@ogram = todo[Ctx](Machines.liftF, Drone.liftF)
}
