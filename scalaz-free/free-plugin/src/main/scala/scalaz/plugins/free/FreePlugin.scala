// Copyright: 2017 - 2018 Sam Halliday, Pawel Szulc
// License: http://www.gnu.org/licenses/lgpl-3.0.en.html

package scalaz.plugins.free

import scala.tools.nsc._

class FreePlugin(override val global: Global) extends AnnotationPlugin(global) {
  override val name: String           = "free"
  override val triggers: List[String] = List("algebra")

  import global._

  private val FreeMacros =
    Select(Select(Select(Ident(nme.ROOTPKG), TermName("scalaz")),
                  TermName("macros")),
           TermName("FreeMacros"))
  def toGen(f: Tree, a: Tree, target: TermName): Tree =
    if (isIde || isScaladoc) Literal(Constant(null))
    else
      TypeApply(
        Select(FreeMacros.duplicate, target),
        List(f.duplicate, a.duplicate)
      )

  def updateClass(triggered: List[Tree], clazz: ClassDef): ClassDef = clazz
  def updateModule(triggered: List[Tree], module: ModuleDef): ModuleDef = module

  def updateCompanion(triggered: List[Tree],
                      clazz: ClassDef,
    companion: ModuleDef): ModuleDef = {

    val foo = DefDef(Modifiers(), TermName("foo"), List(), List(), Ident(TypeName("String")), Literal(Constant(null))).withAllPos(companion.pos)
    val foo_ = treeCopy.Template(
      companion.impl,
      companion.impl.parents,
      companion.impl.self,
      foo :: companion.impl.body
    )

    treeCopy.ModuleDef(companion, companion.mods, companion.name, foo_)

  }

}
