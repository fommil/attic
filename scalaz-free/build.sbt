val scalazVersion = "7.2.22"

addCommandAlias("cpl", "all compile test:compile")
addCommandAlias("fmt", "all scalafmtSbt scalafmt test:scalafmt")
addCommandAlias(
  "check",
  "all headerCheck test:headerCheck scalafmtSbtCheck scalafmtCheck test:scalafmtCheck"
)
addCommandAlias("lint", "all compile:scalafixTest test:scalafixTest")
addCommandAlias("fix", "all compile:scalafixCli test:scalafixCli")

val plugin = (project in file("free-plugin")).settings(
  name := "free-plugin",
  scalafixCli in Compile := {}, // scala-compiler code quality is too low
  scalafixTest in Compile := {},
  scalacOptions in Test += "-Yno-predef",
  scalacOptions in Test += "-Yno-imports", // checks for relative vs full fqn
  libraryDependencies ++= Seq(
    "org.scala-lang" % "scala-compiler" % scalaVersion.value % "provided",
    "org.ensime"     %% "pcplod"        % "1.2.1"            % "test",
    "org.scalaz"     %% "scalaz-core"   % scalazVersion      % "test",
    "org.scalaz"     %% "scalaz-effect" % scalazVersion      % "test"
  ),
  scalacOptions in Test ++= {
    val jar = (packageBin in Compile).value
    Seq(s"-Xplugin:${jar.getAbsolutePath}", s"-Jdummy=${jar.lastModified}")
  },
  scalacOptions in Test := {
    (scalacOptions in Test).value.filterNot(_.startsWith("-Ywarn-unused"))
  },
  javaOptions in Test += {
    val jar = (packageBin in Compile).value.getAbsolutePath
    s"-Dpcplod.settings=-Ymacro-expand:discard,-Xplugin:$jar",
  }
)

// doesn't sbt offer a way to do this?
def ScalazFree: Seq[Setting[_]] = Seq(
  scalacOptions ++= {
    val jar = (packageBin in (plugin, Compile)).value
    Seq(s"-Xplugin:${jar.getAbsolutePath}", s"-Jdummy=${jar.lastModified}")
  }
)

val macros = (project in file("free-macro"))
  .settings(ScalazFree)
  .settings(
    name := "free-macro",
    scalafixCli in Compile := {}, // scala-compiler code quality is too low
    scalafixTest in Compile := {},
    KindProjector,
    resourcesOnCompilerCp(Test),
    scalacOptions += "-Yno-predef",
    scalacOptions in Test += "-Yno-imports", // checks for relative vs full fqn
    libraryDependencies ++= Seq(
      "org.scala-lang" % "scala-compiler" % scalaVersion.value % "provided",
      "org.scala-lang" % "scala-reflect"  % scalaVersion.value % "provided",
      "org.scalaz"     %% "scalaz-core"   % scalazVersion      % "test",
      "org.scalaz"     %% "scalaz-effect" % scalazVersion      % "test"
    )
  )

// root project
skip in publish := true
