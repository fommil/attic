// Copyright: 2017 - 2018 Sam Halliday, Pawel Szulc
// License: http://www.gnu.org/licenses/lgpl-3.0.en.html

package scalaz.macros

import scala.reflect.macros.blackbox

final class FreeMacrosImpl(val c: blackbox.Context) {
  import c.universe._

  def liftM: Tree       = null
  def liftIO: Tree      = null
  def liftF: Tree       = null
  def liftA: Tree       = null
  def interpreter: Tree = null

}

object FreeMacros {
  def liftM: Null = macro FreeMacrosImpl.liftM
  def liftIO: Null = macro FreeMacrosImpl.liftIO
  def liftF: Null = macro FreeMacrosImpl.liftF
  def liftA: Null = macro FreeMacrosImpl.liftA
  def interpreter: Null = macro FreeMacrosImpl.interpreter
}
