/*
 * Copyright ThinkTank Maths Limited 2007
 *
 * This file is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this file.
 * If not, see <http://www.gnu.org/licenses/>.
 */
package almost.stalker;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import java.util.Vector;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextBox;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.obex.ClientSession;
import javax.obex.HeaderSet;
import javax.obex.Operation;

import javax.obex.ResponseCodes;
import thinktank.j2me.BMPGenerator;
import thinktank.j2me.BluetoothDeviceDiscover;
import thinktank.j2me.BluetoothServiceDiscover;
import thinktank.j2me.BluetoothServiceDiscover;
import thinktank.j2me.TTUtils;
import thinktank.j2me.TTUtils.ILogger;

/**
 * This application was inspired by Andy's desire to send messages to people over
 * bluetooth. It is not possible to send a text message as there is no protocol for that,
 * but it is possible to push a media file. This application generates an image from a
 * text input box, generates an image and then sends that to a bluetooth device.
 * 
 * @author Samuel Halliday, ThinkTank Maths Limited
 */
public class BluetoothStalker extends MIDlet implements CommandListener {

	// Clear text box content
	private static final Command CLEAR_COMMAND =
		new Command("Clear", Command.SCREEN, 1);

	// Exit command
	private static final Command EXIT_COMMAND =
		new Command("Exit", Command.EXIT, 0);

	// OK command
	private static final Command OK_COMMAND =
		new Command("OK", Command.OK, 0);

	private final Form logger;

	private TextBox textBox;

	public BluetoothStalker() {
		logger = new Form("ThinkLogger");
		TTUtils.registerLogger(new ILogger() {
			public void log(String message) {
				logger.append(message);
				Display.getDisplay(BluetoothStalker.this).setCurrent(logger);
			}
		});
	}

	// Command implementations.
	public void commandAction(Command c, Displayable d) {
		if (c == EXIT_COMMAND) {
			notifyDestroyed();
		} else if (c == OK_COMMAND) {
			TTUtils.log("OK pressed");
			String message = textBox.getString();
			Image image = renderMessage(message);
			Form form = new Form("Bluetooth Stalker");
			form.append(image);
			Display.getDisplay(this).setCurrent(form);
			BluetoothDeviceDiscover discoverer = new BluetoothDeviceDiscover();
			Hashtable devices = discoverer.discover();
			// possible review the image?
			chooseAndSendToDevice(devices, image);
		// send again? or refresh? persistently store?
		} else if (c == CLEAR_COMMAND) {
			textBox.setString(null);
		}
	}

	/**
	 * @param devices
	 *            a Map of BT friendly names to their device number
	 * @param image
	 *            the image to send
	 */
	private void chooseAndSendToDevice(final Hashtable devices,
		final Image image) {
		TTUtils.log(devices.toString());
		final String[] names = new String[devices.size()];
		Enumeration en = devices.keys();
		int i = 0;
		while (en.hasMoreElements()) {
			String name = (String) en.nextElement();
			names[i] = name;
			i++;
		}
		final List list = new List("Choose a device", Choice.IMPLICIT);
		for (int j = 0; j < names.length; j++) {
			list.append(names[j], null);
		}
		list.setCommandListener(new CommandListener() {
			public void commandAction(Command c, Displayable d) {
				int index = list.getSelectedIndex();
				TTUtils.log("Selected index was " + index + ", which is " +
					names[index]);
				try {
					sendToDevice((RemoteDevice) devices.get(names[index]),
						image);
				} catch (Exception e) {
					TTUtils.log("Failed: " + e.getMessage());
					Display.getDisplay(BluetoothStalker.this).setCurrent(logger);
				}
			}
		});
		Display.getDisplay(this).setCurrent(list);
	}

	/**
	 * @param string
	 * @return
	 */
	private Image renderMessage(String string) {
		// arbitrary size
		int width = 150;
		int height = 100;
		Image image = Image.createImage(width, height);
		Graphics g = image.getGraphics();
		g.setColor(255);
		g.drawString(string, width / 2, height / 2, Graphics.HCENTER |
			Graphics.BASELINE);
		return image;
	}

	private void sendToDevice(RemoteDevice device, Image image)
		throws IOException {
		// TODO: use the service discovery
		BluetoothServiceDiscover serviceDiscoverer =
			new BluetoothServiceDiscover(device, 0x0100, 0x1105);
		Vector records = serviceDiscoverer.discover();
		if (records.size() == 0)
			throw new IOException("Service not supported");

		String btConnectionURL =
			((ServiceRecord) records.firstElement()).getConnectionURL(
			ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
		TTUtils.log("Connecting to " + btConnectionURL);
		byte[] data = BMPGenerator.encodeBMP(image);

		Connection connection;
		try {
			connection =
				Connector.open(btConnectionURL);
		} catch (IOException e) {
			TTUtils.log("exception raised in connection");
			// some OBEX checks
			try {
				Class cs = Class.forName("javax.obex.ClientSession");
				Class hs = Class.forName("javax.obex.HeaderSet");
				Class op = Class.forName("javax.obex.Operation");
				TTUtils.log("found all the OBEX classes");
			} catch (ClassNotFoundException e2) {
				TTUtils.log("failed to find OBEX class " + e2.getMessage());
			}
			throw e;
		}

		ClientSession cs = (ClientSession) connection;
		HeaderSet hs = cs.createHeaderSet();
		// we are sending one file...
		hs.setHeader(HeaderSet.COUNT, new Long(1L));
		HeaderSet response = cs.connect(hs);
		if (response.getResponseCode() !=
			ResponseCodes.OBEX_HTTP_OK) {
			TTUtils.log("Failed to connect " +
				response.getHeader(HeaderSet.DESCRIPTION));
			cs.close();
		}
		try {
			HeaderSet header = cs.createHeaderSet();
			header.setHeader(HeaderSet.NAME, "message.bmp");
			// image/x-ms-bmp
			header.setHeader(HeaderSet.TYPE, "image/bmp");
			header.setHeader(HeaderSet.LENGTH, new Long(data.length));

			Operation putOperation = cs.put(header);
			OutputStream outputStream = putOperation.openOutputStream();
			outputStream.write(data);
			outputStream.close();
			putOperation.close();
			TTUtils.log("File sent!");
		} finally {
			cs.disconnect(null);
			cs.close();
		}
	}

	protected void destroyApp(boolean unConditional)
		throws MIDletStateChangeException {
	}

	protected void pauseApp() {
	}

	protected void startApp() {
		// Form form = new Form("Bluetooth Stalker\n");
		textBox = new TextBox("Write your message", null, 140, TextField.ANY);
		textBox.addCommand(OK_COMMAND);
		textBox.addCommand(EXIT_COMMAND);
		textBox.addCommand(CLEAR_COMMAND);
		textBox.setCommandListener(this);

		Display display = Display.getDisplay(this);
		display.setCurrent(textBox);
	}
}
