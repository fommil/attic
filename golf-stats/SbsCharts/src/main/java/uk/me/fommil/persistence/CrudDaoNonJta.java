/*
 * Copyright (c) 2009 Samuel Halliday <Sam.Halliday@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package uk.me.fommil.persistence;

import com.google.common.base.Preconditions;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.transaction.Transaction;

/**
 * Simple no-fuss CRUD (CREATE, READ, UPDATE, DELETE) DAO (Data Access Object) for {@link Entity}s
 * in a non-JTA JPA environment. Every action is wrapped by a {@link Transaction}, meaning
 * clients could be prone to Last Commit Wins concurrency problems.
 *
 * @param <T> the class of the type
 * @param <K> the class of the type's primary key
 * @author Samuel Halliday
 */
public abstract class CrudDaoNonJta<T, K> {

	private final Class<T> klass;

	private final EntityManagerFactory emf;

	/**
	 * @param klass class of the entity type, must be an {@link Entity} type.
	 * @param emf
	 */
	protected CrudDaoNonJta(Class<T> klass, EntityManagerFactory emf) {
		Preconditions.checkNotNull(klass);
		Preconditions.checkNotNull(emf);
		this.klass = klass;
		this.emf = emf;
		Preconditions.checkArgument(klass.getAnnotations() != null);
		boolean entity = false;
		for (Annotation annotation : klass.getAnnotations()) {
			if (annotation.annotationType().equals(Entity.class))
				entity = true;
		}
		Preconditions.checkArgument(entity);
	}

	/**
	 * @return an entity manager, must be closed after use.
	 */
	protected EntityManager createEntityManager() {
		return emf.createEntityManager();
	}

	/**
	 * Note that if the primary key is user-generated, you will experience an exception if an
	 * entity already exists with that key.
	 *
	 * @param entity
	 * @throws PersistenceException
	 */
	public void create(T entity) {
		Preconditions.checkNotNull(entity);
		EntityManager em = createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
		} catch (PersistenceException e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	/**
	 * Note that if the primary key is user-generated, you will experience an exception if an
	 * entity already exists with the key of any of these entities and the entire create call
	 * will fail.
	 *
	 * @param collection
	 * @throws PersistenceException
	 */
	public void create(Collection<T> collection) {
		Preconditions.checkNotNull(collection);
		if (collection.isEmpty())
			return;
		EntityManager em = createEntityManager();
		try {
			em.getTransaction().begin();
			for (T entity : collection) {
				em.persist(entity);
			}
			em.getTransaction().commit();
		} catch (PersistenceException e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	/**
	 * @param key
	 * @return the entity with the given key, or null if not found
	 * @throws PersistenceException
	 */
	public T read(K key) {
		Preconditions.checkNotNull(key);
		EntityManager em = createEntityManager();
		try {
			return em.find(klass, key);
		} finally {
			em.close();
		}
	}

	/**
	 * Update the database with the existing entity.
	 *
	 * @param entity
	 * @return the updated entity (which will be different from the one passed in)
	 * @throws PersistenceException
	 */
	public T update(T entity) {
		Preconditions.checkNotNull(entity);
		EntityManager em = createEntityManager();
		try {
			em.getTransaction().begin();
			entity = em.merge(entity);
			em.getTransaction().commit();
			return entity;
		} catch (PersistenceException e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	/**
	 * @param entity
	 * @throws PersistenceException
	 */
	public void delete(T entity) {
		Preconditions.checkNotNull(entity);
		EntityManager em = createEntityManager();
		try {
			em.getTransaction().begin();
			entity = em.merge(entity);
			em.remove(entity);
			em.getTransaction().commit();
		} catch (PersistenceException e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	/**
	 * @param id
	 * @throws PersistenceException
	 */
	public void deleteById(K id) {
		Preconditions.checkNotNull(id);
		EntityManager em = createEntityManager();
		try {
			em.getTransaction().begin();
			T entity = em.find(klass, id);
			if (entity != null)
				em.remove(entity);
			em.getTransaction().commit();
		} catch (PersistenceException e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	/**
	 * @return a count of the number of entities in the database.
	 */
	public long count() {
		EntityManager em = createEntityManager();
		Query q = em.createQuery("SELECT COUNT(s) FROM " + getTableName() + " s");
		Long result = querySingle(em, q);
		return result;
	}

	/**
	 * Return all entities in the database.
	 * <p>
	 * <b>WARNING: ONLY TO BE USED FOR SMALL DATABASES</b>
	 *
	 * @return
	 */
	public List<T> readAll() {
		EntityManager em = createEntityManager();
		Query query = em.createQuery("SELECT s FROM " + getTableName() + " s");
		return query(em, query);
	}

	/**
	 * @return the table name in JPA SQL (i.e. the simple class name of the managed entity)
	 */
	protected final String getTableName() {
		// final to avoid possible SQL injection attacks
		return klass.getSimpleName();
	}

	/**
	 * Boilerplate saver - runs the given {@link Query}, expecting a list of {@link Entity}s of given
	 * type, closing the {@link EntityManager}. Never returns {@code null}.
	 *
	 * @param <C>
	 * @param em
	 * @param query
	 * @return
	 */
	protected <C> List<C> query(EntityManager em, Query query) {
		try {
			Preconditions.checkNotNull(em);
			Preconditions.checkNotNull(query);
			@SuppressWarnings("unchecked")
			List<C> result = (List<C>) query.getResultList();
			if (result == null)
				return Collections.emptyList();
			return result;
		} finally {
			em.close();
		}
	}

	/**
	 * Boilerplate saver - runs the given {@link Query}, expecting a single {@link Entity} of type
	 * {@code T}, closing the {@link EntityManager}. Never returns {@code null}.
	 *
	 * @param <C>
	 * @param em
	 * @param query
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected <C> C querySingle(EntityManager em, Query query) {
		try {
			Preconditions.checkNotNull(em);
			Preconditions.checkNotNull(query);
			return (C) query.getSingleResult();
		} finally {
			em.close();
		}
	}
}
