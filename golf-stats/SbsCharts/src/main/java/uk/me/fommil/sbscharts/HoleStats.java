/*
 * Copyright (c) 2011 Samuel Halliday <Sam.Halliday@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package uk.me.fommil.sbscharts;

import com.google.common.base.Objects;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Query;
import javax.persistence.Temporal;
import uk.me.fommil.persistence.CrudDaoNonJta;

/**
 * @author Samuel Halliday
 */
@Entity
@SuppressWarnings("serial")
public class HoleStats implements Serializable {

	private static final Logger log = Logger.getLogger(HoleStats.class.getName());

	/** */
	public static class Dao extends CrudDaoNonJta<HoleStats, Long> {

		/**
		 * @param emf
		 */
		public Dao(EntityManagerFactory emf) {
			super(HoleStats.class, emf);
		}

		public List<HoleStats> searchByPlayer(Long player) {
			EntityManager em = createEntityManager();
			Query query = em.createQuery("SELECT s FROM " + getTableName() + " s "
					+ "WHERE s.pkPlayer = :player");
			query.setParameter("player", player);
			return query(em, query);
		}

		public Long randomPlayer() {
			EntityManager em = createEntityManager();
			Query query = em.createQuery("SELECT DISTINCT pkPlayer FROM " + getTableName());
			List<Long> players = query(em, query);
			log.info(players.size() + " unique players");
			Long player = players.get(new Random().nextInt(players.size()));
			return player;
		}
	}

	@Id
	@GeneratedValue
	private Long id;

	private String sName, sDrive;

	private Integer iHole, iHCP, iPar, iScore, iPutts, iPenaltyStrokes, iBunkerHit, iFairwayHit;

	@org.hibernate.annotations.Index(name = "myNameIndex")
	private Long pkPlayer;

	private Long pkCourse, pkScorecard, pkScorecardHole;

	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date dtInserted;

	/**
	 * @return an array of strings, matching the CSV format of SbS' /MyAccount/ExportResults
	 */
	public String[] toStringArray() {
		DateFormat formatter = new SimpleDateFormat("M/d/yyyy h:mm:ss aa");

		// Player's Email, Course Name, Scorecard Id , iHole, iHCP, iPar, iScore, iPutts,
		// iPenaltyStrokes, iBunkerHit, iFairwayHit, sDriveDirection, dtInserted
		String[] cols = new String[13];
		cols[0] = getPkPlayer() + "@example.com";
		cols[1] = "\\\"" + getsName() + "\\\"";
		cols[2] = getPkScorecard().toString();
		cols[3] = getiHole().toString();
		cols[4] = getiHCP().toString();
		cols[5] = getiPar().toString();
		cols[6] = getiScore().toString();
		cols[7] = getiPutts().toString();
		cols[8] = getiPenaltyStrokes().toString();
		cols[9] = getiBunkerHit().toString();
		cols[10] = getiFairwayHit().toString();
		cols[11] = getsDrive();
		cols[12] = formatter.format(getDtInserted());

		return cols;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof HoleStats) || id == null)
			return false;
		final HoleStats other = (HoleStats) obj;
		return Objects.equal(id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	// <editor-fold defaultstate="collapsed" desc="BOILERPLATE GETTERS/SETTERS">
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtInserted() {
		return dtInserted;
	}

	public void setDtInserted(Date dtInserted) {
		this.dtInserted = dtInserted;
	}

	public Integer getiBunkerHit() {
		return iBunkerHit;
	}

	public void setiBunkerHit(Integer iBunkerHit) {
		this.iBunkerHit = iBunkerHit;
	}

	public Integer getiFairwayHit() {
		return iFairwayHit;
	}

	public void setiFairwayHit(Integer iFairwayHit) {
		this.iFairwayHit = iFairwayHit;
	}

	public Integer getiHCP() {
		return iHCP;
	}

	public void setiHCP(Integer iHCP) {
		this.iHCP = iHCP;
	}

	public Integer getiHole() {
		return iHole;
	}

	public void setiHole(Integer iHole) {
		this.iHole = iHole;
	}

	public Integer getiPar() {
		return iPar;
	}

	public void setiPar(Integer iPar) {
		this.iPar = iPar;
	}

	public Integer getiPenaltyStrokes() {
		return iPenaltyStrokes;
	}

	public void setiPenaltyStrokes(Integer iPenaltyStrokes) {
		this.iPenaltyStrokes = iPenaltyStrokes;
	}

	public Integer getiPutts() {
		return iPutts;
	}

	public void setiPutts(Integer iPutts) {
		this.iPutts = iPutts;
	}

	public Integer getiScore() {
		return iScore;
	}

	public void setiScore(Integer iScore) {
		this.iScore = iScore;
	}

	public Long getPkCourse() {
		return pkCourse;
	}

	public void setPkCourse(Long pkCourse) {
		this.pkCourse = pkCourse;
	}

	public Long getPkPlayer() {
		return pkPlayer;
	}

	public void setPkPlayer(Long pkPlayer) {
		this.pkPlayer = pkPlayer;
	}

	public Long getPkScorecard() {
		return pkScorecard;
	}

	public void setPkScorecard(Long pkScorecard) {
		this.pkScorecard = pkScorecard;
	}

	public Long getPkScorecardHole() {
		return pkScorecardHole;
	}

	public void setPkScorecardHole(Long pkScorecardHole) {
		this.pkScorecardHole = pkScorecardHole;
	}

	public String getsDrive() {
		return sDrive;
	}

	public void setsDrive(String sDrive) {
		this.sDrive = sDrive;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}
	// </editor-fold>
}
