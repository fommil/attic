/*
 * Copyright (c) 2011 Samuel Halliday <Sam.Halliday@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package uk.me.fommil.sbscharts;

import com.google.common.base.Preconditions;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Samuel Halliday
 */
@WebServlet(name = "PopulateDatabaseServlet", urlPatterns = {"/populate"})
@SuppressWarnings("serial")
public class PopulateDatabaseServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(PopulateDatabaseServlet.class.getName());

	@PersistenceUnit
	private EntityManagerFactory emf;

	/**
	 * @param emf
	 * @throws IOException
	 */
	protected synchronized static void populateDatabase(EntityManagerFactory emf) throws IOException {
		{
			HoleStats.Dao dao = new HoleStats.Dao(emf);
			log.info("HoleStats entries in database " + dao.count());
			if (dao.count() == 0) {
				log.info("LOADING HoleStats entries");
				List<HoleStats> stats = HoleStatsParser.readFromFile();
				dao.create(stats);
				log.info("HoleStats entries in database " + dao.count());
			}
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Preconditions.checkNotNull(emf);
		populateDatabase(emf);
	}
}
