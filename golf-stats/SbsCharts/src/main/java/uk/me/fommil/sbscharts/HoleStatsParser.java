/*
 * Copyright (c) 2011 Samuel Halliday <Sam.Halliday@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package uk.me.fommil.sbscharts;

import au.com.bytecode.opencsv.CSVReader;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.io.Closeables;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author Samuel Halliday
 */
public class HoleStatsParser {

	private static final Logger log = Logger.getLogger(HoleStatsParser.class.getName());

	protected static final String DATA_FILE = "/Users/samuel/Documents/SwingBySwing/data/Player Scorecard Detail Import Partial 20111027.zip";

	/**
	 * @return
	 * @throws IOException
	 */
	public static List<HoleStats> readFromFile() throws IOException {
		try {
			List<HoleStats> holes = Lists.newArrayList();
			File file = new File(DATA_FILE);
			ZipFile zip = new ZipFile(file);
			try {
				Enumeration<? extends ZipEntry> en = zip.entries();
				while (en.hasMoreElements()) {
					ZipEntry e = en.nextElement();
					log.info(e.getName());
					InputStream in = zip.getInputStream(e);
					try {
						InputStreamReader reader = new InputStreamReader(in);
						CSVReader csv = new CSVReader(reader);
						String[] line = null;
						while ((line = csv.readNext()) != null) {
							try {
								HoleStats hole = parse(line);
								holes.add(hole);
							} catch (ParseException ex) {
								log.info("skipping line " + ex.getMessage());
							}
						}
					} finally {
						Closeables.closeQuietly(in);
					}
				}
				return holes;
			} finally {
				try {
					zip.close();
				} catch (IOException e) {
					log.warning("couldn't close the zipfile");
				}
			}
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

	private static HoleStats parse(String[] line) throws ParseException {
		Preconditions.checkNotNull(line);

		// HACK: SbS data doesn't quote commas so workaround it
		if (line.length == 16) {
			String[] fixed = new String[15];
			for (int i = 0; i < 4; i++) {
				fixed[i] = line[i];
			}
			fixed[4] = line[4] + "," + line[5];
			for (int i = 5; i < 15; i++) {
				fixed[i] = line[i + 1];
			}
			line = fixed;
		}

		// local variable because of thread safety
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

		try {
			Long pkPlayer = Long.valueOf(line[0]);
			Long pkCourse = Long.valueOf(line[1]);
			Long pkScorecard = Long.valueOf(line[2]);
			Long pkScorecardHole = Long.valueOf(line[3]);
			String sName = line[4];
			Integer iHole = Integer.valueOf(line[5]);
			Integer iHCP = Integer.valueOf(line[6]);
			Integer iPar = Integer.valueOf(line[7]);
			Integer iScore = Integer.valueOf(line[8]);
			Integer iPutts = Integer.valueOf(line[9]);
			Integer iPenaltyStrokes = Integer.valueOf(line[10]);
			Integer iBunkerHit = Integer.valueOf(line[11]);
			Integer iFairwayHit = Integer.valueOf(line[12]);
			String sDrive = line[13];
			Date dtInserted = dateFormat.parse(line[14]);

			HoleStats hole = new HoleStats();
			hole.setPkPlayer(pkPlayer);
			hole.setPkCourse(pkCourse);
			hole.setPkScorecard(pkScorecard);
			hole.setPkScorecardHole(pkScorecardHole);
			hole.setsName(sName);
			hole.setiHole(iHole);
			hole.setiHCP(iHCP);
			hole.setiPar(iPar);
			hole.setiScore(iScore);
			hole.setiPutts(iPutts);
			hole.setiPenaltyStrokes(iPenaltyStrokes);
			hole.setiBunkerHit(iBunkerHit);
			hole.setiFairwayHit(iFairwayHit);
			hole.setsDrive(sDrive);
			hole.setDtInserted(dtInserted);

			return hole;
		} catch (NumberFormatException e) {
			throw new ParseException(Arrays.toString(line) + " " + e.getMessage(), -1);
		}
	}
}
