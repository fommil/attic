/*
 * Copyright (c) 2011 Samuel Halliday <Sam.Halliday@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package uk.me.fommil.sbscharts;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uk.me.fommil.sbscharts.HoleStats.Dao;

/**
 *
 * @author Samuel Halliday
 */
@WebServlet(name = "CsvServlet", urlPatterns = {"/csv"})
@SuppressWarnings("serial")
public class CsvServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(CsvServlet.class.getName());

	@PersistenceUnit
	private EntityManagerFactory emf;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Preconditions.checkNotNull(emf);

		Dao dao = new HoleStats.Dao(emf);
		Long player;
		try {
			player = Long.valueOf(request.getParameter("player"));
		} catch (NumberFormatException e) {
//			response.sendError(13, "No player specified");
			return;
		}
		List<HoleStats> stats = dao.searchByPlayer(player);

		response.setContentType("text/plain");
		PrintWriter writer = response.getWriter();
		writer.write("Player's Email, Course Name, Scorecard Id , iHole, iHCP, iPar, iScore, iPutts, iPenaltyStrokes, iBunkerHit, iFairwayHit, sDriveDirection, dtInserted\n");

		Joiner joiner = Joiner.on(","); // to get the exact SbS format

		for (HoleStats hole : stats) {
			String[] cols = hole.toStringArray();
			String line = joiner.join(cols);
			writer.write(line);
			writer.write("\n");
		}
	}
}
