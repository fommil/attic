/*
 * Copyright (c) 2011 Samuel Halliday <Sam.Halliday@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package uk.me.fommil.sbscharts;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uk.me.fommil.sbscharts.HoleStats.Dao;

/**
 * Provides in-line inclusion of data as a Javascript object.
 *
 * @author Samuel Halliday
 */
@WebServlet(name = "JavascriptDataServlet", urlPatterns = {"/local_data"})
@SuppressWarnings("serial")
public class JavascriptDataServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(JavascriptDataServlet.class.getName());

	@PersistenceUnit
	private EntityManagerFactory emf;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Preconditions.checkNotNull(emf);

		Dao dao = new HoleStats.Dao(emf);
		Long player;
		try {
			//player = 323608L; // Sam
			//player = 1L; // Terrence
			//player = 2L; // Travis
			//player = 257389L; // Kevin
			player = Long.valueOf(request.getParameter("player"));
		} catch (NumberFormatException e) {
			player = dao.randomPlayer();
			log.info("randomly selected " + player);
		}
		List<HoleStats> stats = dao.searchByPlayer(player);

		final List<String> courses = Lists.newArrayList();
		StringBuilder builder = new StringBuilder();
		builder.append("\"");
		builder.append("Player's Email, Course Name, Scorecard Id , iHole, iHCP, iPar, iScore, iPutts, iPenaltyStrokes, iBunkerHit, iFairwayHit, sDriveDirection, dtInserted\\n");
		Joiner joiner = Joiner.on(","); // to get the exact SbS format
		for (HoleStats hole : stats) {
			courses.add(hole.getsName());
			String[] cols = hole.toStringArray();
			String line = joiner.join(cols);
			builder.append(line);
			builder.append("\\n");
		}
		builder.append("\"");

		String popular = Collections.max(Sets.newHashSet(courses), new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return Collections.frequency(courses, o1) - Collections.frequency(courses, o2);
			}
		});

		request.setAttribute("data", builder.toString());
		request.setAttribute("course", popular);
		request.setAttribute("player", player);
		request.setAttribute("database", new File(HoleStatsParser.DATA_FILE).getName());
	}
}
