<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/local_data"/>

<html>
	<head>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">
<%@ include file="ConsistencyVisualisations_compiled.js" %>
		</script>

		<script type="text/javascript">
			local_data = <c:out value='${data}' escapeXml='false' />;
	
			google.load('visualization', '1.0', {'packages':['corechart']});
			google.setOnLoadCallback(function() {
				getSbsData(function(data) {
					drawScoreVisualisationType(data, 3, 'par3');
					drawScoreVisualisationType(data, 4, 'par4');
					drawScoreVisualisationType(data, 5, 'par5');
					drawScoreVisualisationType(data, 0, 'putts');

					drawTeeVisualisation(data, "tee");
					
					drawCourseVisualisation(data, "<c:out value='${course}'/>", "course");
										
					drawRecentPerformanceVisualisation(data, 'recent');
				})
			});
		</script>
		<style type="text/css">
			body {
				font-family: Palatino;
				padding: 25px;
				text-align: justify;
			}
			h1, h2, h3 {
				text-align: center;
			}
			.notEnoughData {
				color: white;
				font-size: 24pt;
				background: #AAE694;
				text-align: center;
				display: table-cell;
				vertical-align: middle;
				-webkit-border-radius: 25px;
				border-radius: 25px;
			}
			.samChart {
				width:1000px;
				height:500px;
			}
		</style>
	</head>
	<body>
		<h1>Golf Consistency Visualisations, by Sam Halliday</h1>
		<p>
			This page contains a sample of the Golf Consistency Visualisations for player
	<c:out value='${player}'/>, using data from the Swing by Swing database
	"<code><c:out value='${database}'/></code>".
</p>
<h2>Score Visualisations</h2>		
<p>
	The following visualisations show the player's scoring on par 3, 4 and 5 holes over
	their entire history of using the SbS Scorecard function. These visualisations are
	intended to be a centrepiece of the player's <code>myaccount</code> page.
</p><p>
	The <b>average</b> trend-line
	should be familiar to most people. The areas around the average show the player's
	<i>consistency</i> around that average, revealing their <b>Best</b>, <b>Good</b>,
	<b>Bad</b> and <b>Worst</b> play. In order to calculate these values requires
	a statistical analysis of the player's scores over several rounds of golf.
</p><p>
	The visualisations are <i>interactive</i> and players are encouraged to move the mouse
	over different periods of their history for more information.
</p>

<h3>Par 3s</h3>
<div id="par3" class="samChart"></div>
<p>
	If the player has not recorded their scores for part of their history, their scores will be inferred
	and displayed using a faded colour scheme, without an average trend.
	If the player has not recorded enough data yet,
	they will be informed that they need to record more data in order to see the visualisations.
</p>
<h3>Par 4s</h3>
<div id="par4" class="samChart"></div>
<p>
	The primary role of the Score Visualisations is to highlight the player's consistency on
	different types of holes so that they can see improvement and notice areas where they
	should be focusing their efforts (e.g. more conservative playing to reduce the width of
	a particularly wide Bad or Worst area). A wide area of colour generally means
	inconsistency in some area of the player's game.
</p>
<h3>Par 5s</h3>
<div id="par5" class="samChart"></div>
<p>
	There are many ways to interpret the results of these visualisations. For example, if
	a player's average is typically closer to "Bad" than "Good", it means that when the player
	is playing badly their score is not wildly different to their average score.
	Alternatively, it could also be interpreted to mean that the player's average score is
	being heavily biased by a tendency to play Badly rather than Good. Both interpretations
	are correct. Mark Twain certainly got it right when he said
	"There are three kinds of lies: lies, damned lies, and statistics."
</p>
<h3>Putting</h3>
<p>
	If the player has been recording their putting data using SbS's
	Statistics function, then the following Putting visualisation will be available
	to the player.
</p><p>
	Chip-ins are not included because it is impossible to tell the
	difference between genuine chip-ins and incorrect data entry.
</p>

<div id="putts" class="samChart"></div>

<h2>Tee Accuracy Visualisations</h2>
<p>
	The following visualisation shows the player's driving/tee shot accuracy over
	their entire history of using the SbS Extended Statistics function.
	These visualisations are intended to be a centrepiece of the player's
	<code>myaccount</code> page.
</p><p>
	Again, the visualisations are interactive and the player is encouraged to move the mouse
	over their history to see a pie chart representation of their driving accuracy for
	a particular period of time.
</p>

<div id="tee" class="samChart"></div>

<h2>Course Visualisation</h2>
<p>
	As an added bonus, beyond the proposed scope of work, players can see a visualisation of their
	scoring at a particular course. This type of visualisation is intended to revitalise the
	<code>myrounds/viewcourse</code> pages.
</p><p>
	The following visualisation is an example for the player's most played course,
	"<c:out value='${course}'/>", but similar visualisations are available for all the player's courses.
</p><p>
	Here the player's scores are displayed for each hole of the course. The thicker box represents
	the region between their Good and Bad game, with the <i>whiskers</i> showing their Best and Worst.
	This visualisation really highlights problem holes which should cause the player to pause and
	reconsider their strategy on those holes.
</p><p>
	Yet again, players get an explanation of the visualisation by moving the mouse over the data they
	wish to learn more about. This is particularly useful for this visualisation, where it might
	be hard for the statistically illiterate to understand the "boxplots".
</p>

<h2><c:out value='${course}'/></h2>
<div id="course" class="samChart"></div>

<p>
	Also included is the player's per-hole Putting and where they are losing points due to Penalties.
	This allows players to know <b>when</b> they should be playing more conservatively (to avoid
	penalties) and which holes they should be visiting at sundown to practice putting on the green!
</p><p>
	It should be possible to include Bunker shots in this visualisation, but there is the danger that
	there is already too much information being presented.
</p>
<h2>Recent Performance Visualisation</h2>
<p>
	As another added bonus, beyond the proposed scope of work, players can see a visualisation of their
	recent performance in a style similar to the Course Visualisations. This visualisations is
	intended to be a key part of the player's <code>myaccount</code> page.
</p>
<div id="recent" class="samChart"></div>
<p>
	The availability of per-player golfing handicap data has the potential to extend all of the above
	visualisations to provide more value for SbS customers. Future changes to the Extended Statistics
	and Club Tracker open up new possibilitiesfor a higher level of game analysis through improved
	visualisation.
</p>

</body>
</html>
