<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
	<head>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<!-- DEVELOPMENT -->
		<script type="text/javascript" src="ConsistencyVisualisations.js"></script>
		<!-- DEPLOYMENT -->
		<!-- <script type="text/javascript" src="ConsistencyVisualisations_compiled.js"></script> -->
		<style type="text/css">
			body {
				font-family: Palatino;
				padding: 25px;
				text-align: justify;
			}
			h1, h2, h3 {
				text-align: center;
			}
			.notEnoughData {
				color: white;
				font-size: 24pt;
				background: #AAE694;
				text-align: center;
				display: table-cell;
				vertical-align: middle;
				-webkit-border-radius: 25px;
				border-radius: 25px;
			}
			.samChart {
				width:1000px;
				height:500px;
			}
		</style>
	</head>
	<body>
		<h1>Golf Consistency Visualisations by Sam Halliday</h1>
		<p>
			Test page for Golf Consistency Visualisations using an <code>XMLHttpRequest</code> communications link.
		</p>
		<h2>Score Visualisations</h2>		
		<h3>Par 3s</h3>
		<div id="par3" class="samChart"></div>
		<h3>Par 4s</h3>
		<div id="par4" class="samChart"></div>
		<h3>Par 5s</h3>
		<div id="par5" class="samChart"></div>
		<h3>Putting</h3>
		<div id="putts" class="samChart"></div>
		<h2>Tee Accuracy Visualisations</h2>
		<div id="tee" class="samChart"></div>
		<h2>Course Visualisation</h2>
		<h2><c:out value='${course}'/></h2>
		<div id="course" class="samChart"></div>
		<h2>Recent Performance Visualisation</h2>
		<div id="recent" class="samChart"></div>

		<script type="text/javascript">
			SBS_DATA_URL = "static.csv";
			// "csv?player=323608";
			// "/MyAccount/ExportResults";
			
			google.load('visualization', '1.0', {'packages':['corechart']});
			google.setOnLoadCallback(function(){
				getSbsData(function(data) {
					drawScoreVisualisationType(data, 3, 'par3');
					drawScoreVisualisationType(data, 4, 'par4');
					drawScoreVisualisationType(data, 5, 'par5');
					drawScoreVisualisationType(data, 0, 'putts');
					
					drawTeeVisualisation(data, "tee");
					
					// NOTE: you need to dynamically set the course name (here "Silverknowes")
					drawCourseVisualisation(data, "Silverknowes", "course");

					drawRecentPerformanceVisualisation(data, 'recent');
				})
			});
		</script>
	</body>
</html>
