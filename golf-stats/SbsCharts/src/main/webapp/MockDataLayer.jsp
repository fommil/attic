<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="/local_data"/>

<html>
	<head>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript" src="ConsistencyVisualisations.js"></script>
	</head>

	<body>
		<div id="mock_data"></div>
		<script type="text/javascript">
			var local_data = <c:out value='${data}' escapeXml='false' />;
		
			google.load("visualization", "1.0", {packages:["table"]});
			google.setOnLoadCallback(function(){
				getSbsData(function(data) {
					var table = new google.visualization.Table(document.getElementById('mock_data'));
					table.draw(data, {showRowNumber: true});
				})
			});
		</script>
	</body>
</html>
