/*
 * Created 03-May-2011
 * 
 * Copyright Samuel Halliday 2011
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package uk.me.fommil.sbsscraper;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Samuel Halliday
 */
public class GolfHole {

	public static GolfHole fromCsv(String line) throws ParseException {
		Preconditions.checkNotNull(line);
		String[] parts = line.split(",");
		Preconditions.checkArgument(parts.length == 10);

		GolfHole golf = new GolfHole();
		golf.setRound(Integer.valueOf(parts[0]));
		golf.setCourse(parts[1]);
		golf.setDate(formatter.parse(parts[2]));
		golf.setHole(Integer.valueOf(parts[3]));
		golf.setPar(Integer.valueOf(parts[4]));
		golf.setScore(Integer.valueOf(parts[5]));
		golf.setDistance(Integer.valueOf(parts[6]));
		golf.setHcp(Integer.valueOf(parts[7]));
		golf.setPutts(Integer.valueOf(parts[8]));
		golf.setDrive(Integer.valueOf(parts[9]));

		return golf;
	}

	public enum Drive {

		NONE, LEFT, RIGHT, STRAIGHT, CHUNK, TOP;

		static Drive parse(String number) {
			if (number.isEmpty() || number.equals("-"))
				return NONE;
			else if (number.matches(".*Left.*"))
				return LEFT;
			else if (number.matches(".*Right.*"))
				return RIGHT;
			else if (number.matches(".*Up.*"))
				return STRAIGHT;
			else if (number.matches(".*Top.*"))
				return TOP;
			else if (number.matches(".*Chunk.*"))
				return CHUNK;
			return null;
		}
	}

	private int round, hole, par, score, distance, hcp, putts, drive;

	private String course;

	private Date date;

	private static final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	public static String csvHeader() {
		return "round,course,date,hole,par,score,distance,hcp,putts,drive";
	}

	public String toCsv() {
		return Joiner.on(",").join(round, course, formatter.format(date), hole, par, score, distance, hcp, putts, drive);
	}

	// <editor-fold defaultstate="collapsed" desc="BOILERPLATE GETTERS/SETTERS">
	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public int getDrive() {
		return drive;
	}

	public void setDrive(int drive) {
		this.drive = drive;
	}

	public int getHcp() {
		return hcp;
	}

	public void setHcp(int hcp) {
		this.hcp = hcp;
	}

	public int getHole() {
		return hole;
	}

	public void setHole(int hole) {
		this.hole = hole;
	}

	public int getPar() {
		return par;
	}

	public void setPar(int par) {
		this.par = par;
	}

	public int getPutts() {
		return putts;
	}

	public void setPutts(int putts) {
		this.putts = putts;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	// </editor-fold>
}
