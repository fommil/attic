/*
 * Created 03-May-2011
 * 
 * Copyright Samuel Halliday 2011
 * PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */
package uk.me.fommil.sbsscraper;

import java.io.BufferedReader;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.Closeables;
import com.google.common.io.Files;
import java.io.FileReader;
import java.util.Properties;

/**
 * Log in to a <a href="http://swingbyswing.com">Swing by Swing</a>
 * account and scrape/parse the statistics.
 * <p>
 * This is a workaround until they implement
 * <a href="http://getsatisfaction.com/swingbyswing/topics/open_format_export_data">RFE "Open Format Export Data"</a>.
 * 
 * @author Samuel Halliday
 */
public class SbsWebParser {

	private static final Logger log = Logger.getLogger(SbsWebParser.class.getName());

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static final void main(String[] args) throws Exception {
		Properties props = new Properties();
		props.load(new FileReader("SbsWebParser.properties"));
		
		File file = new File("SbS-" + props.getProperty("username") + ".csv");
		Set<Integer> readRounds = Sets.newTreeSet();
		StringBuffer buffer = new StringBuffer();

		if (file.exists()) {
			BufferedReader reader = Files.newReader(file, Charsets.UTF_8);
			try {
				String line = reader.readLine(); // header
				while ((line = reader.readLine()) != null) {
					GolfHole hole = GolfHole.fromCsv(line);
					readRounds.add(hole.getRound());
				}
			} finally {
				Closeables.closeQuietly(reader);
			}
		} else {
			buffer.append(GolfHole.csvHeader());
			buffer.append("\n");
		}
		log.info("skipping " + readRounds);


		HttpClient httpclient = new DefaultHttpClient();
		try {
			HttpPost post = new HttpPost(LOGIN_URL);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("loginemail", props.getProperty("username")));
			nvps.add(new BasicNameValuePair("loginpassword", props.getProperty("password")));
			nvps.add(new BasicNameValuePair("keepsignedin", "true"));
			post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
			HttpResponse response = httpclient.execute(post);
			HttpEntity entity = response.getEntity();
			EntityUtils.consume(entity);

			HttpGet get = new HttpGet(TARGET_URL);
			response = httpclient.execute(get);
			String content = EntityUtils.toString(response.getEntity());
//			System.out.println(content);
			
			Matcher roundMatcher = ROUND_PATTERN.matcher(content);
			Set<Integer> rounds = Sets.newTreeSet();

			while (roundMatcher.find()) {
				String round = roundMatcher.group(1);
				rounds.add(Integer.valueOf(round));
			}
			log.info("can get " + rounds);
			rounds.removeAll(readRounds);

			List<GolfHole> history = Lists.newArrayList();
			for (Integer round : rounds) {
				get = new HttpGet(ROUND_BASE_URL + round);
				response = httpclient.execute(get);
				content = EntityUtils.toString(response.getEntity());
				List<GolfHole> holes = parseRound(round, content);
				history.addAll(holes);
			}

			for (GolfHole hole : history) {
				buffer.append(hole.toCsv());
				buffer.append("\n");
			}

			Files.append(buffer, file, Charsets.UTF_8);
		} finally {
			httpclient.getConnectionManager().shutdown();
		}
	}

	private static final String LOGIN_URL = "http://www.swingbyswing.com/myaccount/login";

	private static final String TARGET_URL = "http://www.swingbyswing.com/myrounds/rounds";

	private static final String ROUND_BASE_URL = "http://www.swingbyswing.com/myrounds/viewround/";

	private static final Pattern ROUND_PATTERN = Pattern.compile("<a href=\"/myrounds/viewround/(\\d++)\">");

	private static List<GolfHole> parseRound(int round, String content) throws ParseException {
		// http://getsatisfaction.com/swingbyswing/topics/even_more_statistics_gathering
		// TODO: pitches/chips
		// TODO: mishits
		// http://getsatisfaction.com/swingbyswing/topics/no_new_stats_in_my_round
		// TODO: penalties/bunkers

		Matcher nameMatcher = Pattern.compile("<title>\\s*(.*)\\|.*</title>", Pattern.MULTILINE | Pattern.DOTALL).matcher(content);
		nameMatcher.find();
		String name = nameMatcher.group(1);
		log.info(name);

		Matcher dateMatcher = Pattern.compile("<title>.*\\|(.*)\\s*</title>", Pattern.MULTILINE | Pattern.DOTALL).matcher(content);
		dateMatcher.find();
		Date date = new SimpleDateFormat("MMM dd yyyy").parse(dateMatcher.group(1));
		log.info(date.toString());

		List<Integer> distances = parseEighteen(content, "Men Tee", 1);
		assert distances.size() == 9 || distances.size() == 18;
		List<Integer> hcps = parseEighteen(content, "Men HCP", 0);
		assert hcps.size() == distances.size();
		List<Integer> pars = parseEighteen(content, "Men Par", 0);
		assert pars.size() == distances.size();
		List<Integer> scores = parseEighteen(content, "Me", 0);
		assert scores.size() == distances.size();
		List<Integer> putts = parseEighteen(content, ".*Putts", 0);
		assert putts.size() == distances.size();
		List<Integer> drives = parseEighteen(content, ".*Drive", 0);
		assert drives.size() == distances.size();

		log.info("ROUND = " + round);
		log.info("COURSE = " + name);
		log.info("DATE = " + date);
		log.info("DISTANCES = " + distances);
		log.info("PARS = " + pars);
		log.info("HCP = " + hcps);
		log.info("SCORE = " + scores);
		log.info("PUTTS = " + putts);
		log.info("DRIVE = " + drives);

		List<GolfHole> holes = Lists.newArrayList();

		for (int i = 0; i < distances.size(); i++) {
			if (scores.get(i) < 1)
				continue;
			GolfHole hole = new GolfHole();
			hole.setRound(round);
			hole.setCourse(name);
			hole.setDate(date);
			hole.setHole(i + 1);
			hole.setDistance(distances.get(i));
			hole.setHcp(hcps.get(i));
			hole.setPar(pars.get(i));
			hole.setScore(scores.get(i));
			hole.setPutts(putts.get(i));
			hole.setDrive(drives.get(i));

			holes.add(hole);
		}

		return holes;
	}

	private static List<Integer> parseEighteen(String content, String cue, int skip) {
		Pattern pattern = Pattern.compile("<td.*>\\s++" + cue + "\\s++</td>", Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(content);
		for (int i = 0; i < skip; i++) {
			matcher.find();
		}
		List<Integer> numbers = Lists.newArrayList();
		while (matcher.find()) {
			int frontNine = matcher.end();
			List<Integer> parsed = parseNineNumbers(content, frontNine);
			numbers.addAll(parsed);
		}
		return numbers;
	}

	private static List<Integer> parseNineNumbers(String content, int start) {
		Pattern pattern = Pattern.compile("<td.*>\\s+(.*)\\s+</td>", Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(content.substring(start));
		List<Integer> numbers = Lists.newArrayList();

		for (int i = 0; i < 9; i++) {
			matcher.find();
			String number = matcher.group(1);
			Integer num;
			try {
				num = Integer.valueOf(number);
			} catch (NumberFormatException e) {
				num = GolfHole.Drive.parse(number).ordinal();
			}
			numbers.add(num);
		}
		return numbers;
	}
}
