/**
 * Copyright (c) 2011 Samuel Halliday <Sam.Halliday@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

// public domain workaround from
// http://stackoverflow.com/questions/690251/what-happened-to-console-log-in-ie8
var alertFallback = true;
if (typeof console === "undefined" || typeof console.warn === "undefined") {
	console = {};
	if (alertFallback) {
		console.warn = function(msg) {
			alert(msg);
		};
	} else {
		console.warn = function() {};
	}
}

/**
 * The definition of the colours as used by the various visualisations.
 */
var COLOURS = ["#28809d", "#f6433b", "#5a5a5a", "#e1e11a", "#f1a32b", // score visualisations
"#CFF09E", "#A8DBA8", "#79BD9A", "#3B8686", "#0B486B", // tee accuracy
"#EFFFCD", "#DCE9BE", "#CBE86B", "#5E8C6A", "#88A65E", // tee accuracy missing
"#BFB35A", "#8FBE00", "#AEE239", "red" // course visualisation
];

var BACKGROUND = 'white';

/**
 * Static definitions the percentages of what 'good', 'bad', etc mean.
 */
var STATS_BEST = 0.05;
var STATS_GOOD = 0.20;
var STATS_BAD = 0.80;
var STATS_WORST = 0.95;

var TEE_TYPES = ['Straight', 'Left', 'Right', 'Top', 'Chunk'];

var SBS_DATA_URL, local_data;

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt , from) {
        var len = this.length;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
          this[from] === elt)
                return from;
        }
        return -1;
    };
}

/**
 * @param callback function(google.visualization.DataTable)
 */
function getSbsData(callback) {
    if (local_data != undefined) {
		parseSbSData(local_data, callback);
		return;
	}
	if (SBS_DATA_URL == undefined) {
		throw new "There was no local data or server URL for the data";
	}
	
	downloadUrl(SBS_DATA_URL, function(call) {
		return function(body, status) {
			parseSbSData(body, call)
		};
	}(callback));
}

/**
 * @param url string
 * @param callback function(string response, number status)
 */
function downloadUrl(url, callback) {
    var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP') : new XMLHttpRequest;
	request.onreadystatechange = function(callback, request) {
		return function() {
			if (request.readyState == 4) {
				callback(request.responseText, request.status);
			}
		}
	}(callback, request);
	request.open('GET', url, true);
	request.send(null);
}


/**
 * Synchronous function to retrieve data from the local filesystem.
 * Assumes a string variable called 'local_data' exists containing the data in CSV format,
 * as created by MockDataLayer.R.
 *
 * @param raw string unparsed string data in CSV format
 * @param callback function(google.visualization.DataTable)
 */

function parseSbSData(raw, callback) {
	var csv = CSVToArray(raw);

	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Course');
	data.addColumn('number', 'Hole');
	data.addColumn('number', 'HCP');
	data.addColumn('number', 'Par');
	data.addColumn('number', 'Score');
	data.addColumn('number', 'Putts');
	data.addColumn('number', 'Penalty');
	data.addColumn('number', 'Bunker');
	data.addColumn('number', 'Tee');
	data.addColumn('date', 'Date');
	data.addColumn('number', 'Round');


	var headers = csv[0];
	var expected = ["Player's Email", "Course Name", "Scorecard Id" , "iHole", "iHCP", "iPar", "iScore", "iPutts", "iPenaltyStrokes", "iBunkerHit", "iFairwayHit", "sDriveDirection", "dtInserted"];
	for (var i = 0 ; i < expected.length ; i++) {
	    headers[i] = $.trim(headers[i])
		if (headers[i] != expected[i]) {
			console.warn("header " + i + " was changed from '" + expected[i] + "' to '" + headers[i]+ "'");
		}
	}
	
	for (var i = 1 ; i < csv.length; i++) {
		var datum = csv[i];
		if (i == (csv.length - 1) && datum.length == 1)
			continue;
		if (datum.length != expected.length) {
			console.warn("row " + i + " was invalid: \"" + csv[i] + "\"");
			continue;
		}
		
		var row = []
		row.push($.trim(datum[1]));
		row.push(parseSbsInt(datum[3]));
		row.push(parseSbsInt(datum[4]));
		row.push(parseSbsInt(datum[5]));
		row.push(parseSbsInt(datum[6]));
		row.push(parseSbsInt(datum[7]));
		row.push(parseSbsInt(datum[8]));
		row.push(parseSbsInt(datum[9]));
		row.push(parseSbsTee(datum[11]));
		row.push(parseSbsDate(datum[12]));
		row.push(parseSbsInt(datum[2]));
		
		{
			// NOTE: this is data cleanliness which should really be done server side!
			if (row[2] == 0 || row[2] > 18) {
				// incorrectly labelled data
				//console.warn("removing bad HCP value of " + row[2] + " for " + row[0]);
				row[2] = null;
			}
			if (row[4] <= 0 || row[3] < 3 || row[3] > 5) {
				// nonsense data that should be removed
				//console.warn("removing bad data entry: " + datum);
				continue;
			}
		}
		
		data.addRow(row);
	}
	
	callback(data);
}

/**
 * @param s string containing an sDriveDirection
 * @return positive integer or null
 */
function parseSbsTee(s) {
	s = $.trim(s);
	var type = TEE_TYPES.indexOf(s, 0);
	if (type == -1) return null;
	return type;
}

/**
 * @param s string containing an integer
 * @return positive integer or null
 */
function parseSbsInt(s) {
	var i = parseInt($.trim(s), 10);
	if (i < 0) return null;
	return i;
}

/**
 * @param d string SbS style date in "M/d/yyyy h:mm:ss aa" format
 * @return Date object
 */
function parseSbsDate(d) {
    
	var parts = $.trim(d).split("/");
	var year = parseInt(parts[2].split(" ")[0], 10);
	var month = parseInt(parts[0], 10) - 1;
	var day = parseInt(parts[1], 10);
	return new Date(year, month, day);
}

/**
 * Public domain code from
 * http://www.bennadel.com/blog/1504-Ask-Ben-Parsing-CSV-Strings-With-Javascript-Exec-Regular-Expression-Command.htm
 * to parse a CSV string
 * 
 * @param strData string
 * @param strDelimiter string defaulting to ','
 */
function CSVToArray(strData, strDelimiter) {
	strDelimiter = (strDelimiter || ",");
	var objPattern = new RegExp((
		"(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
		"(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
		"([^\"\\" + strDelimiter + "\\r\\n]*))"
		), "gi");
	var arrData = [[]];
	var arrMatches = null;
	var strMatchedValue;
	while ((arrMatches = objPattern.exec(strData))) {
		var strMatchedDelimiter = arrMatches[1];
		if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
			arrData.push([]);
		}
		if (arrMatches[2]){
			strMatchedValue = arrMatches[2].replace(new RegExp( "\"\"", "g" ), "\""); 
		} else {
			strMatchedValue = arrMatches[3];
		}
		arrData[arrData.length - 1].push(strMatchedValue);
	}
	return( arrData );
}

/**
 * It is not possible to store the Score Visualisations options statically as the y axis range
 * must be customised.
 * 
 * The data and graph options employ several hacks to draw the interpolated areas, hide the tooltips
 * for interpolated values, show the date range in the tooltip and display everything in stacked
 * form. This graph is essentially pushing GViz to its limits.
 * 
 * @param max y value
 * @return object options for google.visualization.ComboChart
 */
function scoreOptions(max){
	return {
		vAxis: {
			title: 'Strokes',
			gridlineColor: '#FFFFFF',
			viewWindowMode: 'explicit',
			viewWindow: {
				min: 0,
				max: max
			}
		},
        chartArea:{left:60,top:20,width:"75%",height:"75%"},
		seriesType: 'area',
		isStacked: true,
		legend: {
			position: 'none'
		},
		areaOpacity: 1,
		series: [{
			color: BACKGROUND
		}, {
			color: COLOURS[2]
		}, {
			color: COLOURS[1]
		}, {
			color: COLOURS[2]
		}, {
			color: BACKGROUND,
			lineWidth: 0,
			visibleInLegend: false
		}, {
			color: COLOURS[4],
			lineWidth: 0,
			visibleInLegend: false
		}, {
			color: COLOURS[3],
			lineWidth: 0,
			visibleInLegend: false
		}, {
			color: COLOURS[4],
			lineWidth: 0,
			visibleInLegend: false
		}, {
			type: 'line',
			color: COLOURS[0],
			lineWidth: 5
		}]
	};
}

/**
 * Draw a timeseries confidence visualisation of the given data for a predefined type of data.
 *
 * @param data google.visualization.DataTable
 * @param type 0 for putts, 3, 4 or 5 for par3, par4 or pa5 respectively
 * @param chart_id string element id of where to draw the chart
 */
function drawScoreVisualisationType(data, type, chart_id) {
	var condition;
	var column;
	switch (type) {
		case 0:
			column = 5;
			condition = {
				column: 5,
				minValue: 1 // NOTE: this excludes chip-ins
			};
			break;
		case 3:
		case 4:
		case 5:
			column = 4;
			condition = {
				column: 3,
				value: type
			};
			break;
		default:
			console.warn("Unrecognised type " + type);
			return;
	}
	drawScoreVisualisation(data, chart_id, condition, column);
}

/**
 * Draw a timeseries confidence visualisation of the given data.
 *
 * @param data google.visualization.DataTable
 * @param chart_id string element id of where to draw the chart
 * @param condition to apply to the data
 * @param column to extract from the data
 */
function drawScoreVisualisation(data, chart_id, condition, column) {
	var trends = new google.visualization.DataTable();
	trends.addColumn('date', 'Date');
	trends.addColumn('number', 'Best');
	trends.addColumn('number', 'Good');
	trends.addColumn('number', 'Bad');
	trends.addColumn('number', 'Worst');
	trends.addColumn('number', 'Average');
	
	var minStrokes = 2;

	timeSeriesFilter(data, condition, column, function(date, low, high, scores){
		var row;
		if (scores.length > minStrokes) {	
			row = [
			date,
			quantileSam(scores, STATS_BEST),
			quantileSam(scores, STATS_GOOD),
			quantileSam(scores, STATS_BAD),
			quantileSam(scores, STATS_WORST),
			mean(scores)
			];
		} else {
			row = [date, null, null, null, null, null];
		}
		trends.addRow(row);
	});
	
	var el = document.getElementById(chart_id);
	if (!enoughData(trends)) {
		disableChart(el);
		return;
	}

	var worsts = trends.getDistinctValues(4);
	var max = Math.max(3, Math.min(15, worsts[worsts.length - 1]));
	var options = scoreOptions(max);

	var stacked = toStackView(trends, 0, [1, 2, 3, 4]);
	var interpolated = interpolateMissing(trends, 0, [1, 2, 3, 4]); 
	var stackedInterpolated = toStackView(interpolated, 0, [1, 2, 3, 4]);
	
	var combined = combineTables([{
		data: stacked,
		format: function(trends) {
			return function(data, row, col){
				return describeStatistic(trends.getValue(row, col));
			}
		}(trends)
	}, {
		data: stackedInterpolated
	}, {
		data: trends,
		columns: [5],
		format: function(data, row, col){
			return data.getValue(row, col).toFixed(2);
		}
	}]);

	var viz = new google.visualization.ComboChart(el);
	viz.draw(combined, options);
}

/**
 * @param data google.visualization.DataTable
 * @param condition to meet when filtering the data
 * @param column to extract from the data
 * @param action function(Date date, Date low, Date high, Array) called in every time step
 */
function timeSeriesFilter(data, condition, column, action) {
	var allDates = data.getDistinctValues(9);
	var start = allDates[0];
	var end = allDates[allDates.length - 1];
	var days = (end - start) / (1000 * 60 * 60 * 24);
	var period = Math.max(24, Math.min(48, days / 50));
	var resolution = period / 2;
	var date = new Date(start);
	
	while (date <= end) {		
		var low = new Date(date);
		low.setDate(date.getDate() - period / 2);
		var high = new Date(date);
		high.setDate(date.getDate() + period / 2);
		
		var filtered = timeSeriesFilter_(data, low, high, condition, column);
		
		action(new Date(date), low, high, filtered);
		date.setDate(date.getDate() + resolution);
	}
}

/**
 * Internal function for filtering rows from the raw data.
 * 
 * @param data google.visualization.DataTable
 * @param low date
 * @param high date
 * @param condition string
 * @param column number
 * @returns Array
 */
function timeSeriesFilter_(data, low, high, condition, column) {
	var conditions = [];
	var dateRestriction = {
		column: 9,
		minValue: low,
		maxValue: high
	};
	conditions.push(dateRestriction)
	if (condition != null) {
		conditions.push(condition);
	}	
	var filteredIndices = data.getFilteredRows(conditions);
	var scores = [];
	while (filteredIndices.length > 0) {
		var index = filteredIndices.pop();
		var score = data.getValue(index, column);
		if (score != null)
			scores.push(score);
	}
	return scores;
}

/**
 * @param tables Array containing objects with entries
 *               {data: google.visualization.DataTable, domain: index (optional, default 0),
 *               format: function(data, row, col) (optional), columns: Array indices (optional, default all)}
 *               All the tables must have the same domain values for all row indices.
 *               Note that the format function is a workaround because DataViews can't be created with custom
 *               FormattedValues and therefore we have to do all this at the end when tables are combined.
 * @returns google.visualization.DataTable combining the given tables.
 */
function combineTables(tables) {
	var combined = new google.visualization.DataTable();
	var data = tables[0].data;
	var domain = tables[0].domain != null ? tables[0].domain : 0;
	var rows = data.getNumberOfRows();
	combined.addColumn(data.getColumnType(domain), data.getColumnLabel(domain));
	combined.addRows(rows);
	for (var i = 0 ; i < rows ; i++) {
		combined.setValue(i, 0, data.getValue(i, domain));
		combined.setFormattedValue(i, 0, data.getFormattedValue(i, domain));
	}
	
	var k = 0;
	for (var i = 0 ; i < tables.length ; i++) {
		data = tables[i].data;
		domain = domain = tables[i].domain != null ? tables[i].domain : 0;
		var format = tables[i].format;
		var columns = tables[i].columns;
		if (columns == null) {
			columns = [];
			for (var col = 0 ; col < data.getNumberOfColumns() ; col++) {
				if (col == domain) continue;
				columns.push(col)
			}
		}

		for (var j = 0 ; j < columns.length ; j++) {
			var col = columns[j];
			combined.addColumn(data.getColumnType(col), data.getColumnLabel(col));
			k++;
			for (var row = 0 ; row < rows ; row++) {
				var val = data.getValue(row, col);
				if (val != null) {
					combined.setValue(row, k, val);
					if (format != null)
						combined.setFormattedValue(row, k, format(data, row, col));
				}
			}
		}
	}
	
	return combined;
}

/**
 * Ridiculous that this needs to be defined
 */
var MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

/**
 * @param low Date
 * @param high Date
 * @returns string representing the date range
 */
function betweenDates(low, high) {
	var d1 = [low.getDate(), MONTHS[low.getMonth()], low.getFullYear()];
	var d2 = [high.getDate(), MONTHS[high.getMonth()], high.getFullYear()];
	
	return d1[0] + "<sup>" + stndrdth(d1[0]) + "</sup>"
	+ (d1[1] != d2[1] ? " " + d1[1].substring(0,3) : "")
	+ (d1[2] != d2[2] ? " " + d1[2] : "")
	+ " to "
	+ d2[0] + "<sup>" + stndrdth(d2[0]) + "</sup> " + d2[1].substring(0,3) + " " + d2[2];
}

/**
 * @param n number
 * @returns string "st", "nd", "rd" or "th" to prepend for ordinals
 */
function stndrdth(n) {
	if (n > 10 && n < 14)
		return "th";
	var dec = n % 10;
	switch (dec) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
	}
}
	
/**
* Converts monotonically increasing columns of a DataTable into values suitable for showing in
* stacked form.
*
* @param data google.visualization.DataTable
* @param domain integer index of the domain
* @param toStack Array data to combine in the stack
* @returns google.visualization.DataView
*/
function toStackView(data, domain, toStack) {
	var i, col;
	
	if (toStack.length < 2)
		return null;
	var columns = [];
	columns.push(domain);
	for (i = 0 ; i < toStack.length ; i++) {
		col = {
			type: 'number', 
			label: data.getColumnLabel(toStack[i]), 
			calc: (function(col, colLast) {
				return function(d, row) {
					var val = d.getValue(row, col);
					if (val == null)
						return null;
					if (colLast > 0) {
						var toSubtract = d.getValue(row, colLast);
						if (toSubtract != null) {
							return (val - toSubtract);
						}
							
					}
					return val;
				}
			}(toStack[i], i > 0 ? toStack[i - 1] : -1))
		}
		columns.push(col);
	}
	
	var view = new google.visualization.DataView(data);
	view.setColumns(columns);
		
	return view;		
}

/**
 * Creates a new DataTable containing interpolated values for missing data.
 *
 * @param data google.visualization.DataTable
 * @param domain integer index of the domain
 * @param toInterpolate Array of integers
 * @returns google.visualization.DataTable
 */
function interpolateMissing(data, domain, toInterpolate) {
	var rows = data.getNumberOfRows();
	
	var interpolated = new google.visualization.DataTable();
	interpolated.addColumn(data.getColumnType(domain), data.getColumnLabel(domain));
	for (var i = 0 ; i < toInterpolate.length ; i++) {
		var c = toInterpolate[i];
		interpolated.addColumn(data.getColumnType(c), data.getColumnLabel(c) + " (missing)");
	}
	interpolated.addRows(rows);
	for (var i = 0 ; i < rows ; i++) {
		interpolated.setValue(i, 0, data.getValue(i, domain));
		interpolated.setFormattedValue(i, 0, data.getFormattedValue(i, domain));
	}
	
	for (var j = 1 ; j <= toInterpolate.length ; j++) {
		var c = toInterpolate[j - 1];
		var start = null;
		
		for (var i = 0 ; i < rows ; i++) {
			var value = data.getValue(i, c);
			if (start == null && value == null) {
				if (i == 0)
					start = 0;
				else
					start = i - 1;
			}
			if (start != null && i == (rows - 1) && value == null) {
				value = data.getValue(start, c);
			}
			if (start != null && value != null) {
				var entries = linearInterpolationFill(
					data.getValue(start, c), value, i - start + 1);
				for (var k = 0 ; k < entries.length ; k++) {
					interpolated.setValue(start + k, j, entries[k]);
				}

				start = null;
			}
		}
	}	
	
	return interpolated;
}

/**
 * Creates an array containing the linearly interpolated values between two points.
 * 
 * @param start number (can be null)
 * @param end number
 * @param steps number of points to interpolate (must be more than 2)
 * @returns google.visualization.DataTable
 */
function linearInterpolationFill(start, end, steps) {
	var interpolated = [];
	var x0 = 0;
	var x1 = steps - 1;
	var y0 = start;
	var y1 = end;
	if (y0 == null) y0 = y1;
	var m = (y1 - y0) / (x1 - x0)
	var c = y0 - m * x0
	for (var i = 0; i < steps; i++) {
		interpolated[i] = m * i + c;
	}
	return interpolated;
}


/**
 * Internal method to provide a simple, rough, description of a number instead of the decimal value.
 *
 * @param number
 */
function describeStatistic(number) {
	var floor = Math.floor(number);
	var ceil = Math.ceil(number);
	if (floor == ceil) {
		return "" + floor;
	}
	return "a mixture of " + floor + "s and " + ceil + "s";
}

/**
 * Internal method to provide a simple, rough, description of a number instead of the decimal value.
 *
 * @param number
 */
function describeStatistic2(number) {
	var floor = Math.floor(number);
	var ceil = Math.ceil(number);
	if (floor == ceil) {
		return "" + floor;
	}
	return floor + " or " + ceil;
}


/**
 * @param data google.visualization.DataTable
 */
function enoughData(data) {
	return (
		data.getDistinctValues(0).length > 1 &&
		(data.getDistinctValues(2).length > 1 || data.getDistinctValues(2)[0] != null) &&
		data.getNumberOfRows() > 1);
}

/**
 * @param el Element which intended to hold the chart
 */
function disableChart(el) {
	el.className += " notEnoughData";
	el.innerHTML = "<p>Sorry, you have not recorded enough data to see this chart</p>"
}

var CANDLE_OPTIONS = {
	vAxis: {
		title: "Strokes",
		minValue: 0,
		gridlineColor: '#FFFFFF'
	},
	hAxis: {
		title: "Hole"
	},
    chartArea: { left: 60, top: 20, width: "75%", height: "75%" },
	colors: [ //COLOURS[15],
	COLOURS[16], COLOURS[17], COLOURS[18], COLOURS[19]]
}

/**
 * Draw the tee accuracy visualisation.
 * 
 * @param data google.visualization.DataTable
 * @param course string course to extract from the data
 * @param chart_id string element id of where to draw the chart
 */
function drawCourseVisualisation(data, course, chart_id) {
	var candles = new google.visualization.DataTable();
	candles.addColumn('string', data.getColumnLabel(0));
	
	var types = [{
		name: 'Score', 
		column: 4,
		tooltip: function(par, average) {
			var diff = (average - par);
			return " (" + Math.abs(diff).toFixed(1) + " " + (diff < 0 ? "below" : "above") + " par)"
		}
	}, {
		name: 'Putting',
		column: 5
	}, {
		name: 'Penalty',
		column: 6
	}];
	
	for (var i = 0 ; i < types.length ; i++) {
		candles.addColumn('number', types[i].name);
		candles.addColumn('number');
		candles.addColumn('number');
		candles.addColumn('number');
		candles.addColumn({
			type: 'string', 
			role: 'tooltip'
		});
	}
	
	var holes = data.getDistinctValues(1);
	for (var k = 0 ; k < holes.length ; k++) {
		var hole = holes[k];
		var rows = data.getFilteredRows([{
			column: 0,
			value: course
		}, {
			column: 1,
			value: hole
		}]);
		if (rows.length == 0)
			continue;
	
		var row = [];
		row.push(hole.toString());
		for (var i = 0 ; i < types.length ; i++) {
			var samples = []
			for (var j = 0 ; j < rows.length ; j++) {
				var r = rows[j];
				var val = data.getValue(r, types[i].column);
				if (val != null && val >= 0)
					samples.push(val);
			}
			var entries = [];
			if (types[i].action != null) {
				entries = types[i].action(samples);
			} else {
				var average = mean(samples);
				entries[0] = quantileSam(samples, STATS_BEST);
				entries[1] = quantileSam(samples, STATS_GOOD);
				entries[2] = quantileSam(samples, STATS_BAD);
				entries[3] = quantileSam(samples, STATS_WORST);
				// ?? horrible formatting, it is not HTML and requires manual newlines
				entries[4] = "Your " + types[i].name + " on the " + hole + stndrdth(hole)
				+ " hole " +
				((entries[2] - entries[1]) == 0 ? "does not vary" : "varies by up to " + Math.ceil(entries[2] - entries[1]) + " strokes")
				+ " most of the time.\n"
				+ "Furthermore, your " + types[i].name + ((entries[3] - entries[0]) == 0 ? " does not vary" : " varies by up to " + Math.ceil(entries[3] - entries[0]) + " strokes")
				+ " between your Best and Worst.\nYour average is "
				+ average.toFixed(1) + ".";
				if (types[i].tooltip != null)
					entries[4] += types[i].tooltip(data.getValue(r, 3), average);
			}
			for (var e = 0 ; e < entries.length; e++) {
				row.push(entries[e]);
			}
		}
		candles.addRow(row)
	}
	
	var el = document.getElementById(chart_id);
	if (!enoughData(candles)) {
		disableChart(el);
		return;
	}
	
	var chart = new google.visualization.CandlestickChart(el);
	chart.draw(candles, CANDLE_OPTIONS);
}

var RECENT_OPTIONS = {
	vAxis: {
		title: "Strokes",
		gridlineColor: '#FFFFFF'
	},
    chartArea: { left: 90, top: 20, width: "70%", height: "75%" },
	colors: [COLOURS[16], COLOURS[17]]
}

/**
 * Draw the recent performance visualisation.
 * 
 * @param data google.visualization.DataTable
 * @param chart_id string element id of where to draw the chart
 */
function drawRecentPerformanceVisualisation(data, chart_id) {
	var numberOfRounds = 10;

	var rounds = data.getDistinctValues(10);
	// ??: assumes round IDs ascend with time
	var latest = rounds.slice(rounds.length - numberOfRounds);

	var recent = new google.visualization.DataTable();
	recent.addColumn('string', "Recent Round");
	
	var types = [{
		name: 'Strokes Against Par', 
		column: 4,
		sample: function(par, score) {
			return score - par;
		}
	}, {
		name: 'Putts',
		column: 5
	}];

	for (var i = 0 ; i < types.length ; i++) {
		recent.addColumn('number', types[i].name);
		recent.addColumn('number');
		recent.addColumn('number');
		recent.addColumn('number');
		recent.addColumn({
			type: 'string', 
			role: 'tooltip'
		});
	}
	
	for (var i = 0 ; i < latest.length ; i++) {
		var round = latest[i];
		var holes = data.getFilteredRows([{
			column: 10, 
			value: round
		}]);
	
		if (holes.length == 0)
			continue;
		
		var row = [];
		var course = null;
		for (var k = 0 ; k < types.length ; k++) {
			var type = types[k];
			var samples = [];
			for (var j = 0 ; j < holes.length ; j++) {
				if (course == null)
					course = data.getValue(holes[j], 0);
				var sample = data.getValue(holes[j], type.column);
				if (type.sample != null)
					sample = type.sample(data.getValue(holes[j], 3), sample)
				samples.push(sample);
			}
			if (row.length == 0)
				row.push(course);
			
			var average = mean(samples);
			var entries = []
			entries[0] = quantileSam(samples, STATS_BEST);
			entries[1] = quantileSam(samples, STATS_GOOD);
			entries[2] = quantileSam(samples, STATS_BAD);
			entries[3] = quantileSam(samples, STATS_WORST);
			// ?? horrible formatting, it is not HTML and requires manual newlines
			entries[4] = "At " + course + ", your " + type.name + " were mostly "
			+ ((entries[2] - entries[1]) == 0 ? "consistent at " + entries[1] : "between " + Math.floor(entries[1]) + " and " + Math.ceil(entries[2])) + ".\n"
			+ "On your Best and Worst holes, your " + type.name + " were "
			+ ((entries[3] - entries[0]) == 0 ? "consistent at " + entries[0] : "between " + Math.floor(entries[0]) + " and " + Math.ceil(entries[3])) + ".\n"
			+ "Your average was "
			+ average.toFixed(1) + ".";
			for (var e = 0 ; e < entries.length; e++) {
				row.push(entries[e]);
			}
		}
		recent.addRow(row);
	}

	var el = document.getElementById(chart_id);
	//	if (!enoughData(recent)) {
	//		disableChart(el);
	//		return;
	//	}
	
	var chart = new google.visualization.CandlestickChart(el);
	chart.draw(recent, RECENT_OPTIONS);
}

/**
 * The data and graph options employ several hacks to draw the interpolated areas, hide the tooltips
 * for interpolated values, show the date range in the tooltip, display everything in stacked
 * form and provide a custom piechart tooltip. This graph is essentially pushing GViz to its limits.
 */
var TEE_OPTIONS = {
	tooltip: {
		trigger: 'none'
	},
	vAxis: {
		title: 'Percentage of Tee Shots',
		gridlineColor: '#FFFFFF',
		viewWindowMode: 'explicit',
		viewWindow: {
			min: 0,
			max: 100
		}
	},
    chartArea: { left: 60, top: 20, width: "75%", height: "75%" },
	seriesType: 'area',
	isStacked: true,
	legend: {
		position: 'right'
	},
	areaOpacity: 1,
	//	focusTarget: 'category',
	series: [{
		color: COLOURS[5]
	}, {
		color: COLOURS[6]
	}, {
		color: COLOURS[7]
	}, {
		color: COLOURS[8]
	}, {
		color: COLOURS[9]
	}, {
		color: COLOURS[10],
		lineWidth: 0,
		visibleInLegend: false
	}, {
		color: COLOURS[11],
		lineWidth: 0,
		visibleInLegend: false
	}, {
		color: COLOURS[12],
		lineWidth: 0,
		visibleInLegend: false
	}, {
		color: COLOURS[13],
		lineWidth: 0,
		visibleInLegend: false
	}, {
		color: COLOURS[14],
		lineWidth: 0,
		visibleInLegend: false
	}]
};

var PIE_OPTIONS = {
	colors: [COLOURS[9], COLOURS[8], COLOURS[7], COLOURS[6], COLOURS[5]], // reversed to match legend
	legend: {
		position: 'left'
	},
    chartArea: { left: 60, top: 20, width: "75%%", height: "75%" },
	is3D: true,
	reverseCategories: true
};

/**
 * Draw the tee accuracy visualisation.
 * 
 * @param data google.visualization.DataTable
 * @param chart_id string element id of where to draw the chart
 */
function drawTeeVisualisation(data, chart_id) {
	var trends = new google.visualization.DataTable();
	trends.addColumn('date', 'Date');
	trends.addColumn('string', 'Date Range'); // for custom tooltips
	trends.addColumn('number', TEE_TYPES[0]);
	trends.addColumn('number', TEE_TYPES[1]);
	trends.addColumn('number', TEE_TYPES[2]);
	trends.addColumn('number', TEE_TYPES[3]);
	trends.addColumn('number', TEE_TYPES[4]);
	
	var minStrokes = 5;
	
	timeSeriesFilter(data, null, 8, function(date, low, high, accuracies) {
		var row;
		if (accuracies.length > minStrokes) {
			var f = frequencies(accuracies);
			for (var i = 0 ; i < TEE_TYPES.length ; i++) {
				if (f[i] == null) f[i] = 0;
				f[i] = 100 * f[i];
			}
			row = [date, betweenDates(low, high), f[0], f[1], f[2], f[3], f[4]];
		} else {
			row = [date, betweenDates(low, high), null, null, null, null, null];
		}
		trends.addRow(row);
	});
	
	var el = document.getElementById(chart_id);
	if (!enoughData(trends)) {
		disableChart(el);
		return;
	}
	
	var interpolated = interpolateMissing(trends, 0, [2, 3, 4, 5, 6]); 
	
	var combined = combineTables([{
		data: trends,
		columns: [2, 3, 4, 5, 6]
	}, {
		data: interpolated
	}]);

	var viz = new google.visualization.ComboChart(el);
	viz.draw(combined, TEE_OPTIONS);

	addTeeVisualisationTooltip(trends, el, viz);
}

/**
 * Sets up the custom tooltip to go along with the tee accuracy visualisation.
 * 
 * @param trends
 * @param viz_el
 * @param viz
 */
function addTeeVisualisationTooltip(trends, viz_el, viz) {
	var pieData = new google.visualization.DataTable();
	pieData.addColumn('string', "Tee Shot");
	pieData.addColumn('number', "Percentage");
	pieData.addRow([TEE_TYPES[0], 0]);
	pieData.addRow([TEE_TYPES[1], 0]);
	pieData.addRow([TEE_TYPES[2], 0]);
	pieData.addRow([TEE_TYPES[3], 0]);
	pieData.addRow([TEE_TYPES[4], 0]);

	var pie_el = document.createElement('div');
	pie_el.style.width = Math.max(250, Math.ceil(viz_el.offsetWidth / 3)) + "px";
	pie_el.style.height = Math.max(200, Math.ceil(viz_el.offsetHeight / 2)) + "px";
	var pie = new google.visualization.PieChart(pie_el);

	var tooltip_title_el = document.createElement('div');
	tooltip_title_el.style.fontFamily = "Arial";
	tooltip_title_el.style.textAlign = "center";
	var tooltip_el = addCustomTooltip(viz_el, viz,
		function(data, pieData, pie, el){
			return function(row) {
				el.innerHTML = data.getValue(row, 1);
				for (var i = 0 ; i < TEE_TYPES.length; i++) {
					var v = data.getValue(row, i + 2);
					if (v == null)
						return false;
			
					pieData.setValue(i, 1, v);
					pieData.setFormattedValue(i, 1, "")
				}
		
				pie.draw(pieData, PIE_OPTIONS);
				return true;
			}
    } (trends, pieData, pie, tooltip_title_el));

    if (tooltip_el == null) {
        return;
    }

	tooltip_el.appendChild(tooltip_title_el);
	tooltip_el.appendChild(pie_el);
}

/**
 * Workaround for inadequencies in the Google APIs. Could probably be written more
 * elegantly by defining an object type, but this does the job.
 * 
 * PS: I discovered a 3rd party tool: http://code.google.com/p/gvtooltip/
 *
 * @param viz_el Element of the Google Visualisation, will be made 'relative'
 * @param viz google.visualization object
 * @param callback function(event)
 * @returns Element that will contain the tooltip
 */
function addCustomTooltip(viz_el, viz, callback) {
	viz_el.style.position = "relative";
	var tooltip = document.createElement('div');
	tooltip.style.background = "white";
	tooltip.style.visibility = "hidden";
	tooltip.style.position = "absolute";
	tooltip.style.border = "2px solid #cccccc";
	tooltip.style.padding = "0px";
	tooltip.style.margin = "0px";
	//	tooltip.style.maxHeight = viz_el.offsetHeight + "px";
	//	tooltip.style.maxWidth = viz_el.offsetWidth + "px";
	viz_el.appendChild(tooltip);
	var close = document.createElement('div');
	close.innerHTML = "[x]";
	close.style.visibility = "hidden";
	close.style.textAlign = "right";
	close.style.fontFamily = "Arial";
	close.style.color = "#cccccc";
	close.onclick = function(e) {
		close.style.visibility = "hidden";
		tooltip.style.visibility = "hidden";
		viz.setSelection([]);
	}
	tooltip.appendChild(close);

	var iframe = viz_el.getElementsByTagName('iframe');
	if (iframe.length != 1) {
		//console.warn("The Google Visualisation API has changed!");
		return null;
	}
	var mouse = {
		x:null, 
		y:null
	};
				
	var show = function(e) {
		var row;
		if (e == null) {
			var selection = viz.getSelection()[0];
			if (selection == null || selection.row == null) {
				close.style.visibility = "hidden";
				tooltip.style.visibility = "hidden";
				return;
			}
			row = selection.row;
		} else {
			row = e.row;
		}
		
		if (row == null || !callback(row)) {
			close.style.visibility = "hidden";
			tooltip.style.visibility = "hidden";
			return;
		}
		
		var cWidth = viz_el.offsetWidth;
		var cHeight = viz_el.offsetHeight;
		var width = tooltip.offsetWidth;
		var height = tooltip.offsetHeight;
		var x = mouse.x;
		
		var left = (x > cWidth / 2 ? x - width - 50 : x + 50);
		var right = (Math.ceil(cHeight/2 - height/2 - 25));
		if ((left + width) > x)
			left = x + 50;
		
		tooltip.style.left = left + "px";
		tooltip.style.top = right + "px";
		tooltip.style.visibility = "visible";
	};
	
	google.visualization.events.addListener(viz, 'onmouseover', function(viz){
		return function(e) {
			if (viz.getSelection().length > 0)
				return;
			show(e);
		}
	}(viz));
	
	google.visualization.events.addListener(viz, 'onmouseout', function(viz, close, tooltip){
		return function(e) {
			if(viz.getSelection().length > 0)
				return;

			close.style.visibility = "hidden";
			tooltip.style.visibility = "hidden";
		}
	}(viz, close, tooltip));
	
	google.visualization.events.addListener(viz, 'select', function(close) {
		return function(e) {
			close.style.visibility = "visible";
			show(e);
		}
	}(close));

	iframe[0].contentWindow.onmousemove = function(mouse) {
		return function(e) {
			mouse.x = e.clientX;
			mouse.y = e.clientY;
		}
	}(mouse);
	
	tooltip.onmouseover = function(close) {
		return function(e) {
			close.style.visibility = "visible";
		}
	}(close)

	return tooltip;
}

/**
 * Calculates the mean value of a collection of numbers - amazing that this isn't in Math.*.
 *
 * @param values array of numbers
 * @returns number mean of the arrays
 */
function mean(values) {
	var sum = 0;
	for (var i = 0; i < values.length; i++) {
		sum += values[i];
	}
	return sum / values.length;
}

/**
 * Use the <a href="http://en.wikipedia.org/wiki/Quantile">R-5 quantile estimation algorithm</a>.
 *
 * @param values array of numbers
 * @param p number [0, 1]
 * @returns number an estimate of the quantile
 */
function quantileR5(values, p) {
	if (p < 0 || p > 1 || values.length == 0) {
		return null;
	}
	var x = values.sort(function(a, b) {
		return a - b
	});
	if (p < 0.5 / x.length) {
		return x[0];
	} else if (p >= (1 - 0.5 / x.length)) {
		return x[x.length - 1];
	}
	var h = x.length * p + 0.5 - 1;
	// -1 to correct for zero indexing
	var hFloor = Math.floor(h);

	return x[hFloor] + (h - hFloor) * (x[hFloor + 1] - x[hFloor]);
}

/**
 * Use the <a href="http://en.wikipedia.org/wiki/Quantile">R-7 quantile estimation algorithm</a>.
 *
 * @param values array of numbers
 * @param p number [0, 1]
 * @returns number an estimate of the quantile
 */
function quantileR7(values, p) {
	if (p < 0 || p > 1 || values.length == 0) {
		return null;
	}
	var x = values.sort(function(a, b) {
		return a - b
	});
	if (p == 1) {
		return x[x.length - 1];
	}
	var h = (x.length - 1) * p + 1 - 1;
	// -1 to correct for zero indexing
	var hFloor = Math.floor(h);

	return x[hFloor] + (h - hFloor) * (x[hFloor + 1] - x[hFloor]);
}

/**
 * Uses a quartile estimator designed for small integer bins by interpolating linearly in the CDF.
 *
 * @param values array of integers
 * @param p number [0, 1]
 * @returns number an estimate of the quantile
 */
function quantileSam(values, p) {
	if (p < 0 || p > 1 || values.length == 0) {
		return null;
	}
	var f = frequencies(values);
	var c = cdf(f);
	
	var x0 = 0;
	var x1 = c.length;
	for (var i = 0 ; i < c.length; i++) {
		if (i > x0 && p >= c[i]) x0 = i;
		if (i < x1 && p <= c[i]) x1 = i;
	}
	
	var y0 = c[x0];
	var y1 = c[x1];
	if (y0 == 0.0) return x1; // this is necessary to avoid unachieved scores
	if (x0 == x1 || y0 == y1) return (x0 + x1) / 2; // avoid divide by zeroes
	var m = (y1 - y0) / (x1 - x0);
	var cy = y0 - m * x0;
	var xp = (p - cy) / m
	return xp;
}

/**
 * @param values Array of integer numbers
 * @returns Array with frequencies as values (values are the indices) padded from zero
 */
function frequencies(values) {
	var bin = new Array();
	for (var i = 0 ; i < values.length ; i++) {
		var v = values[i];
		if (v == null) continue;
		if (bin[v] == null) bin[v] = 0;
		bin[v] += 1;
	}
	var total = 0;
	for (var i = 0 ; i < bin.length ; i++) {
		if (bin[i] == null) bin[i] = 0;
		total += bin[i];
	}
	var frequencies = new Array()
	for (var i = 0 ; i < bin.length ; i++) {
		frequencies[i] = bin[i] / total;
	}
	return frequencies;
}

/**
 * @param f Array from frequencies
 * @returns Array with cdf entries for integer x, padded from zero
 */
function cdf(f) {
	var cdf = new Array();
	
	cdf[0] = f[0];
	for (var i = 1 ; i < f.length ; i++) {
		cdf[i] = cdf[i - 1] + f[i];
	}
	
	if (cdf[cdf.length - 1] != 1.0)
		cdf[cdf.length] = 1.0;
	
	return cdf;
}
