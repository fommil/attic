console.log("Running fommil's Autocomplete Extension")

var allInput = document.getElementsByTagName("input");
for (var i = 0; i < allInput.length; i++) {
    var input = allInput[i]
    if (input.getAttribute("type") === "text" || input.getAttribute("type") === "password") {
        if (input.hasAttribute("autocomplete")) {
            input.removeAttribute("autocomplete")
        }
    }
}
