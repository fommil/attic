[![Build Status](https://travis-ci.org/fommil/java-allocation-instrumenter.svg?branch=master)](https://travis-ci.org/fommil/java-allocation-instrumenter)
[![Coverage Status](https://coveralls.io/repos/fommil/java-allocation-instrumenter/badge.svg?branch=master)](https://coveralls.io/r/fommil/java-allocation-instrumenter?branch=master)

java-allocation-instrumenter
============================

Fork of Google's allocation-instrumenter to better support Scala. I've also introduced some API changes and dropped the Guava dependency.


```xml
<dependency>
  <groupId>com.github.fommil</groupId>
  <artifactId>java-allocation-instrumenter</artifactId>
  <version>3.0</version>
</dependency>
```

