# multiblas

Optimal runtime selection of CPU / GPU BLAS (with an [obligatory reference to the 5th Element](http://www.imdb.com/title/tt0119116/quotes?item=qt0464059)).

## Need


[BLAS](http://www.netlib.org/blas/) (1979) is a set of basic linear algebra operations such as vector dot product and matrix multiplication. BLAS is used extensively in high-performance computing with machine optimised implementations available for specific CPU chipsets (e.g. [Intel](http://en.wikipedia.org/wiki/Math_Kernel_Library), [AMD](http://en.wikipedia.org/wiki/AMD_Core_Math_Library), [Apple](https://developer.apple.com/documentation/Performance/Conceptual/vecLib/), [ATLAS](http://sourceforge.net/projects/math-atlas/) and [OpenBLAS](https://github.com/xianyi/OpenBLAS)).

More recently, BLAS has been implemented for GPUs by [NVIDIA](https://developer.nvidia.com/cublas)
and [AMD](https://github.com/clMathLibraries/clBLAS) (the latter using the [OpenCL standard](http://en.wikipedia.org/wiki/OpenCL)).
Such implementations result in **dramatic** speedups for larger arrays, but are much slower
for small to medium sized arrays (less than ~100,000 entries).

Clearly, BLAS is best implemented by machine optimised implementations (i.e. the CPU) for small to medium arrays,
and by the GPU for larger arrays.

## Approach

MultiBLAS is a proof-of-concept that delegates between two implementations of BLAS using `dlopen/dlsym` on UNIX systems
and `LoadLibrary/GetProcAddress` on Windows systems.

The [First Milestone](https://github.com/fommil/multiblas/issues?milestone=1) of the project is to implement delegation,
machine-specific performance tuning, and demonstrate performance results for `DDOT` and `DGEMM` on OS X, Linux and Windows.

It is very important that the BLAS and CBLAS APIs are preserved. Decades of middleware has been developed to use the BLAS API
and it has been exposed to other languages, e.g. by [netlib-java](https://github.com/fommil/netlib-java/).

The second milestone (requires funding) will cover the full BLAS API.

Future milestones will deal with issues such as dynamic load balancing, minimising startup time from cold, auto-batching and
beyond.

## Donations

Please consider supporting this open source project with a donation:

[![Donate via Paypal](https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=B2HW5ATB8C3QW&lc=GB&item_name=multiblas&currency_code=GBP&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)


## Contributing

Contributors are encouraged to fork this repository and issue pull
requests. Contributors implicitly agree to assign an unrestricted licence
to Sam Halliday, but retain the copyright of their code (this means
we both have the freedom to update the licence for those contributions).

