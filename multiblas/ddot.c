/**
  * gcc -shared -o libblas.so.3 -fPIC ddot.c
  */
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>

double 
ddot_(int *N, double *X, int *incX, double *Y, int *incY)
{
	void           *fun;
	void           *handle;
	double          (*blas_ddot) (int *, double *, int *, double *, int *);
	double          (*cublas_ddot) (int *, double *, int *, double *, int *);
	double		ret;
	char           *error;

	if (N[0] > 10000) {
		handle = dlopen("/opt/local/lib/libtatlas.dylib", RTLD_LAZY);
		if (!handle) {
			fputs(dlerror(), stderr);
			exit(1);
		}
		blas_ddot = dlsym(handle, "ddot_");
		if ((error = dlerror()) != NULL) {
			fputs(error, stderr);
			exit(1);
		}
		ret = blas_ddot(N, X, incX, Y, incY);
		dlclose(handle);
		return ret;
	} else {
		handle = dlopen("/opt/local/lib/libsatlas.dylib", RTLD_LAZY);
		if (!handle) {
			fputs(dlerror(), stderr);
			exit(1);
		}
		cublas_ddot = dlsym(handle, "ddot_");
		if ((error = dlerror()) != NULL) {
			fputs(error, stderr);
			exit(1);
		}
		ret = cublas_ddot(N, X, incX, Y, incY);
		dlclose(handle);
		return ret;
	}
}
