This simple shell script manages versions of the [Glasgow Haskell Compiler](https://www.haskell.org/ghc/) on a per-user basis.

To install, clone this repo and add the `bin` directory to the front of your `PATH`.

```
git clone https://gitlab.com/fommil/ghc-env.git ~/.ghc-env

# into ~/.profile
export PATH=$HOME/.ghc-env/bin:$PATH
```

We assume that `~/.cabal/bin` is already on the `PATH`, e.g.

```
export PATH=$HOME/.cabal/bin:$PATH
```

## `install`

To install pre-compiled binaries from https://www.haskell.org simply select the version to install

```
ghc-env install 8.4.3
```

The logic to detect the file to download is very fragile.

This will create symbolic links for `ghc-8.4.3` and `ghc-env-8.4.3` into `~/.local/bin`.

## `compile`

To compile any version of ghc from source:

```
ghc-env compile 8.6.1 8.4.3
```

The first version is the one to download and build.

The second version is the (installed) `ghc` used to bootstrap the compile.

This can take anywhere between 20 minutes and several hours, depending on the performance of your computer.

Make sure you've prepared your system accordingly https://ghc.haskell.org/trac/ghc/wiki/Building/Preparation/Linux and followed the Bootstrapping instructions below for first use.

## Cabal

The version of ghc can be specified on a per-project basis in cabal in the `cabal.project` file, e.g.

```yaml
packages: ./
with-compiler: ghc-8.6.1
```

[Read the docs](https://cabal.readthedocs.io/en/latest/nix-local-build.html#configuring-builds-with-cabal-project) for more examples of what can be configured on a per-project basis in this file.

## Bootstrapping

The best way to bootstrap from scratch is to install the [Haskell Platform](https://www.haskell.org/platform/#linux-generic).

Then create a local build of `cabal`, `alex` and `happy` (required to compile `ghc`) with

```
cabal new-update
cabal new-install --with-compiler=ghc-8.4.3 cabal-install alex happy
```

You can now `compile` versions of `ghc`. The GHC platform in `/usr/local` can be deleted.
