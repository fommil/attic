Excel Addin
===========

The remote host can be configured in xlrpc-Addin.xll.config file or 
overridden in the `XLRPC_URL_PREFIX` environment variable. The environment
variable is read from the process env variables or the user env variables or
from the machine env variables. In that order of priority.

The config file should be the same name as the .xll file with .config 
appended, and they should be in the same directory. The config file follows
this syntax:

```
  <configuration>
    <appSettings>
      <add key = "urlPrefix" value="http://my-host:1234/" />
      <add key = "xlRpcMethodName" value="MyRPC" />
    </appSettings>
  </configuration>
```
