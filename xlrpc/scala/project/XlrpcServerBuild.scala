import sbt._
import Keys._

object XlrpcServerBuild extends Build {

  val buildOrganisation = "com.evcel"
  val buildVersion = "0.1"
  val buildScalaVersion = "2.11.2"
  val buildScalaMajorVersion = "2.11"

  lazy val buildSettings = Defaults.defaultSettings ++ Seq(
    organization := buildOrganisation,
    version := buildVersion,
    scalaVersion := buildScalaVersion,
    scalacOptions := Seq("-unchecked", "-deprecation", "-feature", "-Xfatal-warnings")
  )

  lazy val core = project("core").settings(
    libraryDependencies ++= Seq(
    // 3.2.9 is a buggy version of json4s, if upgrading look at the call to incomingArgsToJavaArgs in ObjectStore
      "org.json4s" %% "json4s-native" % "3.2.9",
      "org.json4s" %% "json4s-jackson" % "3.2.9",
      "org.scalatest" %% "scalatest" % "2.2.0" % "test"
    ),
    scalaSource in Compile := baseDirectory.value / "src",
    scalaSource in Test := baseDirectory.value / "tests"
  )

  lazy val server = project("server").settings(
    libraryDependencies ++= Seq(
      "org.mashupbots.socko" %% "socko-webserver" % "0.6.0",
      "org.scalatest" %% "scalatest" % "2.2.0" % "test"
    ),
    scalaSource in Compile := baseDirectory.value / "src",
    scalaSource in Test := baseDirectory.value / "tests"
  ).dependsOn(core)

  def project(name: String) = {
    Project(
      id = name,
      base = file(name),
      settings = buildSettings
    )
  }
}
