package xlrpc

import akka.actor.{ActorRef, ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import org.mashupbots.socko.routes.{GET, POST, Routes}
import org.mashupbots.socko.webserver.{WebServer, WebServerConfig}

class HttpServer(actorSystem: ActorSystem, port: Int, localOnly: Boolean, handler: ActorRef) {
  val routes = Routes({
    case GET(request) =>
      handler ! request
    case POST(request) =>
      handler ! request
  })

  def start() {
    val host = if (localOnly) "localhost" else "0.0.0.0"
    val webServer = new WebServer(WebServerConfig(hostname = host, port = port), routes, actorSystem)
    webServer.start()

    Runtime.getRuntime.addShutdownHook(new Thread {
      override def run() {
        webServer.stop()
      }
    })
  }
}

object HttpServer {
  val actorConfig = """
	akka {
	  event-handlers = ["akka.event.slf4j.Slf4jEventHandler"]
	  loglevel=DEBUG
	  actor {
	    deployment {
	      /rest-router {
	        router = round-robin
	        nr-of-instances = 20
	      }
	    }
      my-pinned-dispatcher {
        executor = "thread-pool-executor"
        type = PinnedDispatcher
        core-pool-size-min = 2
        core-pool-size-factor = 2.0
        core-pool-size-max = 20
      }
	  }
	}"""

  class TestClass {
    def add(a: Double, b: Double) = a + b
    def mult(a: Double, b: Double) = a * b
    def sleep(a: Double): String = {
      Thread.sleep(a.toInt)
      s"Slept $a ms"
    }
    def testTypes(
      a: String, aa: Array[String], aaa: Array[Array[String]],
      b: Double, bb: Array[Double], bbb: Array[Array[Double]],
      c: Int, cc: Array[Int], ccc: Array[Array[Int]],
      d: Boolean, dd: Array[Boolean], ddd: Array[Array[Boolean]],
      e: Object, ee: Array[Object], eee: Array[Array[Object]]
      ): Array[Array[Object]] = {
      val res = a + ", " + aa.toList + ", " + aaa.toList.map(_.toList) + ", " +
        b + ", " + bb.toList + ", " + bbb.toList.map(_.toList) + ", " +
        c + ", " + cc.toList + ", " + ccc.toList.map(_.toList) + ", " +
        d + ", " + dd.toList + ", " + ddd.toList.map(_.toList) + ", " +
        e + ", " + ee.toList + ", " + eee.toList.map(_.toList)
      Array(Array(res: Object))
    }
  }

  def main(args: Array[String]) {
    val actorSystem: ActorSystem = ActorSystem("HttpServer", ConfigFactory.parseString(actorConfig))
    val handler = ObjectStore(Map(
              "TestClass" -> ExportInstanceWithType(new TestClass(), classOf[TestClass]),
              "Math" -> ExportStatic(classOf[Math])
            ), errorOnOverloaded = false)
    val funcHandler = actorSystem.actorOf(Props(
      new ReflectionFunctionHandler(actorSystem, handler)
    ))
    val httpHandler = actorSystem.actorOf(Props(new HttpHandler(funcHandler)))

    val server = new HttpServer(actorSystem, 1234, localOnly = false, httpHandler)
    server.start()
  }
}
