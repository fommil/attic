package xlrpc

import akka.actor.{ActorRef, Actor}
import org.mashupbots.socko.events.{HttpResponseStatus, HttpRequestEvent}
import org.mashupbots.socko.routes.{POST, Path, GET}

class HttpHandler(functionHandler: ActorRef) extends Actor {
  def receive = {
    case request: HttpRequestEvent =>
      request match {
        case GET(Path("/methods")) =>
          functionHandler ! GetMethods(request)
        case POST(Path("/rpc")) =>
          functionHandler ! ExecuteMethod(request)
        case Path("/favicon.ico") =>
          request.response.write(HttpResponseStatus.NOT_FOUND)
        case _ =>
          request.response.contentType = "text/html"
          request.response.write(homePage)
      }
  }


  def homePage = {
    val buf = new StringBuilder()
    buf.append("<html>\n")
    buf.append("<head>\n")
    buf.append("  <title>xl rpc server</title>\n")
    buf.append("</head>\n")
    buf.append("<body>\n")

    buf.append("<h1>xl rpc server</h1>\n")
    buf.append("<p>This is the homepage for this xl rpc server</p>\n")

    buf.append("<h1>Methods</h1>\n")
    buf.append("<p>Click <a href=\"/methods\">here</a> for a list of methods</p>\n")

    buf.append("</body>\n")
    buf.append("</html>\n")

    buf.toString()
  }
}
