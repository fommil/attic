package xlrpc

import java.lang.reflect.{Modifier, Method}
import akka.actor.{ActorSystem, Terminated, Props, Actor}
import akka.routing.{RoundRobinRoutingLogic, Router, ActorRefRoutee}
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.JsonDSL.WithDouble._
import org.mashupbots.socko.events.HttpRequestEvent

case class GetMethods(request: HttpRequestEvent)

case class ExecuteMethod(request: HttpRequestEvent)

class ReflectionFunctionHandler(actorSystem:ActorSystem, handler: ObjectStore) extends Actor {
  var router = {
    val routees = Vector.fill(5) {
      val r = actorSystem.actorOf(Props[Worker](new Worker(handler)).withDispatcher("akka.actor.my-pinned-dispatcher"))
      context watch r
      ActorRefRoutee(r)
    }
    Router(RoundRobinRoutingLogic(), routees)
  }

  override def receive = {
    case GetMethods(request) =>
      request.response.contentType = "text/json"
      request.response.write(handler.methodsJson)
    case e:ExecuteMethod =>
      router.route(e, sender())
    case Terminated(a) =>
      router = router.removeRoutee(a)
      val r = context.actorOf(Props(new Worker(handler)))
      context watch r
      router = router.addRoutee(r)
  }
}

class Worker(handler: ObjectStore) extends Actor {
  override def receive = {
    case ExecuteMethod(request) =>
      val json = request.request.content.toString(java.nio.charset.Charset.forName("UTF-8"))
      val result = handler.invokeAndResultAsJson(json)
      request.response.contentType = "text/json"
      request.response.write(result)
  }
}