package xlrpc

import java.lang.reflect.{UndeclaredThrowableException, InvocationTargetException, Modifier, Method}
import org.json4s.JsonAST.JArray
import org.json4s._

import org.json4s.JsonDSL.WithDouble._
import org.json4s.jackson.JsonMethods._

import scala.util.control.NonFatal

/**
 * ObjectStore contains the objects exported to excel.
 *
 * The methods method returns the json which describes the interface for excel.
 *
 * The invoke method is called with json from excel. It deserialises the json and invokes the
 * specified method with the given params.
 *
 * @param export Register the methods to be exported. The key becomes the scope
 *               identifier. The value pair of (Object, Class[_]) are the instance
 *               and the type of the instance. The type can be different so you
 *               can have trait which defines the methods and an implementation
 *               with more methods, but which you don't want to export.
 *
 *               For example Map("A" -> (myImplemtation, classOf[MyInterface]))
 *               {{{
 *               trait MyInterface {
 *                def a(s: String): String
 *               }
 *               class MyImplementation extends MyInterface {
 *                def a(s: String) = s
 *                def b(ss: String) = ss
 *               }
 *               }}}
 *               MyInterface.b will not be exported.
 *               In excel you would do: =A.a("?") to call.
 *
 * @param errorOnOverloaded You can not export overloaded methods to excel. This flag
 *                          means we will throw an error when encountering overloaded
 *                          methods. If it is false, one of the methods will be exported,
 *                          it is not defined which one.
 *
 * @param errorOnInvalidTypes Excel only supports a handful of types. It is best to stick
 *                            to strings, doubles or arrays of these as the Excel addin
 *                            is optimised for these types. Some other types are supported.
 *                            If a method has an unsupported type we would generally throw
 *                            but this flag allows us to simply not export that method.
 */
class ObjectStore private (export: Map[String, (Option[Object], Class[_])],
                           errorOnOverloaded: Boolean,
                           errorOnInvalidTypes: Boolean ) {
  private val emptyClass = new Object {}
  private val ignoreMethodNames = emptyClass.getClass.getMethods.map(_.getName).toSet

  private val methods = {
    val byName = export.toList.flatMap {
      case (name, (instance, klass)) =>
        val filterStatic = instance.nonEmpty
        val classMethods = klass.getMethods.filterNot(filterMethod(_, filterStatic))
        classMethods.map {
          case method => (name + "." + method.getName) ->(instance, method)
        }
    }
    val duplicates = byName.groupBy(_._1).filter(_._2.size > 1)
    if(errorOnOverloaded)
      require(duplicates.isEmpty, "You can't overload methods exported to excel: " + duplicates.keys)
    byName.toMap
  }

  val methodsJson = {
    val json = "methods" -> methods.flatMap {
      case (name, (_, method)) =>
        val typesMaybeErrors: Array[Either[String, String]] = method.getParameterTypes.map(t => typeToString(method, t))
        val typeNames: Either[String, Array[String]] = typesMaybeErrors.partition(_.isLeft) match {
          case (Array(), noErrors) => Right(for(Right(t) <- noErrors) yield t)
          case (errors, _) => Left(errors.head.left.get)
        }
        val result = for(params <- typeNames.right; returnType <- typeToString(method, method.getReturnType).right) yield {
          ("name" -> name) ~ ("paramTypes" -> params.toList) ~ ("returnType" -> returnType)
        }
        result.left.foreach(error => if(errorOnInvalidTypes) sys.error(error))
        result.right.toOption
    }
    pretty(render(json))
  }

  def invoke(jsonStr: String) = {
    val json = parse(jsonStr)
    val (instance, method) = (json \ "method").values match {
      case name: String => methods.getOrElse(name, sys.error("Unrecognised method name: " + name))
      case o => sys.error("Invalid method name: " + o)
    }

    def unbundle(value: JValue): Any = value match {
      case JString(s) => s
      case JDouble(d) => d
      case JInt(i) => i
      case JDecimal(d) => d
      case JBool(d) => d
      case JArray(a) => a.map(unbundle).toArray
      case JNull => null
      case JObject(Nil) =>
        // We get JObject(Nil) if you call the function through excel but without
        // specifying all the parameters,
        null
      case o => sys.error("Invalid argument: " + o)
    }

    val params = method.getParameterTypes
    val arguments = unbundle(json \\ "args").asInstanceOf[Array[Object]]

    // There's a bug in 3.2.9 of json4s that means arguments comes wrapped on a single entry list.
    // In 3.2.10 it is fixed and this line needs to be changed to the one below it.
    val args = incomingArgsToJavaArgs(params, arguments(0).asInstanceOf[Array[Object]])
    //    val args = incomingArgsToJavaArgs(params, arguments) // For when we use 3.2.10 json4s

    method.invoke(instance.orNull, args.asInstanceOf[Array[Object]]: _*)
  }

  def invokeAndResultAsJson(jsonStr: String) = try {
    val res = invoke(jsonStr)
    toJson(res)
  } catch {
    case e: InvocationTargetException =>
      toJson("#Error: " + e.getCause.getMessage)
    case e: UndeclaredThrowableException =>
      toJson("#Error: " + e.getCause.getMessage)
    case NonFatal(e) =>
      toJson("#Error:" + e.getMessage)
  }

  private def toJson(obj: Object) = {
    obj match {
      case d: Number => pretty(render("result" -> List(List(d.doubleValue()))))
      case s: String => pretty(render("result" -> List(List(s))))
      case d: Array[Double] => pretty(render("result" -> List(d.toList.map(_.doubleValue()))))
      case d: Array[Array[Double]] => pretty(render("result" -> d.toList.map(_.toList.map(_.doubleValue()))))
      case d: Array[String] => pretty(render("result" -> List(d.toList)))
      case d: Array[Array[String]] => pretty(render("result" -> d.toList.map(_.toList)))
      case d: Array[Array[Object]] =>
        val res = d.map {
          _.map {
            case s: String => s: JValue
            case d: java.lang.Number => d.doubleValue(): JValue
            case o => sys.error("Invalid return type: " + (o.getClass, o))
          }.toList
        }
        pretty(render("result" -> res.toList))
      case d: Array[Object] =>
        val res = d.map {
          case s: String => s: JValue
          case d: java.lang.Number => d.doubleValue(): JValue
          case o => sys.error("Invalid return type: " + (o.getClass, o))
        }
        pretty(render("result" -> List(res.toList)))
      case _ => sys.error("Invalid return type: " + obj)
    }
  }

  private def filterMethod(m: Method, filterStatic: Boolean) = {
    Modifier.isPrivate(m.getModifiers) ||
      Modifier.isProtected(m.getModifiers) ||
      (Modifier.isStatic(m.getModifiers) && filterStatic) ||
      m.getName.contains("$") ||
      ignoreMethodNames.contains(m.getName)
  }

  private def incomingArgsToJavaArgs(params: Array[Class[_]], arguments: Array[Object]) = {
    for (((param, arg), i) <- params.zip(arguments).zipWithIndex) yield {
      try {
        param.toString match {
          case "boolean" => boolValue(arg)
          case "double" => doubleValue(arg)
          case "float" => doubleValue(arg)
          case "long" => longValue(arg)
          case "int" => intValue(arg)
          case "class java.lang.Object" => arg
          case "class java.lang.String" => applyNotNull(arg, _.toString)

          case "class [D" =>
            val values = flatten(arg.asInstanceOf[Array[Object]])
            val doubles = Array.ofDim[Double](values.size)
            (0 until doubles.size).foreach(
              i => doubles(i) = doubleValue(values(i))
            )
            doubles
          case "class [I" =>
            val values = flatten(arg.asInstanceOf[Array[Object]])
            val ints = Array.ofDim[Int](values.size)
            (0 until ints.size).foreach(
              i => ints(i) = intValue(values(i))
            )
            ints
          case "class [Ljava.lang.Object;" => flatten(arg.asInstanceOf[Array[Object]])
          case "class [Ljava.lang.String;" =>
            val values = flatten(arg.asInstanceOf[Array[Object]])
            values.map(_.toString)
          case "class [Z" =>
            val values = flatten(arg.asInstanceOf[Array[Object]])
            val bools = Array.ofDim[Boolean](values.size)
            (0 until bools.size).foreach(
              i => bools(i) = boolValue(values(i))
            )
            bools

          case "class [[D" =>
            val (actual, colSize) = to2dArray(arg)
            val doubles = Array.ofDim[Double](actual.size, colSize)
            for (i <- 0 until doubles.size; j <- 0 until doubles(0).size) {
              doubles(i)(j) = doubleValue(actual(i)(j))
            }
            doubles
          case "class [[I" =>
            val (actual, colSize) = to2dArray(arg)
            val ints = Array.ofDim[Int](actual.size, colSize)
            for (i <- 0 until ints.size; j <- 0 until ints(0).size) {
              ints(i)(j) = intValue(actual(i)(j))
            }
            ints
          case "class [[Ljava.lang.Object;" =>
            val (values, _) = to2dArray(arg)
            values
          case "class [[Ljava.lang.String;" =>
            val (values, _) = to2dArray(arg)
            values.map(_.asInstanceOf[Array[_]]).map(_.map(e => applyNotNull(e, _.toString)))
          case "class [[Z" =>
            val (actual, colSize) = to2dArray(arg)
            val ints = Array.ofDim[Boolean](actual.size, colSize)
            for (i <- 0 until ints.size; j <- 0 until ints(0).size) {
              ints(i)(j) = boolValue(actual(i)(j))
            }
            ints

          case e => sys.error("Invalid param type: " + e)
        }
      }
      catch {
        case e: ClassCastException => throw new RuntimeException("Invalid type at " + i + " (" + e.getMessage + ")", e)
      }
    }
  }

  private def flatten(a: Array[Object]) = if(a == null) {
    Array.ofDim[Object](0)
  } else {
    a.flatMap {
      case a: Array[Object] => a
      case a: Any => Array(a)
    }
  }

  private def applyNotNull[T](a: Any, f: Any => T): T = a match {
    case null => null.asInstanceOf[T]
    case o => f(o)
  }

  private def boolValue(a: Any) = a match {
    case b: Boolean => b
    case i: BigInt => i.intValue() != 0
    case d: BigDecimal => d.intValue() != 0
    case d: Double => d.toInt != 0
    case null => null.asInstanceOf[Boolean]
    case o => sys.error("Invalid boolean: " + o)
  }

  private def intValue(a: Any) = applyNotNull(longValue(a), _.asInstanceOf[Long].toInt)

  private def longValue(a: Any) = a match {
    case i: BigInt => i.longValue()
    case d: BigDecimal => d.toLong
    case d: Double => d.toLong
    case null => null.asInstanceOf[Long]
    case o => sys.error("Invalid number: " + o)
  }

  private def floatValue(a: Any) = applyNotNull(doubleValue(a), _.asInstanceOf[Double].toInt)

  private def doubleValue(a: Any) = a match {
    case i: BigInt => i.doubleValue()
    case d: BigDecimal => d.doubleValue()
    case d: Double => d
    case null => null.asInstanceOf[Double]
    case o => sys.error("Invalid number: " + o)
  }

  private def to2dArray(a: Any) = if(a == null ) {
    (Array.ofDim[Object](0, 0), 0)
  } else {
    val values = a.asInstanceOf[Array[Object]]
    val actual = values.map(_.asInstanceOf[Array[Object]])
    require(actual.map(_.size).distinct.size == 1, "Array should not be jagged")
    (actual, actual.map(_.size).head)
  }

  /**
   * everything is exported as either double, string or object and arrays of these. the addin
   * is optimized for these types.
   */
  private def typeToString(method: Method, clazz: Class[_]): Either[String, String] = clazz.toString match {
    case "double" | "int" | "boolean" | "long" | "float" => Right("double")
    case "class [D" | "class [I" | "class [F" | "class [J" | "class [Z" => Right("double[]")
    case "class [[D" | "class [[I" | "class [[F" | "class [[J" | "class [[Z" => Right("double[][]")
    case "class java.lang.String" => Right("string")
    case "class [Ljava.lang.String;" => Right("string[]")
    case "class [[Ljava.lang.String;" => Right("string[][]")
    case "class java.lang.Object" => Right("object")
    case "class [Ljava.lang.Object;" => Right("object[]")
    case "class [[Ljava.lang.Object;" => Right("object[][]")
    case "void" => Left(method.getName + ", void return types are not valid")
    case n@"class scala.runtime.Nothing$" =>
      Left(method.getName + ", invalid type: " + n + ". " +
        "This is usually caused by defining a method that always throws.")
    case t =>
      Left(method.getName + ", invalid type: " + t + ". " +
        "Valid types are doubles, strings, object and one or two dimension arrays of those.")
  }
}

sealed trait Export
case class ExportInstance(instance: Object) extends Export
case class ExportInstanceWithType[T](instance: Object, klass: Class[T]) extends Export
case class ExportStatic[T](klass: Class[T]) extends Export

object ObjectStore {

  def apply(
    export: Map[String, Export],
    errorOnOverloaded: Boolean = true,
    errorOnInvalidTypes: Boolean = true): ObjectStore = {
    new ObjectStore(export.mapValues {
      case ExportInstanceWithType(instance, klass) => (Some(instance), klass)
      case ExportInstance(instance) => (Some(instance), instance.getClass)
      case ExportStatic(klass) => (None, klass)
    }, errorOnOverloaded, errorOnInvalidTypes)
  }

  /**
   * convenience method. other apply is a better choice.
   */
  def apply(export: (String, Object)*): ObjectStore = {
    apply(export.map {
      case (name, instance) => name -> ExportInstance(instance)
    }.toMap)
  }
}