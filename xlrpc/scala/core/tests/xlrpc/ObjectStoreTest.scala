package xlrpc

import org.scalatest.{FunSuite, ShouldMatchers}

class ObjectStoreTest extends FunSuite with ShouldMatchers{

  test("test void creating json") {
    class A {
      def aa() = Array(Array("?"))
    }
    val json = ObjectStore("AClass" -> new A).methodsJson
    json shouldEqual
      """{
        |  "methods" : [ {
        |    "name" : "AClass.aa",
        |    "paramTypes" : [ ],
        |    "returnType" : "string[][]"
        |  } ]
        |}""".stripMargin
  }

  test("test simple creating json") {
    class A {
      def aa(x: Double, y: Array[Double], z: Array[Array[String]]): Array[Double] = Array(0.0)
    }
    class B {
      def bb(x: Any, y: Array[Any], z: Array[Array[Any]]): Array[String] = Array("?")
    }

    val json = ObjectStore("AClass" -> new A, "BClass" -> new B).methodsJson
    json shouldEqual
      """{
        |  "methods" : [ {
        |    "name" : "AClass.aa",
        |    "paramTypes" : [ "double", "double[]", "string[][]" ],
        |    "returnType" : "double[]"
        |  }, {
        |    "name" : "BClass.bb",
        |    "paramTypes" : [ "object", "object[]", "object[][]" ],
        |    "returnType" : "string[]"
        |  } ]
        |}""".stripMargin
  }

  test("invoke no params") {
    class A {
      def aa() = "hw"
    }
    val jsonStr =
      """{
        |"method": "A.aa",
        |"args": []
        |}""".stripMargin

    val objectStore = ObjectStore("A" -> new A)

    objectStore.invoke(jsonStr) shouldEqual "hw"
    objectStore.invokeAndResultAsJson(jsonStr) shouldEqual """{
                                                                 |  "result" : [ [ "hw" ] ]
                                                                 |}""".stripMargin
  }

  test("invoke single argument") {
    class A {
      def aa(x: Double) = x * 3
    }
    val jsonStr =
      """{
        |"method": "A.aa",
        |"args": [3.0]
        |}""".stripMargin

    val objectStore = ObjectStore("A" -> new A)
    objectStore.invoke(jsonStr) shouldEqual 9.0
    objectStore.invokeAndResultAsJson(jsonStr) shouldEqual """{
                                                                 |  "result" : [ [ 9.0 ] ]
                                                                 |}""".stripMargin
  }
  test("invoke 2 doubles") {
    class A {
      def aa(x: Double, y: Double) = x + y
    }
    val jsonStr =
      """{
        |"method": "A.aa",
        |"args": [3.0, 4.0]
        |}""".stripMargin

    val objectStore = ObjectStore("A" -> new A)
    objectStore.invoke(jsonStr) shouldEqual 7.0
    objectStore.invokeAndResultAsJson(jsonStr) shouldEqual """{
                                                                 |  "result" : [ [ 7.0 ] ]
                                                                 |}""".stripMargin
  }

  test("invoke array doubles") {
    class A {
      def aa(x: Array[Double]) = x
    }
    val jsonStr =
      """{
        |"method": "A.aa",
        |"args": [[3.0, 4.0]]
        |}""".stripMargin

    val objectStore = ObjectStore("A" -> new A)
    objectStore.invoke(jsonStr) shouldEqual Array(3.0, 4.0)
    objectStore.invokeAndResultAsJson(jsonStr) shouldEqual """{
                                                             |  "result" : [ [ 3.0, 4.0 ] ]
                                                             |}""".stripMargin
  }

  test("null params") {
    class A {
      def aa(a: Int, b: Boolean): Array[Any] = {
        assert(a === null.asInstanceOf[Double])
        assert(b === null.asInstanceOf[Boolean])
        Array(a.asInstanceOf[Double], b.asInstanceOf[Boolean].toString)
      }
    }
    val jsonStr =
      """{
        |"method": "A.aa",
        |"args": [null, null]
        |}""".stripMargin

    val objectStore = ObjectStore("A" -> new A)
    objectStore.invoke(jsonStr) shouldEqual Array(0.0, "false")
    objectStore.invokeAndResultAsJson(jsonStr) shouldEqual """{
                                                             |  "result" : [ [ 0.0, "false" ] ]
                                                             |}""".stripMargin
  }

  test("too many params passed") {
    class A {
      def aa(a: Int): Array[Double] = {
        Array(a.toDouble)
      }
    }
    // We register a generic rpc function with excel that calls our code
    // and puts empty lists for unspecified params.
    val jsonStr =
      """{
        |"method": "A.aa",
        |"args": [null, {}, [], null]
        |}""".stripMargin

    val objectStore = ObjectStore("A" -> new A)
    objectStore.invoke(jsonStr) shouldEqual Array(0.0)
    objectStore.invokeAndResultAsJson(jsonStr) shouldEqual """{
                                                             |  "result" : [ [ 0.0 ] ]
                                                             |}""".stripMargin
  }

  test("invoke double and arrays") {
    class A {
      def aa(x: Double, yy: Array[Double]) = Array.fill[Double](x.toInt, yy.sum.toInt)(7)
    }
    val jsonStr =
      """{
        |"method": "A.aa",
        |"args": [2.0, [2.0, 3.0]]
        |}""".stripMargin

    val objectStore = ObjectStore("A" -> new A)
    objectStore.invoke(jsonStr) shouldEqual Array(Array(7.0, 7.0, 7.0, 7.0, 7.0), Array(7.0, 7.0, 7.0, 7.0, 7.0))
    objectStore.invokeAndResultAsJson(jsonStr) shouldEqual
      """{
        |  "result" : [ [ 7.0, 7.0, 7.0, 7.0, 7.0 ], [ 7.0, 7.0, 7.0, 7.0, 7.0 ] ]
        |}""".stripMargin
  }

  test("invoke and return array of object") {
    class A {
      def aa() = Array(Array("market1", 1.0), Array("market2", 2.0))
    }
    val jsonStr =
      """{
        |"method": "A.aa",
        |"args": []
        |}""".stripMargin

    val objectStore = ObjectStore("A" -> new A)
    objectStore.invoke(jsonStr) shouldEqual Array(Array("market1", 1.0), Array("market2", 2.0))
    objectStore.invokeAndResultAsJson(jsonStr) shouldEqual
      """{
        |  "result" : [ [ "market1", 1.0 ], [ "market2", 2.0 ] ]
        |}""".stripMargin
  }

  test("array strings") {
    class A {
      def aa(a: Array[String], b: Array[Array[String]]): Array[Array[String]] = {
        b
      }
    }

    val jsonStr =
      """
        |{
        |"method":"A.aa",
        |"args":
        |[
        | ["b"],
        | [["c"],["d"]]
        |]
        |}
      """.stripMargin

    val objectStore = ObjectStore("A" -> new A)
    objectStore.invokeAndResultAsJson(jsonStr) shouldEqual """{
                                                             |  "result" : [ [ "c" ], [ "d" ] ]
                                                             |}""".stripMargin
  }

  test("all supported types") {
    class A {
      def aa(
        a: String, aa: Array[String], aaa: Array[Array[String]],
        b: Double, bb: Array[Double], bbb: Array[Array[Double]],
        c: Int, cc: Array[Int], ccc: Array[Array[Int]],
        d: Boolean, dd: Array[Boolean], ddd: Array[Array[Boolean]],
        e: Object, ee: Array[Object], eee: Array[Array[Object]]
        ): Array[Array[Object]] = {
        val str = a + ", " + aa.toList + ", " + aaa.toList.map(_.toList) + ", " +
          b + ", " + bb.toList + ", " + bbb.toList.map(_.toList) + ", " +
          c + ", " + cc.toList + ", " + ccc.toList.map(_.toList) + ", " +
          d + ", " + dd.toList + ", " + ddd.toList.map(_.toList) + ", " +
          e + ", " + ee.toList + ", " + eee.toList.map(_.toList)
        Array(Array(str))
      }
    }

    val jsonStr1 =
      """
        |{
        |"method":"A.aa",
        |"args":
        |[
        |"a", ["aa", "2a"], [["aaa"]],
        |1.0, [2.0, 2.5], [[3.0]],
        |4.0, [5.0], [[6.0]],
        |false, [true], [[false]],
        |"e", ["ee"], [["eee"]]
        |]
        |}
      """.stripMargin

    // excel converts 1d arrays to 2d arrays sometimes so this string is equivalent to above.
    val jsonStr2 =
      """
        |{
        |"method":"A.aa",
        |"args":
        |[
        |"a", [["aa"],["2a"]], [["aaa"]],
        |1, [[2],[2.5]], [[3.0]],
        |4, [[5]], [[6]],
        |0, [1.0], [[0]],
        |"e", ["ee"], [["eee"]]
        |]
        |}
      """.stripMargin

    val objectStore = ObjectStore("A" -> new A)
    for(jsonStr <- List(jsonStr1, jsonStr2)) {
      objectStore.invokeAndResultAsJson(jsonStr) shouldEqual
        """{
             |  "result" : [ [ "a, List(aa, 2a), List(List(aaa)), 1.0, List(2.0, 2.5), List(List(3.0)), 4, List(5), List(List(6)), false, List(true), List(List(false)), e, List(ee), List(List(eee))" ] ]
             |}""".stripMargin
      }

    val excelCallWithNoParams =
      """
        |{"method":"A.aa","args":[{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}]}
      """.stripMargin
    objectStore.invokeAndResultAsJson(excelCallWithNoParams) shouldEqual
      """{
        |  "result" : [ [ "null, List(), List(), 0.0, List(), List(), 0, List(), List(), false, List(), List(), null, List(), List()" ] ]
        |}""".stripMargin

    val excelCallWithNullsMixedIn =
      """
        |{"method":"A.aa",
        |"args":["str",[["str","str","str"]],[["str",{},{}],["str","str","str"]],
        |1.0,[[2.0,3.0,4.0]],[[1.0,{},{}],[2.0,3.0,4.0]],1.0,[[2.0,3.0,4.0]],
        |[[1.0,{},{}],[2.0,3.0,4.0]],
        |true,[[false,true,false]],[[true,{},{}],[false,true,false]],
        |"str",[["str","str","str"]],[["str",{},{}],["str","str","str"]]]}
      """.stripMargin
    objectStore.invokeAndResultAsJson(excelCallWithNullsMixedIn) shouldEqual
      """{
        |  "result" : [ [ "str, List(str, str, str), List(List(str, null, null), List(str, str, str)), 1.0, List(2.0, 3.0, 4.0), List(List(1.0, 0.0, 0.0), List(2.0, 3.0, 4.0)), 1, List(2, 3, 4), List(List(1, 0, 0), List(2, 3, 4)), true, List(false, true, false), List(List(true, false, false), List(false, true, false)), str, List(str, str, str), List(List(str, null, null), List(str, str, str))" ] ]
        |}""".stripMargin
  }

  test("export static") {
    val os = ObjectStore(Map("Math" -> ExportStatic(classOf[Math])), errorOnOverloaded = false)
    os.invokeAndResultAsJson("""{"method":"Math.pow","args":[2, 10]}""") shouldEqual "{\n  \"result\" : [ [ 1024.0 ] ]\n}"
  }

  test("errors") {
    intercept[IllegalArgumentException] {
      ObjectStore(Map("Math" -> ExportStatic(classOf[Math])))
    }.getMessage should startWith("requirement failed: You can't overload methods exported to excel: ")
    ObjectStore(Map("Math" -> ExportStatic(classOf[Math])), errorOnOverloaded = false)

    intercept[RuntimeException] {
      ObjectStore(Map("String" -> ExportInstance("?")), errorOnOverloaded = false)
    }.getMessage should include("invalid type")
    ObjectStore(Map("String" -> ExportInstance("?")), errorOnOverloaded = false, errorOnInvalidTypes = false)
  }
}
