﻿using System;
using System.Linq;
using NUnit.Framework;

namespace xlrpc.Test
{
    [TestFixture]
    public class JsonInvokerTest
    {
        class TestInvoker : IRemoteInvoker
        {
            private readonly Func<String, String> _func;
            public TestInvoker(Func<string, string> func)
            {
                _func = func;
            }

            public string Invoke(string method)
            {
                return _func(method);
            }
        }

        private void test(String invokeStr, String resultStr, Object expected, params object[] args)
        {
            Func<String, String> func = (a) =>
            {
                Assert.AreEqual(a, invokeStr);
                return resultStr;
            };
            var test = new TestInvoker(func);
            var invoker = new JsonInvoker(test);

            var result = invoker.Invoke("method1", args);
            Assert.That(expected, Is.EqualTo(result));
        }

        [Test]
        public void TestInvokeArrayDoubles()
        {
            var invoke = @"{""method"":""method1"",""args"":[[1.0,2.0,3.0]]}";
            var resultString = @"{
          ""result"" : [ [ 7.0, 7.0, 7.0, 7.0, 7.0 ] ]
        }";
            test(invoke, resultString, new[,] { {7.0, 7.0, 7.0, 7.0, 7.0} }, new[] { 1.0, 2.0, 3.0 });
        }

        [Test]
        public void TestInvokeArrayArrayDoubles()
        {
            var invoke = @"{""method"":""method1"",""args"":[[[1.0,2.0,3.0],[4.0,5.0,6.0]]]}";
            var resultString = @"{
          ""result"" :[ [ 7.0, 7.0, 7.0, 7.0, 7.0 ], [ 7.0, 7.0, 7.0, 7.0, 7.0 ] ]
        }";
            test(invoke, resultString, 
                new[,] { {7.0, 7.0, 7.0, 7.0, 7.0},{7.0, 7.0, 7.0, 7.0, 7.0} }, 
                new[,] {{ 1.0, 2.0, 3.0 },{ 4.0, 5.0, 6.0 }});
        }
    }
}
