﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace xlrpc.Test
{
    [TestFixture]
    public class IntgerationTest
    {
//        [Test]
        public void MultiThreadedTest()
        {
            var http = new Http("/*URL*/");
            var invoker = new JsonInvoker(http);
            Invoker.MethodInvoker = invoker;
            var method = typeof(Invoker).GetMethod("Invoke", new[] { typeof(string), typeof(object[]) });
            var json = http.MethodJson();
            var methodParser = new MethodParser(json, typeof(Invoker), method);
            var methods = methodParser.Methods();
            var result = methods.Item1.Find(m => m.Name.Equals("TestClass.mult"));
            Console.WriteLine("Starting 10 threads");
            Parallel.ForEach(Enumerable.Repeat(result, 10), m => CallMethods(invoker, m));
            Console.WriteLine("Test done");
        }

        private void CallMethods(JsonInvoker invoker , MethodInfo method)
        {
            Console.WriteLine("query method " + method.Name + " 1000 times.");
            for (var i = 0; i < 1000; i++)
            {
                var res = method.Invoke(invoker, new object[] { 2.0, 5.0 });
                Console.Out.WriteLine("result: " + res);
                Console.Out.Flush();  
            }
        }
    }
}
