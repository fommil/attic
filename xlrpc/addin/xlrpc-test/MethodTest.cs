﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using xlrpc;

namespace xlrpc_test
{
    [TestFixture]
    public class MethodTest
    {
        class TestInvoker : IMethodInvoker
        {
            private readonly Func<String, Object[], object> _func;
            public TestInvoker(Func<string, object[], object> func)
            {
                _func = func;
            }

            public object Invoke(string name, params object[] args)
            {
                return _func(name, args);
            }
        }

        [Test]
        public void TestParseJson()
        {
            var json = @"
{
	""Methods"": [
		{
		""name"": ""method1"",
		""paramTypes"": [
			""string""
		],
		""returnType"": ""string"",
		}
	]
}";
            var method = typeof (Invoker).GetMethod("Invoke", new[] {typeof (string), typeof (object[])});
            var methodParser = new MethodParser(json, typeof(Invoker), method);

            var isCalled = false;
            Func<String, Object[], object> func = (name, args) =>
            {
                isCalled = true;
                Assert.AreEqual("method1", name);
                Assert.AreEqual(args, new string[]{"arg1"});
                return "Result";
            };

            var invoker = new TestInvoker(func);
            Invoker.MethodInvoker = invoker;
            var method1 = methodParser.MethodsOnly().First();
            var result = method1.Invoke(null, new object[] { "arg1" });
            Assert.IsTrue(isCalled, "Method wasn't called");
            Assert.AreEqual("Result", result);
        }
    }
}
