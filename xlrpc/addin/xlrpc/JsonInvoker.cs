﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace xlrpc
{
    public class JsonInvoker : IMethodInvoker
    {
        private readonly IRemoteInvoker _invoker;

        public JsonInvoker(IRemoteInvoker invoker)
        {
            _invoker = invoker;
        }

        public object Invoke(string name, params object[] args)
        {
            try
            {
                var invokeDict = new Dictionary<String, Object> {{"method", name}, {"args", args}};
                var json = JsonConvert.SerializeObject(invokeDict);
                var jsonResult = _invoker.Invoke(json);

                var resultDict = JsonConvert.DeserializeObject<Dictionary<String, object[,]>>(jsonResult);
                return resultDict["result"];
            }
            catch (Exception e)
            {
                return "#Error deserialising: " + e.Message;
            }
        }
    }
}
