﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ExcelDna.Integration;
using NUnit.Framework;

namespace xlrpc
{
    public class XlAddIn : IExcelAddIn
    {
        private static Exception _lastException;

        public void AutoOpen()
        {
            try
            {
                RegisterXlRpcMethod();
                RegisterRemoteMethods();
            }
            catch (Exception e)
            {
                _lastException = e;
                var method = GetType().GetMethod("xlRpcError");
                ExcelIntegration.RegisterMethods(new List<MethodInfo>(new[]{method}));
            }
        }

        private void RegisterRemoteMethods()
        {
            var host = GetHost();
            var http = new Http(host);
            var invoker = new JsonInvoker(http);
            Invoker.MethodInvoker = invoker;
            var method = typeof(Invoker).GetMethod("Invoke", new[] { typeof(string), typeof(object[]) });
            var json = http.MethodJson();
            var methodParser = new MethodParser(json, typeof(Invoker), method);
            var methods = methodParser.Methods();
            ExcelIntegration.RegisterMethods(methods.Item1, methods.Item2, methods.Item3);
        }

        private void RegisterXlRpcMethod()
        {
            var xlRpcMethodName = GetAppSetting("xlRpcMethodName") ?? "xlRpc";
            var methodJson = @"
{
	""methods"": [
		{
		""name"": """+xlRpcMethodName+@""",
		""paramTypes"": [
			""object"",""object"",""object"",""object"",""object"",""object"",""object"",""object"",
			""object"",""object"",""object"",""object"",""object"",""object"",""object"",""object"",
			""object"",""object"",""object"",""object"",""object"",""object"",""object"",""object"",
			""object"",""object"",""object"",""object"",""object"",""object"",""object"",""object"",
			""object"",""object"",""object"",""object"",""object"",""object"",""object"",""object""
		],
		""returnType"": ""object"",
		}
	]
}";
            var xlRpcMethod = typeof(XlAddIn).GetMethod("xlRpc", new[] { typeof(string), typeof(object[]) });
            var methodParser = new MethodParser(methodJson, typeof(Invoker), xlRpcMethod);
            var xlRpc = methodParser.Methods();
            ExcelIntegration.RegisterMethods(xlRpc.Item1, xlRpc.Item2, xlRpc.Item3);
        }

        public static string xlRpcError()
        {
            var error = "";
            if (_lastException is TypeInitializationException)
            {
                error = _lastException.InnerException.Message;
            }
            else
            {
                error = _lastException.Message;
            }
            return error;
        }

        public static object xlRpc(string name, params object[] args)
        {
            return Invoker.Invoke(args[0].ToString(), args.Skip(1).ToArray());
        }

        public void AutoClose()
        {
        }

        internal static string GetHost()
        {
            var hostFromUser = Environment.GetEnvironmentVariable("XLRPC_URL_PREFIX", EnvironmentVariableTarget.User);
            var hostFromSystem = Environment.GetEnvironmentVariable("XLRPC_URL_PREFIX", EnvironmentVariableTarget.Machine);
            var hostFromProc = Environment.GetEnvironmentVariable("XLRPC_URL_PREFIX", EnvironmentVariableTarget.Process);
            var host = hostFromProc ?? hostFromUser ?? hostFromSystem;
            if(host == null)
                return GetAppSetting("urlPrefix");
            return host;
        }

        internal static string GetAppSetting(string key)
        {
            object setting = System.Configuration.ConfigurationManager.AppSettings[key];
            if (setting == null)
            {
                throw new Exception("No value for key: " + key);
            }
            return setting.ToString();
        } 
    }

}
