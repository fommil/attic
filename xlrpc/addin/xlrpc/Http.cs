﻿using System;
using System.Text;
using System.IO;
using System.Net;

namespace xlrpc
{
    public interface IRemoteInvoker
    {
        String Invoke(String json);
    }

    public class Http : IRemoteInvoker
    {
        private readonly String _urlPrefix;
        readonly string _userName = Environment.UserName;
       
        public Http(String urlPrefix)
        {
            ServicePointManager.DefaultConnectionLimit = 10;
            if (urlPrefix.EndsWith("/"))
                _urlPrefix = urlPrefix.TrimEnd(Convert.ToChar("/"));
            else
                _urlPrefix = urlPrefix;
            var windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (windowsIdentity != null)
            {
                _userName = windowsIdentity.Name;
            }
        }

        public String MethodJson()
        {
            WebRequest request = WebRequest.Create(_urlPrefix + "/methods");
            request.Headers.Add("user", _userName);
            WebResponse response = request.GetResponse();
            Console.WriteLine(((HttpWebResponse) response).StatusDescription);
            Stream dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            response.Close();
            return responseFromServer;
        }

        public String Invoke(String json)
        {
            
            var request = (HttpWebRequest)WebRequest.Create(_urlPrefix + "/rpc");
            // Set AllowWriteStreamBuffering to 'false'. 
            request.AllowWriteStreamBuffering = false;
            request.Method = "POST";
            request.Headers.Add("user", _userName);
            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            request.ContentType = "text/json";
            request.ContentLength = byteArray.Length;
            var dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            
            var response = request.GetResponse();
            dataStream = response.GetResponseStream();
            var reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();
            return responseFromServer;
        }
    }
}
