﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xlrpc
{
    public interface IMethodInvoker
    {
        object Invoke(string name, params object[] args);
    }

    public class Invoker
    {
        private static IMethodInvoker methodInvoker;

        public static IMethodInvoker MethodInvoker
        {
            set { methodInvoker = value; }
            get { return methodInvoker; }
        }

        public static object Invoke(string name, params object[] args)
        {
            return methodInvoker.Invoke(name, args);
        }
    }
}
