﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;
using ExcelDna.Integration;
using Strilanc.Value;

namespace xlrpc
{
    class IniReader
    {
        readonly string _path;

        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);

        public IniReader()
        {
            var myName = (string) XlCall.Excel(XlCall.xlGetName);
            var ini = myName.Replace(".xll", ".ini");
            _path = new FileInfo(ini).FullName;
            if (!new FileInfo(ini).Exists)
            {
                throw new Exception(".ini file not found: " + _path);
            }
        }

        public string Read(string key)
        {
            return Read(key, new May<string>());
        }

        public string Read(string key, May<string> defaultValue)
        {
            var retVal = new StringBuilder(1024);
            GetPrivateProfileString("xlrpc", key, defaultValue.Else(""), retVal, 1024, _path);
            if (retVal.ToString() == "")
            {
                throw new Exception("No value for key: "+ key);
            }
            return retVal.ToString();
        }
    }
}
