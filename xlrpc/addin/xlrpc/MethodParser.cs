﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using ExcelDna.Integration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace xlrpc
{
    public class MethodParser
    {
        private readonly string _json;
        private readonly MethodInfo _invoker;
        private readonly Type _invokerType;

        public MethodParser(String json, Type invokerType, MethodInfo invoker)
        {
            _json = json;
            _invoker = invoker;
            _invokerType = invokerType;
        }

        public List<MethodInfo> MethodsOnly()
        {
            return Methods().Item1;
        } 

        public Tuple<List<MethodInfo>, List<object>, List<List<object>>> Methods()
        {
            var methodAttr = new ExcelFunctionAttribute();
            methodAttr.IsThreadSafe = true;
            methodAttr.IsExceptionSafe = false;
            methodAttr.IsMacroType = false;
            methodAttr.IsVolatile = false;

            // not doing anything with these yet
            var argAttr = new ExcelArgumentAttribute();

            var csMethods = new List<MethodInfo>();
            var methodAttributes = new List<object>();
            var argAttributes = new List<List<object>>();

            dynamic jo = JObject.Parse(_json);
            var methods = jo.methods;
            foreach (var method in methods)
            {
                var paramTypes = new List<Type>();
                var methodArgAttributes = new List<object>();
                foreach (var param in method.paramTypes)
                {
                    // specifying everything as an object allows us to 
                    // do type error handling properly in the server.
                    // otherwise if you get any param type wrong
                    // excel just shows #VALUE in the cell and doesn't 
                    // tell you which param is wrong.
//                    paramTypes.Add(StringToType[param.ToString()]);
                    paramTypes.Add(typeof (object));
                    methodArgAttributes.Add(argAttr);
                }
                argAttributes.Add(methodArgAttributes);
                var returnType = typeof (object);

                MethodInfo methodInfo = CreateMethod(method.name.ToString(), paramTypes, returnType);
                csMethods.Add(methodInfo);
                methodAttributes.Add(methodAttr);
            }
            return Tuple.Create(csMethods, methodAttributes, argAttributes);
        }

        private DynamicMethod CreateMethod(String name, List<Type> paramTypes, Type returnType)
        {
            var method = new DynamicMethod(name, returnType, paramTypes.ToArray(), _invokerType);

            var parameterInfos = method.GetParameters();

            var il = method.GetILGenerator();

            // Create array
            var parameters = il.DeclareLocal(typeof(object[]));

            il.Emit(OpCodes.Ldc_I4, parameterInfos.Length);
            il.Emit(OpCodes.Newarr, typeof(Object));
            il.Emit(OpCodes.Stloc, parameters);

            // Store values in array
            for (int parameterIndex = 0; parameterIndex < parameterInfos.Length; parameterIndex++)
            {
                ParameterInfo parameter = parameterInfos[parameterIndex];

                il.Emit(OpCodes.Ldloc, parameters);
                il.Emit(OpCodes.Ldc_I4, parameterIndex);
                il.Emit(OpCodes.Ldarg, parameterIndex);

                Box(il, parameter.ParameterType);

                il.Emit(OpCodes.Stelem_Ref);
            }

            il.Emit(OpCodes.Ldstr, name);

            il.Emit(OpCodes.Ldloc, parameters);

            il.Emit(OpCodes.Call, _invoker);

            UnBox(il, returnType);
            il.Emit(OpCodes.Ret);

            return method;
        }

        private static readonly IDictionary<string, Type> StringToType = new Dictionary<string, Type>
        {
            {"double", typeof(double)},
            {"string", typeof(string)},
            {"object", typeof(object)},

            {"double[]", typeof(double[])},
            {"string[]", typeof(string[])},
            {"object[]", typeof(object[])},

            {"double[][]", typeof(double[,])},
            {"string[][]", typeof(string[,])},
            {"object[][]", typeof(object[,])},
        };


        private void UnBox(ILGenerator il, Type type)
        {
            if (CanBox(type))
            {
                il.Emit(OpCodes.Unbox_Any, type);
            }
        }

        private void Box(ILGenerator il, Type type)
        {
            if (CanBox(type))
            {
                il.Emit(OpCodes.Box, type);
            }
        }

        private bool CanBox(Type type)
        {
            return type.IsPrimitive;
        }
    }
}
