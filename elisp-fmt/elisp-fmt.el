;;; elisp-fmt.el --- Format emacs-lisp files -*- lexical-binding: t -*-

;; Copyright (C) 2016 Sam Halliday
;; License: http://www.gnu.org/licenses/gpl.html

;; Homepage: https://gitlab.org/fommil/elisp-fmt
;; Keywords: lisp
;; Package-Version:  1.0
;; Package-Requires: ((elisp-format "0.5.7") (commander "0.7.0"))

;;; Commentary:
;;
;;  Wrapper around `elisp-format' for use in Cask scripts.
;;
;;; Code:

(require 'commander)
(require 'elisp-format)

(defvar elisp-fmt-directory default-directory)

(file-directory-p default-directory)

;; redefine upstream function to skip hidden files and folders
(defun elisp-format-directory (dir)
  "Format recursive elisp files under DIR."
  (interactive "DDirectory: ")
  (let ((suffix (format "^.*\\.el%s$" (regexp-opt load-file-rep-suffixes))))
    (dolist (file (directory-files dir t))
      (if (file-directory-p file)
          ;; Don't match . or .. directory.
          (unless (string-match "^\\..*" (file-name-nondirectory file))
            ;; Find files in sub-directory.
            (elisp-format-directory file))
        ;; Not backup file.
        (unless (string-match "^\\.?#" (file-name-nondirectory file))
          ;; Match elisp file or it's compress format.
          (if (string-match suffix (file-name-nondirectory file))
              (elisp-format-file file)))))))

(defun elisp-fmt/directory (dir)
  "`DIR' the directory to format."
  (setq elisp-fmt-directory dir))

(defun elisp-fmt/run ()
  "Format the sources."
  (elisp-format-directory elisp-fmt-directory)
  (whitespace-cleanup))

(commander (name "elisp-fmt")
           (description "Format emacs-lisp files.")
           (default elisp-fmt/run)
           (option "--dir" elisp-fmt/run))

(provide 'elisp-fmt.el)

;;; elisp-fmt.el ends here
