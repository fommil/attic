/* Copyright Samuel Halliday 2012 */
package fommil.logging

import akka.event.LoggingAdapter
import concurrent.{Future, ExecutionContext}

/** A wrapper to ensure that fire-and-forget `Future` blocks will be logged
  * if an exception is thrown.
  *
  * @see https://groups.google.com/d/topic/akka-user/IDDXvHCH_8o/discussion
  */
object LoggedFuture {
  def apply[T](msg: String)(f: => T)
              (implicit log: LoggingAdapter, executor: ExecutionContext) = {
    val future = Future {
      f
    }
    future onFailure {
      case e: Throwable => log.error(e, msg)
    }
    future
  }
}