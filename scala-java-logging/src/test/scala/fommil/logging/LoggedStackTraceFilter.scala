/* Copyright Samuel Halliday 2008 */
package fommil.logging

import org.specs2.control.StackTraceFilter
import akka.contrib.jul.JavaLogging

/** Uses the J2SE Logging framework for specs2 stack traces.
  *
  * To enable, put the following in the Specification
  * {{{
  *     args.report(traceFilter=LoggedStackTraceFilter)
  * }}}
  *
  * @see https://groups.google.com/d/topic/specs2-users/3cCUf-kUsaU/discussion
  */
object LoggedStackTraceFilter extends StackTraceFilter with JavaLogging {
  def apply(e: Seq[StackTraceElement]) = Nil
  override def apply[T <: Exception](e: T): T = {
    log.error(e, "Specs2")
    // this only works because log.error will construct the LogRecord instantly
    e.setStackTrace(new Array[StackTraceElement](0))
    e
  }
}
