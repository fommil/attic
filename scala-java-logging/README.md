scala-java-logging
==================

Utilise the Java Logger – with excellent formatting and filtering – in Scala projects.

The primary motivation is to clear up the output from log messages (and in particular, exceptions)
so that problems can be very easily identified. This is achieved by:

1. custom formatter which allows configuration file definition of output logs (why this never shipped with Java, we'll never know).
2. stacktrace filter which (by default) hides internal Java / Scala / Akka traces.
3. custom log filter which uses the classname (not the logger name): excellent for use with Akka.
4. `LoggedFuture` which means that failing fire-and-forget Akka Futures are not ignored.
5. (hacky) support for Specs2, which by default tries to do its own exception handling
   (copy over the `LoggedStackTraceFilter` to your project to get this support).

Points 1 - 3 can be enabled by copying the `logging.properties` file to your project (customising as needed)
and passing `-Djava.util.logging.config.file=logging.properties` to the `java` binary when
running your application.

To get a handle to a Logger, simply mixin the `JavaLogging` trait (this is actually part of `akka-contrib`).

Akka developers are advised to use the following `Config` setup to ensure that the Akka Logger is using the
Java Logger backend:

```
akka {
  event-handlers = ["akka.contrib.jul.JavaLoggingEventHandler"]
  actor.debug.unhandled = true
  loglevel = DEBUG
  stdout-loglevel = WARNING
}
```

Installation
============

We haven't built to sonatype yet, so clone this repository and issue an `sbt publish-local`.
Then use the following in your `build.sbt`:

```
libraryDependencies += "fommil" % "scala-java-logging" % "1.0"
```

Java developers can copy the relevant `java` files to their codebases to get the benefit of
the custom formatters and filters.

