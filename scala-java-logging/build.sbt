/** Project */
name := "scala-java-logging"

version := "1.0"

organization := "fommil"

scalaVersion := "2.10.0-RC5"

/** Dependencies */
resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies += "com.typesafe.akka" % "akka-contrib" % "2.1.0-RC6"       cross CrossVersion.full

libraryDependencies += "org.specs2"        % "specs2"       % "1.12.3" % "test" cross CrossVersion.full

/** Compilation */
fork := true

javaOptions += "-Xmx2G"

javaOptions += "-Djava.util.logging.config.file=logging.properties"

outputStrategy := Some(StdoutOutput)
