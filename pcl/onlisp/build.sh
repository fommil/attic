#!/bin/bash

wkhtmltopdf --page-width 20.25cm \
            --page-height 27cm \
            --margin-top 15mm \
            --margin-bottom 15mm \
            onlisp.html onlisp.pdf

gs -dBATCH -dNOPAUSE -q \
   -dPDFSETTINGS=/ebook \
   -dCompressFonts=true \
   -dDetectDuplicateImages=true \
   -sDEVICE=pdfwrite \
   -r150 \
   -g1600x1200 \
   -dPDFFitPage \
   -sOutputFile=onlisp-optimised.pdf \
   onlisp.pdf
