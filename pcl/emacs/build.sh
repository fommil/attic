#!/bin/bash

MODULES="ccmode cl eieio"

for A in $MODULES ; do
    if [ ! -f "$A" ] ; then
        wget https://www.gnu.org/software/emacs/manual/html_mono/${A}.html
    fi
done

for A in $MODULES ; do
    wkhtmltopdf --page-width 20.25cm \
                --page-height 27cm \
                --margin-top 15mm \
                --margin-bottom 15mm \
                --user-style-sheet style.css \
                ${A}.html ${A}.pdf
done
