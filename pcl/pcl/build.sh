#!/bin/bash

if [ ! -d www.gigamonkeys.com ] ; then
    wget --recursive \
         --no-clobber \
         --page-requisites \
         --html-extension \
         --convert-links \
         --restrict-file-names=windows \
         --domains gigamonkeys.com \
         --no-parent http://www.gigamonkeys.com/book/
fi

BASE="www.gigamonkeys.com/book/"
PARTS=`cat $BASE/index.html | grep '<li><a href' | sed s/[^\']*\'// | sed s/\'.*// | grep -iv blurbs | tr '\n' ' '`

# so we can override
> www.gigamonkeys.com/book/style.css

# could parallelise if needed http://unix.stackexchange.com/a/216466/22890
COUNTER=1
for PART in ${PARTS} ; do
    PAGE=`printf %02d $COUNTER`
    let COUNTER=$COUNTER+1
    wkhtmltopdf --load-error-handling skip \
                --load-media-error-handling skip \
                --user-style-sheet style.css \
                --page-width 20.25cm \
                --page-height 27cm \
                --margin-top 15mm \
                --margin-bottom 15mm \
                ${BASE}/${PART} pcl_${PAGE}.pdf
    gs -q -dNOPAUSE -dBATCH -dPDFSETTINGS=/ebook -sDEVICE=pdfwrite -sOutputFile=pcl-opt_${PAGE}.pdf pcl_${PAGE}.pdf
done

# http://stackoverflow.com/questions/10450120
gs -dBATCH -dNOPAUSE -q \
   -dPDFSETTINGS=/ebook \
   -dCompressFonts=true \
   -dDetectDuplicateImages=true \
   -sDEVICE=pdfwrite \
   -sOutputFile=pcl-opt.pdf pcl-opt_*.pdf

# this is corrupted as per https://github.com/wkhtmltopdf/wkhtmltopdf/issues/2972
gs -dBATCH -dNOPAUSE -q \
   -dPDFSETTINGS=/ebook \
   -dCompressFonts=true \
   -dDetectDuplicateImages=true \
   -sDEVICE=pdfwrite \
   -sOutputFile=pcl.pdf pcl_*.pdf

#rm pcl_*.pdf pcl-opt_*.pdf
