package com.github.fommil

import breeze.linalg._
import util.Random
import pimpathon.file._

/**
 * Simple application of a Kalman Filter to a 2D canon ball trajectory
 * with noisy sensors and perfect physics.
 *
 * The Kalman equations reduce to
 *
 *    x' = F.x + B.u
 *
 * with basis (x, v_x, y, v_y)
 *
 * runMain com.github.fommil.Canon
 */
object Canon extends App {

  // constants
  val dt = 0.1
  val g = 9.8

  def I = DenseMatrix.eye[Double](4)

  // control, gravity acts regardless of the state
  val B = DenseMatrix(
    (0.0, 0.0, 0.0, 0.0),
    (0.0, 0.0, 0.0, 0.0),
    (0.0, 0.0, -1.0, 0.0),
    (0.0, 0.0, 0.0, -1.0)
  )
  val u = DenseVector(0, 0, g * dt * dt, g * dt)

  // state transition
  val F = DenseMatrix(
    (1.0, dt, 0.0, 0.0),
    (0.0, 1.0, 0.0, 0.0),
    (0.0, 0.0, 1.0, dt),
    (0.0, 0.0, 0.0, 1.0)
  )

  // perfect measurements
  val H = I

  // guess of state and variance
  var s = DenseVector.zeros[Double](4)
  var P = I

  // estimated process error and measurement error
  val Q = DenseMatrix.zeros[Double](4, 4)
  val R = I * 0.2

  // actual state and noise in measurement
  var x = DenseVector(0.0, 100, 0.0, 100)
  def noisy(actual: Double) = actual + Random.nextGaussian * 50

  val out = file("src/main/R/canon.csv")
  out.delete()
  out.write("time,zx,zy,px,py,x,y\n")

  // Run the simulation. We're doing both simulation and Kalman logic
  // together to keep this concise. In reality, the observer can't see
  // x and may be using a different F, B and u.
  var t = 0.0

  while (x(2) >= 0) {
    // measurement
    //val z = DenseVector(noisy(x(0)), x(1), noisy(x(2)), x(3))
    val z = x.mapValues(noisy)

    // actual simulation
    x = F * x + B * u
    t += dt

    // prediction step
    val predS = F * s + B * u
    val predP = F * P * F.t + Q
    // observation step
    val innov = z - H * predS
    val innov_cov = H * predP * H.t + R
    // update step
    val gain = predP * H.t * inv(innov_cov)
    s = predS + gain * innov
    P = (I - gain * H) * predP

    out.write(List(
      t, z(0), z(2), s(0), s(2), x(0), x(2)
    ).mkString(",") + "\n")
  }

}
