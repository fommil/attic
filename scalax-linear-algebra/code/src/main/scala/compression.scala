package com.github.fommil

import java.awt.Color
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import BufferedImage._

import breeze.linalg._
import pimpathon.file._

// runMain com.github.fommil.SvdImage
object SvdImage extends App {

  def imageToMatrix(img: BufferedImage): DenseMatrix[Double] = {
    val width = img.getWidth()
    val height = img.getHeight()
    val mat = DenseMatrix.zeros[Double](height, width)
    val data = img.getData
    for {
      x <- 0 until width
      y <- 0 until height
    } {
      mat(y, x) = data.getSample(x, y, 0)
    }
    mat
  }

  def matrixToImage(mat: DenseMatrix[Double]): BufferedImage = {
    // using TYPE_BYTE_GRAY comes out very dark... meh
    val img = new BufferedImage(mat.cols, mat.rows, TYPE_INT_RGB)
    for {
      x <- 0 until mat.cols
      y <- 0 until mat.rows
    } {
      val g = ((0 max mat(y, x).toInt) min 255)
      img.setRGB(x, y, new Color(g, g, g).getRGB())
    }
    img
  }

  def writeOnImage(img: BufferedImage, i: Int): Unit = {
    val g = img.getGraphics()
    g.setPaintMode()
    g.setFont(g.getFont().deriveFont(30f))
    g.drawString(i.toString, 0, 25)
    g.dispose()
  }

  val jpg = ImageIO.read(file("../images/odersky-grayscale.jpg"))
  val orig = imageToMatrix(jpg)

  val svd.SVD(u, s, v) = svd(orig)

  for {
    i <- 1 until u.cols
    if i <= 100 // more makes no discernable difference!
  } {
    val out = f"compressed-$i%03d.bmp"
    println("compressing to " + out)
    val compressed = u(::, 0 until i) * diag(s(0 until i)) * v(0 until i, ::)
    val converted = matrixToImage(compressed)

    writeOnImage(converted, i)
    ImageIO.write(converted, "BMP", file(out))
  }
  // to create the video run (yes, I know this introduces artefacts):
  // avconv -i 'compressed-%03d.bmp' -filter:v "setpts=20.0*PTS" ../images/odersky.mp4

}
