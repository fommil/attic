/*
  Build BLAS first, e.g. from netlib-java/netlib/blas

  gcc -shared -o libblas.so -fPIC -O3 *.f

  Then copy *.h from netlib-java/netlib/cblas and compile CBLAS with

  gcc -shared -o libcblas.so -fPIC -O3 -DADD_ *.f *.c

  This compiles a binary against "libcblas.so" to be defined at runtime.

  To set up ACML, go into /opt/acml5.3.1/gfortran64_mp/lib/ and type

  sudo ln -s libacml_mp.so libblas.so

  To set up MKL, go into /opt/intel/mkl/lib/intel64/ and type

  sudo ln -s libmkl_rt.so libblas.so

  Compile this file against the bundled CBLAS and BLAS (but `ldd` will use system libs)

  gcc -O3 dgemmtest.c common.c -o dgemmtest -I. -L. -lcblas -lblas -lgfortran -lm

  Then choosing the backend is a runtime choice:

  LD_LIBRARY_PATH=. ./dgemmtest > ../R/netlib-ref.csv
  LD_LIBRARY_PATH=/usr/lib/atlas-base:. ./dgemmtest > ../R/atlas.csv
  LD_LIBRARY_PATH=/usr/lib/openblas-base:. ./dgemmtest > ../R/openblas.csv
  LD_LIBRARY_PATH=/opt/acml/lib/:. ./dgemmtest > ../R/acml.csv
  LD_LIBRARY_PATH=/opt/intel/mkl/lib/intel64:. ./dgemmtest > ../R/intel.csv

  # veclib requires running on mac, and compile with
  # -I/System/Library/Frameworks/vecLib.framework/Headers -framework veclib
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cblas.h>
#include "common.h"

long benchmark(int size) {
    int m = sqrt(size);
    long requestStart, requestEnd;

    double* a = random_array(m * m);
    double* b = random_array(m * m);

    requestStart = currentTimeNanos();

    double* c = calloc(m * m, sizeof(double));

    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, m, m, 1, a, m, b, m, 0, c, m);

    requestEnd = currentTimeNanos();

    free(a);
    free(b);
    free(c);

    return (requestEnd - requestStart);
}

int main() {
    srand(time(NULL));

    double factor = 6.0 / 100.0;
    int i, j;
    for (i = 0 ; i < 5 ; i++) {
        for (j = 1 ; j <= 100 ; j++) {
            int size = (int) pow(10.0, factor * j);
            if (size < 10) continue;
            long took = benchmark(size);
            // the first times is just a warm-up
            if (i == 4) {
                printf("%d,%lu\n", size, took);
                fflush(stdout);
            }
        }
    }

    return(0);
}
