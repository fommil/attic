/*
 gcc -O3 bottleneck.c common.c -lm -o bottleneck-write
 gcc -O3 bottleneck.c common.c -DREAD_COL -lm -o bottleneck-readwrite
 ./bottleneck-write > ../R/bottleneck-write.csv
 ./bottleneck-readwrite > ../R/bottleneck-readwrite.csv
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cblas.h>
#include "common.h"

long benchmark(int size) {
    int i, j, k, c;
    int m = sqrt(size);
    long requestStart, requestEnd;
    int length = m * m;
    double tmp;
    double* a = random_array(length);
    requestStart = currentTimeNanos();
    double* b = calloc(length, sizeof(double));
    
    for (i = 0 ; i < m ; i++) {
        c = i * m;
        for (j = 0 ; j < m ; j ++) {
            #ifdef READ_COL
            for (k = 0 ; k < m ; k++) {
                tmp = a[c + k];
            }
            b[c + j] = tmp;
            #else
            b[c + j] = a[c + j];
            #endif
        }
    }
    requestEnd = currentTimeNanos();
    free(a);
    free(b);
    return (requestEnd - requestStart);
}

main() {
    srand(time(NULL));

    double factor = 6.0 / 100.0;
    int i, j;
    //    for (i = 0 ; i < 10 ; i++) {
        for (j = 1 ; j <= 100 ; j++) {
            int size = (int) pow(10.0, factor * j);
            if (size < 10) continue;
            long took = benchmark(size);
            printf("%d,%lu\n", size, took);
            fflush(stdout);
        }
        //    }
}
