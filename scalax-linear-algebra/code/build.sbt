// mkdir -p src/{main,test}/{fortran,c,java,scala}/

organization := "com.github.fommil"
name := "scalax"
version := "1.0-SNAPSHOT"
scalaVersion := "2.11.4"

libraryDependencies ++= Seq(
  "org.scalanlp" %% "breeze" % "0.10",
  "org.scalanlp" %% "breeze-natives" % "0.10",
  "com.github.fommil.netlib" % "all" % "1.1.2" pomOnly(),
  "com.google.guava" % "guava" % "18.0",
  "com.github.stacycurl" %% "pimpathon-core" % "1.2.0",
  "org.scalatest"  %% "scalatest" % "2.2.2" % "test"
)

// lionRuns := 0
// lionAllocRuns := 1
// lionAllocRate := 1

scalariformSettings
