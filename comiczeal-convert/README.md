comiczeal-convert
=================

Converts Comic Zeal "Imported Comics" directories into a sensible CBZ directory structure.

Running
=======

You must have both [sbt](https://github.com/sbt/sbt) and [sqlite3](http://www.sqlite.org)
installed to use this application. e.g. on OS X run the following (if you manage your OSS
applications with [MacPorts](http://www.macports.org)):

```
sudo port install sqlite3 sbt
```

Then you have to install a local version of [scalaqlite](https://github.com/srhea/scalaqlite/tree/jna),
because it is not distributed using the normal Scala distribution channels.

```
git clone git@github.com:srhea/scalaqlite.git
cd scalaqlite
git checkout jna
# edit build.sbt to agree with these scalaVersion and scalatest versions
sbt publish-local
```

then you can go back into your `comiczeal-convert` directory and issue

```
LD_LIBRARY_PATH=/opt/local/lib sbt "run \"input folder\" \"output folder\""
```

(you may need to set `LD_LIBRARY_PATH` to your `libsqlite3` directory)


Donations
=========

Please consider supporting the maintenance of this open source project with a donation:

[![Donate via Paypal](https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=B2HW5ATB8C3QW&lc=GB&item_name=comiczeal-convert&currency_code=GBP&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)

Licence
=======

Copyright (C) 2013 Samuel Halliday

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/


Contributing
============

Contributors are encouraged to fork this repository and issue pull
requests. Contributors implicitly agree to assign an unrestricted licence
to Sam Halliday, but retain the copyright of their code (this means
we both have the freedom to update the licence for those contributions).
