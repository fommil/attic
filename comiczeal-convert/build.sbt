organization := "com.github.fommil"

name := "comiczeal-convert"

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.0"

libraryDependencies ++= Seq(
  "org.srhea"     %% "scalaqlite" % "0.3-SNAPSHOT",
  "org.scalatest" %% "scalatest"  % "1.9.1" % "test"
)


// javaOptions += "-Djna.library.path=/opt/local/lib"

javaOptions += "-Djava.awt.headless=true"

mainClass := Some("com.github.fommil.cbz.ComicZealConvert")

fork := true
