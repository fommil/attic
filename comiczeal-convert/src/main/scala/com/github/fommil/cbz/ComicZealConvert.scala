// Copyright (C) 2013 Samuel Halliday

package com.github.fommil.cbz

import org.srhea.scalaqlite.{SqlInt, SqliteDb}
import java.io.File
import concurrent.{Await, Future}
import concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global
import java.io.{BufferedInputStream, FileInputStream, FileOutputStream}
import java.util.zip.{ZipEntry, ZipOutputStream}


case class NotebookEntry(id: Long, parent: Option[Int], name: String)

object ComicZealConvert extends App {

  val path = args.toList match {
    case input :: output :: Nil => (input, output)
    case _ => ("Imported Comics", "Output")
  }
  val in = new File(path._1)
  val out = new File(path._2)
  require(in.exists, "input %s does not exist" format (in))
  require(!out.exists, "output %s already exists" format (out))

  val dbFile = new File(in, "comic_database.sqlite")
  require(dbFile.exists)
  var db = new SqliteDb(dbFile.getAbsolutePath)

  val entries = (db.query("SELECT * FROM ZNOTEBOOKENTRY;").toList map {
    row =>
      val id = row(0).toInt
      val parent = row(8) match {
        case SqlInt(int) => Some(int)
        case _ => None
      }
      val name = row(21).toString
      (id, NotebookEntry(id, parent, name))
  }).toMap
  db.close()

  def getOutput(entry: NotebookEntry, path: List[String]): List[String] = entry match {
    case NotebookEntry(_, None, _) => path
    case NotebookEntry(_, Some(parent), branch) => getOutput(entries(parent), Nil) ::: List(branch) ::: path
  }

  def getOutput(entry: NotebookEntry): File = new File(out, getOutput(entry, Nil).reduce(_ + "/" + _) + ".cbz")

  def create(in: File, out: File) {
    println("Converting " + in + " to " + out)
    val dir = out.getParentFile
    require(dir.isDirectory || dir.mkdirs(), "Failed to create: " + dir)

    val zip = new ZipOutputStream(new FileOutputStream(out))
    in.list().foreach {
      file =>
        val filename = new File(in, file).getAbsolutePath
        zip.putNextEntry(new ZipEntry(filename))
        val stream = new BufferedInputStream(new FileInputStream(filename))
        try {
          var b = stream.read()
          while (b > -1) {
            zip.write(b)
            b = stream.read()
          }
        } finally {
          stream.close()
          zip.closeEntry()
        }
    }
    zip.close()
  }

  val tasks = entries.values flatMap {
    entry =>
      val file = new File(in, entry.name)
      if (file.isDirectory) {
        Some(Future {
          create(file, getOutput(entry))
        })
      } else None
  }

  Await.result(Future.sequence(tasks), Duration.Inf)
}
