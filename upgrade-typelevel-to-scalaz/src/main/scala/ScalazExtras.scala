// Copyright: 2018 - 2018 Sam Halliday
// License: https://opensource.org/licenses/BSD-3-Clause

package prelude

import scalaz._, Scalaz._

/**
 * Extensions to scalaz that are suitable for submission upstream.
 *
 * DO NOT add anything here that is unlikely to pass peer review: only pure,
 * total, functions. Each addition should ideally be documented by an upstream
 * ticket or pull request.
 */
trait ScalazExtras {
  // https://github.com/scalaz/scalaz/pull/1890
  implicit final class EitherTOps[F[_], A, B](
    private val self: EitherT[F, A, B]
  ) {

    /** Map into the underlying monad */
    def mapF[C](f: B => F[C])(implicit M: Monad[F]): EitherT[F, A, C] =
      self.flatMapF {
        f.andThen(mb => M.map(mb)(b => \/-(b)))
      }
  }

  implicit final class ExtraEitherOps[A, B](private val v: Either[A, B]) {
    def toValidationNel: ValidationNel[A, B] = v.disjunction.validationNel
  }

  implicit final class ExtraBindRecOps[F[_], A](private val self: F[A]) {
    def forever[B](implicit F: BindRec[F]): F[B] = F.forever(self)
  }

  // tuple mapN
  // https://github.com/scalaz/scalaz/issues/1899
  // format: off
  implicit final class ApplyTuple2Ops[F[_], A1, A2](
    private val self: (F[A1], F[A2])
  ) {
    def mapN[B](f: (A1, A2) => B)(implicit F: Apply[F]): F[B] = {
      val (fa, fb) = self
      F.apply2(fa, fb)(f)
    }
  }

  implicit final class ApplyTuple3Ops[F[_], A1, A2, A3](
    private val self: (F[A1], F[A2], F[A3])
  ) {
    def mapN[B](f: (A1, A2, A3) => B)(implicit F: Apply[F]): F[B] = {
      val (fa, fb, fc) = self
      F.apply3(fa, fb, fc)(f)
    }
  }

  implicit final class ApplyTuple4Ops[F[_], A1, A2, A3, A4](
    private val self: (F[A1], F[A2], F[A3], F[A4])
  ) {
    def mapN[B](f: (A1, A2, A3, A4) => B)(implicit F: Apply[F]): F[B] = {
      val (fa, fb, fc, fd) = self
      F.apply4(fa, fb, fc, fd)(f)
    }
  }

  implicit final class ApplyTuple5Ops[F[_], A1, A2, A3, A4, A5](
    private val self: (F[A1], F[A2], F[A3], F[A4], F[A5])
  ) {
    def mapN[B](f: (A1, A2, A3, A4, A5) => B)(implicit F: Apply[F]): F[B] = {
      val (fa, fb, fc, fd, fe) = self
      F.apply5(fa, fb, fc, fd, fe)(f)
    }
  }

  implicit final class ApplyTuple7Ops[F[_], A1, A2, A3, A4, A5, A6, A7](
    private val self: (F[A1], F[A2], F[A3], F[A4], F[A5], F[A6], F[A7])
  ) {
    def mapN[B](f: (A1, A2, A3, A4, A5, A6, A7) => B)(implicit F: Apply[F]): F[B] = {
      val (fa, fb, fc, fd, fe, ff, fg) = self
      F.apply7(fa, fb, fc, fd, fe, ff, fg)(f)
    }
  }

  implicit final class ApplyTuple8Ops[F[_], A1, A2, A3, A4, A5, A6, A7, A8](
    private val self: (F[A1], F[A2], F[A3], F[A4], F[A5], F[A6], F[A7], F[A8])
  ) {
    def mapN[B](f: (A1, A2, A3, A4, A5, A6, A7, A8) => B)(implicit F: Apply[F]): F[B] = {
      val (fa, fb, fc, fd, fe, ff, fg, fh) = self
      F.apply8(fa, fb, fc, fd, fe, ff, fg, fh)(f)
    }
  }

  implicit final class ApplyTuple9Ops[F[_], A1, A2, A3, A4, A5, A6, A7, A8, A9](
    private val self: (F[A1], F[A2], F[A3], F[A4], F[A5], F[A6], F[A7], F[A8], F[A9])
  ) {
    def mapN[B](f: (A1, A2, A3, A4, A5, A6, A7, A8, A9) => B)(implicit F: Apply[F]): F[B] = {
      val (fa, fb, fc, fd, fe, ff, fg, fh, fi) = self
      F.apply9(fa, fb, fc, fd, fe, ff, fg, fh, fi)(f)
    }
  }

  implicit final class ApplyTuple10Ops[F[_], A1, A2, A3, A4, A5, A6, A7, A8, A9, A10](
    private val self: (F[A1], F[A2], F[A3], F[A4], F[A5], F[A6], F[A7], F[A8], F[A9], F[A10])
  ) {
    def mapN[B](f: (A1, A2, A3, A4, A5, A6, A7, A8, A9, A10) => B)(implicit F: Apply[F]): F[B] = {
      val (fa, fb, fc, fd, fe, ff, fg, fh, fi, fj) = self
      F.apply10(fa, fb, fc, fd, fe, ff, fg, fh, fi, fj)(f)
    }
  }

  implicit final class ApplyTuple11Ops[F[_], A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11](
    private val self: (F[A1], F[A2], F[A3], F[A4], F[A5], F[A6], F[A7], F[A8], F[A9], F[A10], F[A11])
  ) {
    def mapN[B](f: (A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11) => B)(implicit F: Apply[F]): F[B] = {
      val (fa, fb, fc, fd, fe, ff, fg, fh, fi, fj, fk) = self
      F.apply11(fa, fb, fc, fd, fe, ff, fg, fh, fi, fj, fk)(f)
    }
  }

  implicit final class ApplyTuple14Ops[F[_], A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14](
    private val self: (F[A1], F[A2], F[A3], F[A4], F[A5], F[A6], F[A7], F[A8], F[A9], F[A10], F[A11], F[A12], F[A13], F[A14])
  ) {
    def mapN[B](f: (A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13, A14) => B)(implicit F: Apply[F]): F[B] = {
      import F._
      val (fa, fb, fc, fd, fe, ff, fg, fh, fi, fj, fk, fl, fm, fn) = self
      ap(fn)(ap(fm)(ap(fl)(ap(fk)(ap(fj)(ap(fi)(ap(fh)(ap(fg)(ap(ff)(ap(fe)(ap(fd)(ap(fc)(ap(fb)(map(fa)(f.curried))))))))))))))
    }
  }
  // format: on

}
