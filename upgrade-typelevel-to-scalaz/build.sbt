name := "upgrade-typelevel-to-scalaz"
libraryDependencies ++= Seq(
  "org.scalaz" %% "scalaz-ioeffect-cats" % "2.10.1",
  "com.fommil" %% "deriving-macro"       % "0.13.1",
  compilerPlugin("com.github.ghik" %% "silencer-plugin" % "1.0"),
  "com.github.ghik" %% "silencer-lib" % "1.0" % Provided
)

addCommandAlias("cpl", "all compile test:compile")
addCommandAlias("fmt", "all scalafmtSbt scalafmt test:scalafmt")
addCommandAlias(
  "check",
  "all scalafmtSbtCheck scalafmtCheck test:scalafmtCheck"
)
addCommandAlias("fix", "all compile:scalafixCli test:scalafixCli")
