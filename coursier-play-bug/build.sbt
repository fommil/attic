libraryDependencies ++= Seq(
  "org.apache.cassandra" % "cassandra-all" % versions.cassandra % Test,
  "com.typesafe.akka" %% "akka-persistence-cassandra" % versions.persistenceCassandra
)
