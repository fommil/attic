package fommil

import akka.persistence.cassandra.testkit.CassandraLauncher
import java.nio.file.Files

object Fail {
  def main(args: Array[String]): Unit = {

    val dir = Files.createTempDirectory("cassandra")
    CassandraLauncher.start(
      cassandraDirectory = dir.toFile,
      configResource = CassandraLauncher.DefaultTestConfigResource,
      clean = true,
      port = 0
    )
  }
}
