import sbt._
import sbt.Keys._

object Dependencies {
  object versions {
    val cassandra = "3.5"
    val persistenceCassandra = "0.14"
  }
}

object CommonPlugin extends AutoPlugin {
  override def requires = plugins.JvmPlugin
  override def trigger = allRequirements

  val autoImport = Dependencies

  override val buildSettings = Seq(
    organization := "com.fommil",
    scalaVersion := "2.11.9"
  )

  override val projectSettings = Seq(
    fork := true
  )

}
